@extends('backend.layouts.master')
@push('header')
<style type="text/css">
	.line_set
	{
		padding-top: 22px !important;
	}
	.panel_toolbox>li.active
	{
		background-color: #73879C !important; 
		color: #515356 !important;
		border-radius: 10px;
	}
</style>
<script src="{{ asset('backend/vendors/jquery/dist/jquery.min.js') }}"></script>
@endpush
@section('content')

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="col-sm-6">
				<h2>Users List </h2>
				</div>
				
				<div class="col-sm-6">
					<ul class="nav navbar-right panel_toolbox" >
						<li class=""><a href="{{ URL::to('/backend/dashboard') }}">Dashboard</a></li>
						<li><a>/</a></li>
						<li class="active"><a>Meals</a></li>
						<li><a>/</a></li>
						<li class="active"><a>{{ $mode }} Meals</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<form class="form-horizontal form-label-left">
					<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Default Input</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="Default Input">
                        </div>
                      </div>
				</form>
			</div>
		</div>
	</div>
<script type="text/javascript">
$(function () {
	    $('.list_users').DataTable({
	      "paging": true,
	      "ordering": true,
	      "info": true,
	      "autoWidth": false,
	      "aaSorting": [],
	      "aoColumns": [
	            { "bSortable": false },
	            { "bSortable": false },
	            null,
	            null,
	            null,
	           	{ "bSortable": false },
            ]

	    });
  	});
</script>
@endsection