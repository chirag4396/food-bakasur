@extends('backend.layouts.master')
@push('header')
<style type="text/css">
    table td,th{
        border: 1px solid #858585;
        padding: 8px;
    }
</style>
@endpush
@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title" style="text-align: center;">
				<h3>Business Details</h3>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			
			<table class="table" style="width: 97.7%;margin-left: 15px;">
                <tbody><tr class="block-header" style="background-color:#73879C !important; color:#ffff 	!important;"><th style="border-color: #73879C !important;"><center>Business Title : {{ $bus_details['b_name'] }}</center></th></tr>
                 </tbody>
            </table>
            <div class="col-sm-12" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="2" style="padding: 10px;"><center>Business Detail</center></th></tr>
                         <tr>
                            <td width="20%"><b>Business Type</b></td>  <td>{{ ($bus_details->b_bt_id==0)? $bus_details->b_other_name : $bus_details->business_type  }}</td>
                        </tr>
                        <tr>
                            <td width="20%"><b>Contact Person</b></td>  <td>{{ $bus_details->b_contact_person_name }}</td>
                        </tr>
                         <tr>
                            <td width="20%"><b>Contact No.</b></td><td>{{ $bus_details->b_contact_person_mobile }} </td>
                        </tr>
                        <tr>
                             <td  width="20%"><b>Web Link</b></td> <td>{{ $bus_details->b_weblink }}</td>
                        </tr>
                        <tr>
                              <td width="20%"><b>Address</b></td><td>{{ $bus_details->b_address }}</td>
                        </tr>
                        <tr>
                              <td width="20%"><b>Payment Status</b></td><td>{{ ($bus_details->b_payment_status==1)?'Paid':'Not Paid' }}</td>
                        </tr>
                        
                         <tr>
                              <td width="20%"><b>Posted On</b></td><td>{{ $bus_details->created_at }}</td>
                        </tr>   
                    </table>
                </div>
            </div>
           
            <div class="col-sm-6" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="3" style="padding: 10px;"><center>Business Image</center></th></tr>
                        <tr>
                            <td><img src="{{ $bus_details['b_photo'] }}"  height="209px" width="100%" style="padding: 10px;"></td>
                        </tr>
                    </table>
                </div>
            </div>
             <div class="col-sm-6" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="3" style="padding: 10px;"><center>Business Logo</center></th></tr>
                        <tr>
                            <td><img src="{{ $bus_details['b_logo'] }}"  height="209px" width="100%" style="padding: 10px;"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-6" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="2" style="padding: 10px;"><center>User Details</center></th></tr>
                        <tr>
                           <td >Name:</td><td >{{ $user_details['name'] }}</td>
                           
                        </tr>
                        <tr>
                           <td >Email:</td><td >{{ $user_details['email'] }}</td>
                           
                        </tr>
                        <tr>
                           <td >Phone:</td><td>{{ $user['ud_mobile'] }}</td>
                           
                        </tr>
                        <tr>
                           <td >Birthdate:</td><td >{{ $user['ud_birthday'] }}</td>
                           
                        </tr>
                        <tr>
                           <td>Followers:</td><td>{{ $user['ud_followers'] }}</td>
                           
                        </tr>
                        <tr>
                            <td>Following:</td><td>{{ $user['ud_followings'] }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-6" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="3" style="padding: 10px;"><center>Open Days and Timing</center></th>
                        </tr>
                        <tr><th>Days</th><th>Open Time</th><th>Close Time</th></tr>
                            @foreach($timing as $t=>$v)
                                <tr><td>{{ $v->day_title }}</td>
                                    <td>{{ date('h:i a', strtotime($v->but_open)) }}</td>
                                    <td>{{ date('h:i a', strtotime($v->but_close)) }}</td></tr>
                            @endforeach
                        
                    </table>
                </div>
            </div>
           
		</div>
	</div>
	</div>
		
@endsection
<script type="text/javascript">
    function Imageview(step,image,title)
    {
        var str=step+' : '+title;
        $('.model-content').css({"width": "50%"});
            $('.modal-title').html(str);
            $('.modal-body').html('<center><img src='+image+'></img></center>');
            $("#myModal").modal({show: true});
    }
</script>