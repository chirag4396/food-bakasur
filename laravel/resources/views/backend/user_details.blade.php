@extends('backend.layouts.master')
@push('header')
<script src="{{ asset('backend/vendors/jquery/dist/jquery.min.js') }}"></script>
<style type="text/css">
	.panel_toolbox>li.active
	{
		background-color: #73879C !important; 
		color: #515356 !important;
		border-radius: 10px;
	}
	.pagination
	{
		float: right;
	}
</style>
@endpush
@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="col-sm-6">
				<h2>User Details </h2>
				</div>
				
				<div class="col-sm-6">
					<ul class="nav navbar-right panel_toolbox" >
						<li class=""><a href="{{ URL::to('/backend/dashboard') }}">Dashboard</a></li>
						<li><a>/</a></li>
						<li><a href="{{ URL::to('/backend/users') }}">Users</a></li>
						<li><a>/</a></li>
						<li class="active"><a>User Details</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
                      	<li role="presentation" class="active"><a href="#tab_content44" id="business-tabb" role="tab" data-toggle="tab" aria-controls="business" aria-expanded="true">Businesses</a>
                        </li>
                        <li role="presentation" ><a href="#tab_content11" id="recipe-tabb" role="tab" data-toggle="tab" aria-controls="recipe" aria-expanded="true">Recipes</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content22" role="tab" id="blog-tabb" data-toggle="tab" aria-controls="blog" aria-expanded="false">Blogs </a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content33" role="tab" id="profile-tabb" data-toggle="tab" aria-controls="profile" aria-expanded="false">User Detail</a>
                        </li>
                      </ul>
                      <div id="myTabContent2" class="tab-content">
                      	<div role="tabpanel" class="tab-pane fade active in" id="tab_content44" aria-labelledby="business-tab">
                          <table class="table table-striped jambo_table bulk_action user-business">
								<thead>
									<tr class="headings">
										
										<th class="column-title">Business Name</th>
										<th class="column-title">Payment Status </th>
										<th class="column-title">Business Type </th>
										<th class="column-title no-link last"><span class="nobr">Action</span>
										</th>
									</tr>
								</thead>

								<tbody>
									@foreach($businesses as $r=>$v)
										<tr>
											
											<td>{{ $v->b_name }}</td>
											<td>{{ ($v->b_payment_status==1)?'Paid':'Not Paid' }}</td>
											<td>{{ $v->business_type }}</td>
											
											<td><a href="{{ URL::to('/backend/user-business/'.$v->b_id) }}"><button class="btn btn-warning">View</button></a></td>
										</tr>
									@endforeach
								</tbody>

							</table>
							{{ $businesses->links() }}
                        </div>
                        <div role="tabpanel" class="tab-pane fade " id="tab_content11" aria-labelledby="recipe-tab">
                          <table class="table table-striped jambo_table bulk_action user-recipe">
								<thead>
									<tr class="headings">
										
										<th class="column-title">Recipe Name</th>
										<th class="column-title">Meal </th>
										<th class="column-title">Cuisines </th>
										<th class="column-title">Food Type </th>
										<th class="column-title no-link last"><span class="nobr">Action</span>
										</th>
										<th class="bulk-actions" colspan="7">
											<a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
										</th>
									</tr>
								</thead>

								<tbody>
									@foreach($recepieData as $r=>$v)
										<tr>
											
											<td>{{ $v->rec_title }}</td>
											<td>{{ $v->meal }}</td>
											<td>{{ $v->cuisines }}</td>
											<td>{{ ($v->rec_veg==1)? 'Veg':'Non-Veg' }}</td>
											<td><a href="{{ URL::to('/backend/user-recipe/'.$v->rec_id) }}"><button class="btn btn-warning">View</button></a></td>
										</tr>
									@endforeach
								</tbody>
							</table>
							{{ $recepieData->links() }}
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content22" aria-labelledby="blog-tab">
                        	@if(Session::has('message'))
			            		<div class="col-12">
									<div class="alert alert-danger alert-dismissible">
					                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					                  
					                  {{ Session::get('message') }}
					                </div>
								</div>
			            	@endif
                          	<table class="table table-striped jambo_table bulk_action user-blog">
								<thead>
									<tr class="headings">
										<th class="column-title">Title</th>
										<th class="column-title">Meal </th>
										<th class="column-title">Cuisines </th>
										<th class="column-title">Food Type </th>
										<th class="column-title no-link last"><span class="nobr">Action</span>
										</th>
										<th class="bulk-actions" colspan="7">
											<a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
										</th>
									</tr>
								</thead>

								<tbody>
									@foreach($blogData as $b=>$v)
										<tr>
											
											<td>{{ $v->blog_title }}</td>
											<td>{{ $v->meal }}</td>
											<td>{{ $v->cuisines }}</td>
											<td>{{ ($v->blog_veg==1)? 'Veg':'Non-Veg' }}</td>
											<td><a href="{{ URL::to('/backend/user-blog/'.$v->blog_id) }}"><button class="btn btn-warning">View</button></a></td>
										</tr>
									@endforeach
								</tbody>
							</table>
							{{ $blogData->links() }}
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content33" aria-labelledby="profile-tab">
                        	<div class="col-sm-8">
	                          <table class="table table-striped jambo_table bulk_action">
	                          	<tbody>
	                          		<tr><td width="50%">Name:</td><td width="50%">{{ $userData->name }}</td></tr>
	                          		<tr><td width="50%">Email:</td><td width="50%">{{ $userData->email }}</td></tr>
	                          		<tr><td width="50%">Phone:</td><td width="50%">{{ $users->ud_mobile }}</td></tr>
	                          		<tr><td width="50%">Gender:</td><td width="50%">{{ ($users->ud_gender==0)?'Male':'Female' }}</td></tr>
	                          		<tr><td width="50%">Birth date:</td><td width="50%">{{ $users->ud_birthday }}</td></tr>
	                          		<tr><td width="50%">Followers:</td><td width="50%">{{ $users->ud_followers }}</td></tr>
	                          		<tr><td width="50%">Following:</td><td width="50%">{{ $users->ud_followings }}</td></tr>
	                          	</tbody>
	                          </table>
	                      </div>
	                      <div class="col-sm-4">
	                          <table class="table table-striped jambo_table bulk_action">
	                          			<thead><tr><th><center>Profile Pic</center></th></tr></thead>
	                          	<tbody>
	                          		<tr><td width="50%"><center><img src="{{ $users->profile_pic }}"></center></td></tr>
	                          	</tbody>
	                          </table>
	                      </div>
                        </div>
                      </div>
                    </div>

                  </div>
		</div>
	</div>
	<script type="text/javascript">
		setTimeout(function() {
		$(".alert").hide()
	}, 3500);
		$(function () {
		    $('.user-blog').DataTable({
		      "paging": false,
		      "ordering": true,
		      "info": true,
		      "autoWidth": false,
		      "aaSorting": [],
		      "aoColumns": [
		            
		            null,
		            null,
		            null,
		            null,
		           	{ "bSortable": false },
	            ]

		    });
		    $('.user-recipe').DataTable({
		      "paging": false,
		      "ordering": true,
		      "info": true,
		      "autoWidth": false,
		      "aaSorting": [],
		      "aoColumns": [
		            
		            null,
		            null,
		            null,
		            null,
		           	{ "bSortable": false },
	            ]

		    });
		    $('.user-business').DataTable({
		      "paging": false,
		      "ordering": true,
		      "info": true,
		      "autoWidth": false,
		      "aaSorting": [],
		      "aoColumns": [
		            
		            null,
		            null,
		            null,
		           	{ "bSortable": false },
	            ]

		    });
	  	});
	</script>


@endsection


