@if(!isset(Auth::user()->name))
		<script type="text/javascript">
    window.location = "{{ url('/login') }}";//here double curly bracket
		</script>
@endif
@include('backend.layouts.common_css')

@include('backend.layouts.sidebar')

@include('backend.layouts.header')



@yield('content')

@include('backend.layouts.footer')
@include('backend.layouts.common_js')
@include('backend.layouts.modal')

