<meta name="csrf-token" content="{{ csrf_token() }}">
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Food Bakasur</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{ asset('uploads/user_ph.jpg') }}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{ ucfirst(Auth::user()->name) }}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Dashboard </a>
                  </li>
                  <li><a><i class="fa fa-user"></i> Users <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li href="{{ URL::to('/backend/users') }}"><a href="{{ URL::to('/backend/users') }}">Users Info</a></li>
                            <li><a href="{{ URL::to('/backend/blogs') }}">Blogs</a></li>
                            <li><a href="{{ URL::to('/backend/recipes') }}">Recipes</a></li>
                            <li><a href="{{ URL::to('/backend/business') }}">Business</a></li>
                        </ul>
                  </li>
                  <li><a ><i class="fa fa-align-justify"></i> Comments <span class="fa fa-chevron-down"></span></a>
                     <ul class="nav child_menu">
                          
                            <li><a href="{{ URL::to('/backend/blog-comments') }}">Blog Comments</a></li>
                            <li><a href="{{ URL::to('/backend/recipe-comments') }}">Recipe Comments</a></li>
                            <li><a href="{{ URL::to('/backend/business-comments') }}">Business Comments</a></li>
                        </ul>
                  </li>
                 
                </ul>
              </div>
              

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
           
            <!-- /menu footer buttons -->
          </div>
        </div>
    