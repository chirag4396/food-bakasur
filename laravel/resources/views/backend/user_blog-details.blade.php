@extends('backend.layouts.master')
<style type="text/css">
	table td,th{
        border: 1px solid #858585;
        padding: 8px;
    }
</style>
@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title" style="text-align: center;">
				<h3>Blog Details</h3>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			{{-- <div class="x_title" style="text-align: center;">
				<h3>Blog Details</h3>
				<div class="clearfix"></div>
			</div>
			<div class="x_title" style="background-color: #73879C;color: white;text-align: center;font-size: 19px;">
				<span style="color: white;">Blog Title : {{ $blog_details['blog_title'] }}</span>
				<div class="clearfix" style="text-align:center !important;background-color: #73879C;color: white"></div>
			</div>
			<div class="x_content" >
					<div class="col-md-12" style="background-color:#1ABB9C;padding: 10px;color: white;text-align: center">
						<span style="font-size: 19px;">Blog Story</span>
					</div>
					<div class="" style="border-color:#1ABB9C;border:1px solid #1ABB9C ; ">
						<span style="padding: 10px;">{{ strip_tags($blog_details['blog_story']) }}</span>
					</div>
			</div>
			<div class="x_content">
				<div class="col-md-12" style="background-color:#73879C;padding: 10px;color: white;text-align: center">
						<span style="font-size: 19px;">Cover Photo</span>
					</div>
					<div class="" style="border-color:#73879C;border:1px solid #1ABB9C ; ">
						<img src="{{ asset('img/1.png') }}"  height="400px" width="100%" style="padding: 10px;">
					</div>
			</div> --}}
			
					
			
			<table class="table" style="width: 97.7%;margin-left: 15px;">
                <tbody><tr class="block-header" style="background-color:#73879C !important; color:#ffff 	!important;"><th style="border-color: #73879C !important;"><center>Blog Title : {{ $blog_details['blog_title'] }}</center></th></tr>
                 </tbody>
            </table>
            <div class="col-sm-12" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="4" style="padding: 10px;"><center>Blog Detail</center></th></tr>
                        <tr>
                            <td width="30%"><b>Meal Type </b></td>
                            <td width="30%"><b>Cuisine Type </b></td>
                            <td width="20%"><b>Food Type</b></td>
                            <td width="20%"><b>Posted Date Time</b></td>
                       </tr>
                        <tr>
                            <td>{{ $blog_details->meal }}</td>
                            <td>{{ $blog_details->cuisines }} </td>
                            <td>{{ ($blog_details->blog_veg==1)? 'Veg':'Non-Veg' }}</td>
                            <td>{{ ($blog_details->created_at) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-12" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#73879C !important; color:antiquewhite !important;"><th colspan="3" style="padding: 10px;"><center>Blog Story</center></th></tr>
                        <tr>
                            <td>{{ strip_tags($blog_details['blog_story']) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-12" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="3" style="padding: 10px;"><center>Cover Image</center></th></tr>
                        <tr>
                            <td><img src="{{ $blog_details['cover-pic'] }}"  height="400px" width="100%" style="padding: 10px;"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-12" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="2" style="padding: 10px;"><center>Blogger Details</center></th></tr>
                        <tr>
                           <td >Name:</td><td >{{ $user_details['name'] }}</td>
                           
                        </tr>
                        <tr>
                           <td >Email:</td><td >{{ $user_details['email'] }}</td>
                           
                        </tr>
                        <tr>
                           <td >Phone:</td><td>{{ $user['ud_mobile'] }}</td>
                           
                        </tr>
                        <tr>
                           <td >Birthdate:</td><td >{{ $user['ud_birthday'] }}</td>
                           
                        </tr>
                        <tr>
                           <td>Followers:</td><td>{{ $user['ud_followers'] }}</td>
                           
                        </tr>
                        <tr>
                            <td>Following:</td><td>{{ $user['ud_followings'] }}</td>
                        </tr>
                    </table>
                </div>
            </div>
           <table class="table" style="width: 97.7%;margin-left: 15px;">
                <tbody><tr class="block-header" style="background-color:#73879C !important; color:#ffff 	!important;"><th style="border-color: #73879C !important;"><center>Total Views : {{ $blog_details['blog_views'] }}</center></th></tr>
                 </tbody>
            </table>
		</div>
	</div>
	</div>
		
@endsection