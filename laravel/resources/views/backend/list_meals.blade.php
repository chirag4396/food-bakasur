@extends('backend.layouts.master')
@push('header')
<style type="text/css">
	.line_set
	{
		padding-top: 22px !important;
	}
	.panel_toolbox>li.active
	{
		background-color: #73879C !important; 
		color: #515356 !important;
		border-radius: 10px;
	}
</style>
<script src="{{ asset('backend/vendors/jquery/dist/jquery.min.js') }}"></script>
@endpush
@section('content')

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="col-sm-6">
				<h2>Users List </h2>
				</div>
				
				<div class="col-sm-6">
					<ul class="nav navbar-right panel_toolbox" >
						<li class=""><a href="{{ URL::to('/backend/dashboard') }}">Dashboard</a></li>
						<li><a>/</a></li>
						<li class="active"><a>Users</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<div class="table-responsive">
					<table class="table table-striped jambo_table bulk_action list_users">
						<thead>
							<tr class="headings">
								<th>
									<input type="checkbox" id="check-all" class="flat">
								</th>
								<th class="column-title">Photo</th>
								<th class="column-title">Name </th>
								<th class="column-title">Email </th>
								<th class="column-title">Mobile </th>
								<th class="column-title no-link last"><span class="nobr">Action</span>
								</th>
								<th class="bulk-actions" colspan="7">
									<a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
								</th>
							</tr>
						</thead>

						<tbody>
							@foreach($all_meals as $key=>$value)
							<tr class="even pointer">
								<td class="a-center line_set">
									<input type="checkbox" class="flat" name="table_records">
								</td>
								<td class="line_set">{{ $value->meal_title }}</td>
								
								<td class="line_set"><a href="{{ URL::to('/backend/meal/'.$value->meal_id) }}"><button class="btn btn-warning">Update</button></a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>


			</div>
		</div>
	</div>
<script type="text/javascript">
$(function () {
	    $('.list_users').DataTable({
	      "paging": true,
	      "ordering": true,
	      "info": true,
	      "autoWidth": false,
	      "aaSorting": [],
	      "aoColumns": [
	            { "bSortable": false },
	            { "bSortable": false },
	            null,
	            null,
	            null,
	           	{ "bSortable": false },
            ]

	    });
  	});
</script>
@endsection