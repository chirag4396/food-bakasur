@extends('backend.layouts.master')
@push('header')
<style type="text/css">
	.line_set
	{
		padding-top: 22px !important;
	}
	.panel_toolbox>li.active
	{
		background-color: #73879C !important; 
		color: #515356 !important;
		border-radius: 10px;
	}
</style>


@endpush
@section('content')

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="col-sm-6">
				<h2>Businesses List </h2>
				</div>
				
				<div class="col-sm-6">
					<ul class="nav navbar-right panel_toolbox" >
						<li class=""><a href="{{ URL::to('/backend/dashboard') }}">Dashboard</a></li>
						<li><a>/</a></li>
						<li class="active"><a>Businesses</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				
					@if(Session::has('message'))
			            		<div class="col-12">
									<div class="alert alert-danger alert-dismissible">
					                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					                  
					                  {{ Session::get('message') }}
					                </div>
								</div>
			            	@endif
			            	<div class="col-12 chnaged-status" style="display: none">
									<div class="alert alert-danger alert-dismissible">
					                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					                  
					                  <span class="sessintext"></span>
					                </div>
								</div>
			            	<div class="tablebody table-responsive">
                          	<table class="table table-striped jambo_table bulk_action user-business">
								<thead>
									<tr class="headings">
										
										<th class="column-title">Business Name</th>
										<th class="column-title">Payment Status </th>
										<th class="column-title">Business Type </th>
										<th class="column-title no-link last"><span class="nobr">Action</span>
										</th>
									</tr>
								</thead>

								<tbody>
									@foreach($businesses as $r=>$v)
										<tr>
											
											<td>{{ $v->b_name }}</td>
											<td>{{ ($v->b_payment_status==1)?'Paid':'Not Paid' }}</td>
											<td>{{ $v->business_type }}</td>
											
											<td><a href="{{ URL::to('/backend/user-business/'.$v->b_id) }}"><button class="btn btn-warning">View</button></a></td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
				</div>


			</div>
		</div>
	</div>
	@push('footer')
<script type="text/javascript">
	$('.user-business').dataTable({
	      "paging": true,
	      "ordering": true,
	      "info": true,
	      "autoWidth": false,
	      "aaSorting": [],
	      "aoColumns": [
	            null,
	            null,
	            null,
	           	{ "bSortable": false },
            ]

	    });
	setInterval("getNewRecords()",3000);
	function getNewRecords()
	{
		var ajaxURL="{{ URL::to('/backend/ajax-recipe-data') }}";
		//alert(ajaxURL);return false;
		 $.ajax({
		 	url:ajaxURL,
            type:'post',
            headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
            success:function(data){
            	//alert(data); false;
            	$('.table_data').replaceWith(data);
            
            }
         });
	}
	function changeStatus(id)
	{

		var ajaxURL="{{ URL::to('/backend/change-recipe-status') }}";
		//alert(ajaxURL);return false;
		 $.ajax({
		 	url:ajaxURL,
            type:'post',
            data:{id:id},
            headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
            success:function(data){
            	$('.chnaged-status').show();
            	$('.sessintext').html(data);
            	setInterval(location.reload(),5000);
            }
         });
	}
	
		    
	  
</script>
@endpush
@endsection
