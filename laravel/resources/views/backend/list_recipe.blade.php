@extends('backend.layouts.master')
@push('header')
<style type="text/css">
	.line_set
	{
		padding-top: 22px !important;
	}
	.panel_toolbox>li.active
	{
		background-color: #73879C !important; 
		color: #515356 !important;
		border-radius: 10px;
	}
</style>


@endpush
@section('content')

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="col-sm-6">
				<h2>Recipes List </h2>
				</div>
				
				<div class="col-sm-6">
					<ul class="nav navbar-right panel_toolbox" >
						<li class=""><a href="{{ URL::to('/backend/dashboard') }}">Dashboard</a></li>
						<li><a>/</a></li>
						<li class="active"><a>Recipes</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				
					@if(Session::has('message'))
			            		<div class="col-12">
									<div class="alert alert-danger alert-dismissible">
					                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					                  
					                  {{ Session::get('message') }}
					                </div>
								</div>
			            	@endif
			            	<div class="col-12 chnaged-status" style="display: none">
									<div class="alert alert-danger alert-dismissible">
					                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					                  
					                  <span class="sessintext"></span>
					                </div>
								</div>
			            	<div class="tablebody table-responsive">
                          	<table class="table table-striped jambo_table bulk_action user-recs" width="100%">
								<thead>
									<tr class="headings">
										
										<th width="25%">Title</th>
										<th width="15%">Meal </th>
										<th width="15%">Cuisines </th>
										<th width="15%">Food Type </th>
										<th  width="10%">Status </th>
										<th  width="20%"><center>Action</center></th>
									</tr>
								</thead>
								
								<tbody class="table_data">
									
									@foreach($recData as $b=>$v)
										<tr>
											
											<td>{{ $v->rec_title }}</td>
											<td>{{ $v->meal }}</td>
											<td>{{ $v->cuisines }}</td>
											<td>{{ ($v->rec_veg==1)? 'Veg':'Non-Veg' }}</td>
											<td>{{ ($v->rec_admin_status==1)?'Approved':'Disapproved' }}</td>
											<td><center><a href="{{ URL::to('/backend/user-recipe/'.$v->rec_id) }}"><button class="btn btn-warning">View</button></a><input type="button" name="" value="{{ ($v->rec_admin_status==1)?'Disapprove':'Approve' }}" class="{{ ($v->rec_admin_status==1)?'btn btn-danger':'btn btn-info' }}" style="margin-left: 20px;" onclick="changeStatus({{ $v->rec_id }})"></center></td>
										</tr>
									@endforeach

								</tbody>
								
							</table>
						</div>
				</div>


			</div>
		</div>
	</div>
	@push('footer')
<script type="text/javascript">
	$('.user-recs').dataTable({
	      "paging": true,
	      "ordering": true,
	      "info": true,
	      "autoWidth": false,
	      "aaSorting": [],
	      "aoColumns": [
	            null,
	            null,
	            null,
	            null,
	            null,
	           	{ "bSortable": false },
            ]

	    });
	setInterval("getNewRecords()",3000);
	function getNewRecords()
	{
		var ajaxURL="{{ URL::to('/backend/ajax-recipe-data') }}";
		//alert(ajaxURL);return false;
		 $.ajax({
		 	url:ajaxURL,
            type:'post',
            headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
            success:function(data){
            	//alert(data); false;
            	$('.table_data').replaceWith(data);
            
            }
         });
	}
	function changeStatus(id)
	{

		var ajaxURL="{{ URL::to('/backend/change-recipe-status') }}";
		//alert(ajaxURL);return false;
		 $.ajax({
		 	url:ajaxURL,
            type:'post',
            data:{id:id},
            headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
            success:function(data){
            	$('.chnaged-status').show();
            	$('.sessintext').html(data);
            	setInterval(location.reload(),5000);
            }
         });
	}
	
		    
	  
</script>
@endpush
@endsection
