@extends('backend.layouts.master')
@push('header')
<style type="text/css">
	.line_set
	{
		padding-top: 22px !important;
	}
	.panel_toolbox>li.active
	{
		background-color: #73879C !important; 
		color: #515356 !important;
		border-radius: 10px;
	}
</style>


@endpush
@section('content')

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<div class="col-sm-6">
				<h2>Recipe Comments List </h2>
				</div>
				
				<div class="col-sm-6">
					<ul class="nav navbar-right panel_toolbox" >
						<li class=""><a href="{{ URL::to('/backend/dashboard') }}">Dashboard</a></li>
						<li><a>/</a></li>
						<li class="active"><a>Recipe Comments</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				
					@if(Session::has('message'))
			            		<div class="col-12">
									<div class="alert alert-danger alert-dismissible">
					                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					                  
					                  {{ Session::get('message') }}
					                </div>
								</div>
			            	@endif
			            	<div class="col-12 chnaged-status" style="display: none">
									<div class="alert alert-danger alert-dismissible">
					                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					                  
					                  <span class="sessintext"></span>
					                </div>
								</div>
			            	<div class="tablebody table-responsive">
                          	<table class="table table-striped jambo_table bulk_action user-blogs" width="100%">
								<thead>
									<tr class="headings">
										
										<th width="25%">Commnets</th>
										<th width="25%">Recipe Title</th>
										
										<th  width="20%"><center>Action</center></th>
									</tr>
								</thead>
								
								<tbody class="table_data">
									
									@foreach($all_commments as $b=>$v)
										<tr>
											
											<td>{{ $v->rc_content }}</td>
											<td>{{ $v->rec }}</td>
											<td><center><a href="{{ URL::to('/backend/user-recipe/'.$v->rc_rec_id) }}"><button class="btn btn-warning">View</button></a><input type="button" name="" value="{{ ($v->rc_admin_status==1)?'Disapprove':'Approve' }}" class="{{ ($v->rc_admin_status==1)?'btn btn-danger':'btn btn-info' }}" style="margin-left: 20px;" onclick="changeStatus({{ $v->rc_id }})"></center></td>
										</tr>
									@endforeach

								</tbody>
								
							</table>
							{{ $all_commments->links() }}
						</div>
				</div>


			</div>
		</div>
	</div>
	@push('footer')
<script type="text/javascript">
	$('.user-blogs').dataTable({
	      "paging": false,
	      "ordering": true,
	      "info": true,
	      "autoWidth": false,
	      "aaSorting": [],
	      "aoColumns": [
	            null,
	            null,
	           	{ "bSortable": false },
            ]

	    });
	setInterval("getNewRecords()",1000);
	function getNewRecords()
	{
		var ajaxURL="{{ URL::to('/backend/ajax-recipecomments-data') }}";
		//alert(ajaxURL);return false;
		 $.ajax({
		 	url:ajaxURL,
            type:'post',
            headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
            success:function(data){
            	$('.table_data').replaceWith(data);
            		$('.user-blogs').dataTable( {
						retrieve: true,
		    			paging: false
				} );
            }
         });
	}
	function changeStatus(id)
	{

		var ajaxURL="{{ URL::to('/backend/change-recipecomments-status') }}";
		//alert(ajaxURL);return false;
		 $.ajax({
		 	url:ajaxURL,
            type:'post',
            data:{id:id},
            headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
            success:function(data){
            	$('.chnaged-status').show();
            	$('.sessintext').html(data);
            	setInterval(location.reload(),5000);
            }
         });
	}
	
		    
	  
</script>
@endpush
@endsection
