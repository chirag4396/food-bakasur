@extends('backend.layouts.master')
@push('header')
<style type="text/css">
    table td,th{
        border: 1px solid #858585;
        padding: 8px;
    }
</style>
@endpush
@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title" style="text-align: center;">
				<h3>Recipe Details</h3>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			
			<table class="table" style="width: 97.7%;margin-left: 15px;">
                <tbody><tr class="block-header" style="background-color:#73879C !important; color:#ffff 	!important;"><th style="border-color: #73879C !important;"><center>Recipe Title : {{ $rec_details['rec_title'] }}</center></th></tr>
                 </tbody>
            </table>
            <div class="col-sm-12" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="4" style="padding: 10px;"><center>Recipe Detail</center></th></tr>
                        <tr>
                            <td width="30%"><b>Meal Type </b></td>
                            <td width="30%"><b>Cuisine Type </b></td>
                            <td width="20%"><b>Food Type</b></td>
                            <td width="20%"><b>Posted Date Time</b></td>
                       </tr>
                        <tr>
                            <td>{{ $rec_details->meal }}</td>
                            <td>{{ $rec_details->cuisines }} </td>
                            <td>{{ ($rec_details->rec_veg==1)? 'Veg':'Non-Veg' }}</td>
                            <td>{{ ($rec_details->created_at) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-12" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#73879C !important; color:antiquewhite !important;"><th colspan="3" style="padding: 10px;"><center>Recipe Description</center></th></tr>
                        <tr>
                            <td>{{ strip_tags($rec_details['rec_description']) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-6" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="3" style="padding: 10px;"><center>Recipe Image</center></th></tr>
                        <tr>
                            <td><img src="{{ $rec_details['image'] }}"  height="209px" width="100%" style="padding: 10px;"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-6" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="2" style="padding: 10px;"><center>User Details</center></th></tr>
                        <tr>
                           <td >Name:</td><td >{{ $user_details['name'] }}</td>
                           
                        </tr>
                        <tr>
                           <td >Email:</td><td >{{ $user_details['email'] }}</td>
                           
                        </tr>
                        <tr>
                           <td >Phone:</td><td>{{ $user['ud_mobile'] }}</td>
                           
                        </tr>
                        <tr>
                           <td >Birthdate:</td><td >{{ $user['ud_birthday'] }}</td>
                           
                        </tr>
                        <tr>
                           <td>Followers:</td><td>{{ $user['ud_followers'] }}</td>
                           
                        </tr>
                        <tr>
                            <td>Following:</td><td>{{ $user['ud_followings'] }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-12" style="margin:10px 0 27px 0 !important;">
                <div class="table-responsive">
                    <table width="100%">
                        <tr class="block-header" style="background-color:#1ABB9C !important; color:antiquewhite !important;"><th colspan="3" style="padding: 10px;"><center>Recipe Steps</center></th></tr>
                     
                            @php $i=1@endphp
                            @foreach($recipe_steps as $rs=>$v)
                               <tr>
                            <td>Step {{ $i++ }}</td>
                            <td>{{ $v->rs_title }}</td>
                            <td><a onclick="Imageview('Step {{ $i}}','{{ $v->rs_rec_image }}','{{ $v->rs_title }}')"><button class="btn btn-warning">Image View</button></a></td>
                            <input type="hidden" value="{{ $v->rs_rec_image }}" id="Step{{ $i }}" class="steps">
                             </tr>
                            @endforeach
                       
                    </table>
                </div>
            </div>
		</div>
	</div>
	</div>
		
@endsection
<script type="text/javascript">
    function Imageview(step,image,title)
    {
        var str=step+' : '+title;
        $('.model-content').css({"width": "50%"});
            $('.modal-title').html(str);
            $('.modal-body').html('<center><img src='+image+'></img></center>');
            $("#myModal").modal({show: true});
    }
</script>