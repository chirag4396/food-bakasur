<div id="businesscomments-div"  style="position: relative;">
@foreach($businessComment as $b=>$businesscom)
<div class="media mb-4">
  	<img class="d-flex mr-3 rounded-circle" src="{{ $businesscom->cu_image }}" alt="" style="height: 50px;width: 50px;">
	  <div class="media-body">
	    <h5 class="mt-0">{{ $businesscom->comment_user }}</h5>
	    <p>{{ $businesscom->busc_content }}</p>
	  </div>
</div>
@endforeach
</div>