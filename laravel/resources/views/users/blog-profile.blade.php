@extends('users.layouts.master')
@push('header')
@endpush
@section('content')
<div class="main-top blog-main   " style="background: #eee;">
   <div class="container white blog-main-new">
      <div class="col-lg-4  col-xl-3 col-xs-12 main-left-edit ">
         <div class="col-lg-12 col-md-6 col-sm-6  " style="">
            <img class="img4" src="img/f1.jpg" alt="images/users/1470242246.jpg" title="Chirag Khatri">
         </div>
         <div class="col-lg-12   ">
            <h3 class="dark " style="text-align: center;"> <i class="fa fa-user" aria-hidden="true"></i> Jasitha udayakrishnan</h3>
         </div>
         <div class="col-xs-12 col-sm-5 col-lg-12 ">
            <button class="btn col-xs-5 col-md-5 col-lg-5  btn-follow  font-small">Follow</button>
            <button class="btn col-xs-7 col-md-7 col-lg-7  btn-follow  font-small"> 100 Following</button>
            <button class="btn col-xs-12  btn-follow  font-small">100 Coins</button>
         </div>
      </div>
      <!--middle starts-->
      <div class="col-lg-8 col-xs-12  col-mg-7 main-middle ">
         <div class="bd-example bd-example-tabs" role="tabpanel">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
               <li class="nav-item col-xs-12 col-sm-3 col-lg-3 ">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-expanded="true">About Me</a>
               </li>
               <li class="nav-item col-xs-12 col-sm-3 col-lg-3">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-expanded="false">Published Trips
                  </a>
               </li>
               <li class="nav-item col-xs-12 col-sm-2 col-lg-2">
                  <a class="nav-link" id="Drafts-tab" data-toggle="tab" href="#Drafts" role="tab" aria-controls="Drafts" aria-expanded="false">My Drafts
                  </a>
               </li>
               <li class="nav-item col-xs-12 col-sm-3 col-lg-3">
                  <a class="nav-link" id="Recent-tab" data-toggle="tab" href="#Recent" role="tab" aria-controls="Recent" aria-expanded="false">Recent Activities
                  </a>
               </li>
            </ul>
            <div class="tab-content " id="myTabContent">
               <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab" aria-expanded="true">
                  <div class="col-xs-12 col-lg-12">
                     <div class="col-xs-12 white edit">
                        <h3 class="dark " style="text-align: center; font-weight:bolder;"> About Me</h3>
                        <div class="border-line3"></div>
                        <div class="col-xs-12  border-bottom">
                           <div class="col-xs-12 col-lg-4 ">
                              <b>Myself </b>
                           </div>
                           <div class="col-xs-12 col-lg-8  ">
                              <p>I love to go out and Travel as far as possible from all these concrete forest and wanted to live under it. </p>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                        <div class="col-xs-12  border-bottom">
                           <div class="col-xs-12 col-lg-4 ">
                              <b>My Travel Favorites </b>
                           </div>
                           <div class="col-xs-12 col-lg-8  ">
                              <div class="btn-group btn-group-sm" role="group" aria-label="Small button group">
                                 <button type="button" class="btn btn-secondary">Adventure</button>
                                 <button type="button" class="btn btn-secondary">Beach</button>
                                 <button type="button" class="btn btn-secondary">Budget Travel</button>
                                 <button type="button" class="btn btn-secondary">Day Trip</button>
                                 <button type="button" class="btn btn-secondary">Nature</button>
                                 <button type="button" class="btn btn-secondary">Family Trip</button>
                                 <button type="button" class="btn btn-secondary">Food &amp; Drinks</button>
                                 <button type="button" class="btn btn-secondary">Historic</button>
                                 <button type="button" class="btn btn-secondary">Road Trip</button>
                                 <button type="button" class="btn btn-secondary">Wild Life</button>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                        <div class="col-xs-12">
                           <button class="btn btn-primary col-sm-6 col-lg-4 offset-lg-4 col-xs-6 ">Update Password</button>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab" aria-expanded="false">
                  <div class="col-xs-12 col-lg-12">
                     <div class="col-xs-12 col-md-6  col-lg-6">
                        <div class="col-xs-12  col-lg-12  trovi-card ">
                           <div class="col-xs-12 trovi-news">
                              <a href="#">
                              <img class="img3" src="img/shyam.jpg" height="40" width="40">
                              </a>
                              <div class=" pull-left">
                                 <p>
                                    <a href="#" class="trovi-link">Tanya.Korteling</a>
                                 </p>
                                 <div class="font-small">
                                    <span> 1250 Followers </span> |
                                    <span> 1000 Trips </span> 
                                 </div>
                              </div>
                           </div>
                           <div class="trovi-image">
                              <a href="#">
                                 <img src="img/img.jpg">
                                 <div class="image-overlay"></div>
                              </a>
                              <div class="clearfix"></div>
                           </div>
                           <div class="trovi-title">
                              Awesome Trip to lavasa  Trip to lavasa
                           </div>
                           <div class="col-md-4 col-lg-4 trovi-share ">
                              <ul>
                                 <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
                              </ul>
                           </div>
                           <div class="col-md-8 col-md-8   trovi-share no-padding">
                              <ul>
                                 <li><i class="fa fa-eye" aria-hidden="true"></i> <a class="font-small"> 11.2M views</a> </li>
                                 <li><i class="fa fa-clock-o" aria-hidden="true"></i> <a class="font-small">  60 min read</a> </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-12 col-md-6 col-lg-6">
                        <div class="col-xs-12  col-lg-12  trovi-card ">
                           <div class="col-xs-12 trovi-news">
                              <a href="#">
                              <img class="img3" src="img/shyam.jpg" height="40" width="40">
                              </a>
                              <div class=" pull-left">
                                 <p>
                                    <a href="#" class="trovi-link">Tanya.Korteling</a>
                                 </p>
                                 <div class="font-small">
                                    <span> 1250 Followers </span> |
                                    <span> 1000 Trips </span> 
                                 </div>
                              </div>
                           </div>
                           <div class="trovi-image">
                              <a href="#">
                                 <img src="img/img.jpg">
                                 <div class="image-overlay"></div>
                              </a>
                              <div class="clearfix"></div>
                           </div>
                           <div class="trovi-title">
                              Awesome Trip to lavasa  Trip to lavasa
                           </div>
                           <div class="col-md-4 col-lg-4 trovi-share ">
                              <ul>
                                 <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
                              </ul>
                           </div>
                           <div class="col-md-8 col-md-8   trovi-share no-padding">
                              <ul>
                                 <li><i class="fa fa-eye" aria-hidden="true"></i> <a class="font-small"> 11.2M views</a> </li>
                                 <li><i class="fa fa-clock-o" aria-hidden="true"></i> <a class="font-small">  60 min read</a> </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                  <div class="col-xs-12 col-lg-12">
                     <div class="col-xs-12 col-md-6  col-lg-6">
                        <div class="col-xs-12  col-lg-12  trovi-card ">
                           <div class="col-xs-12 trovi-news">
                              <a href="#">
                              <img class="img3" src="img/shyam.jpg" height="40" width="40">
                              </a>
                              <div class=" pull-left">
                                 <p>
                                    <a href="#" class="trovi-link">Tanya.Korteling</a>
                                 </p>
                                 <div class="font-small">
                                    <span> 1250 Followers </span> |
                                    <span> 1000 Trips </span> 
                                 </div>
                              </div>
                           </div>
                           <div class="trovi-image">
                              <a href="#">
                                 <img src="img/img.jpg">
                                 <div class="image-overlay"></div>
                              </a>
                              <div class="clearfix"></div>
                           </div>
                           <div class="trovi-title">
                              Awesome Trip to lavasa  Trip to lavasa
                           </div>
                           <div class="col-md-4 col-lg-4 trovi-share ">
                              <ul>
                                 <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
                              </ul>
                           </div>
                           <div class="col-md-8 col-md-8   trovi-share no-padding">
                              <ul>
                                 <li><i class="fa fa-eye" aria-hidden="true"></i> <a class="font-small"> 11.2M views</a> </li>
                                 <li><i class="fa fa-clock-o" aria-hidden="true"></i> <a class="font-small">  60 min read</a> </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-12 col-md-6  col-lg-6">
                        <div class="col-xs-12  col-lg-12  trovi-card ">
                           <div class="col-xs-12 trovi-news">
                              <a href="#">
                              <img class="img3" src="img/shyam.jpg" height="40" width="40">
                              </a>
                              <div class=" pull-left">
                                 <p>
                                    <a href="#" class="trovi-link">Tanya.Korteling</a>
                                 </p>
                                 <div class="font-small">
                                    <span> 1250 Followers </span> |
                                    <span> 1000 Trips </span> 
                                 </div>
                              </div>
                           </div>
                           <div class="trovi-image">
                              <a href="#">
                                 <img src="img/img.jpg">
                                 <div class="image-overlay"></div>
                              </a>
                              <div class="clearfix"></div>
                           </div>
                           <div class="trovi-title">
                              Awesome Trip to lavasa  Trip to lavasa
                           </div>
                           <div class="col-md-4 col-lg-4 trovi-share ">
                              <ul>
                                 <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
                              </ul>
                           </div>
                           <div class="col-md-8 col-md-8   trovi-share no-padding">
                              <ul>
                                 <li><i class="fa fa-eye" aria-hidden="true"></i> <a class="font-small"> 11.2M views</a> </li>
                                 <li><i class="fa fa-clock-o" aria-hidden="true"></i> <a class="font-small">  60 min read</a> </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="tab-pane fade" id="Drafts" role="tabpanel" aria-labelledby="Drafts-tab" aria-expanded="false">
                  <div class="col-xs-12 col-lg-12">
                     <div class="col-xs-12 col-md-6  col-lg-6">
                        <div class="col-xs-12  col-lg-12  trovi-card ">
                           <div class="col-xs-12 trovi-news">
                              <a href="#">
                              <img class="img3" src="img/f1.jpg" height="40" width="40">
                              </a>
                              <div class=" pull-left">
                                 <p>
                                    <a href="#" class="trovi-link">Tanya.Korteling</a>
                                 </p>
                                 <div class="font-small">
                                    <span> 1250 Followers </span> |
                                    <span> 1000 Trips </span> 
                                 </div>
                              </div>
                           </div>
                           <div class="trovi-image">
                              <a href="#">
                                 <img src="img/img.jpg">
                                 <div class="image-overlay"></div>
                              </a>
                              <div class="clearfix"></div>
                           </div>
                           <div class="trovi-title">
                              Awesome Trip to lavasa  Trip to lavasa
                           </div>
                           <div class="col-md-4 col-lg-4 trovi-share ">
                              <ul>
                                 <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
                              </ul>
                           </div>
                           <div class="col-md-8 col-md-8   trovi-share no-padding">
                              <ul>
                                 <li><i class="fa fa-eye" aria-hidden="true"></i> <a class="font-small"> 11.2M views</a> </li>
                                 <li><i class="fa fa-clock-o" aria-hidden="true"></i> <a class="font-small">  60 min read</a> </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-12 col-md-6  col-lg-6">
                        <div class="col-xs-12  col-lg-12  trovi-card ">
                           <div class="col-xs-12 trovi-news">
                              <a href="#">
                              <img class="img3" src="img/shyam.jpg" height="40" width="40">
                              </a>
                              <div class=" pull-left">
                                 <p>
                                    <a href="#" class="trovi-link">Tanya.Korteling</a>
                                 </p>
                                 <div class="font-small">
                                    <span> 1250 Followers </span> |
                                    <span> 1000 Trips </span> 
                                 </div>
                              </div>
                           </div>
                           <div class="trovi-image">
                              <a href="#">
                                 <img src="img/img.jpg">
                                 <div class="image-overlay"></div>
                              </a>
                              <div class="clearfix"></div>
                           </div>
                           <div class="trovi-title">
                              Awesome Trip to lavasa  Trip to lavasa
                           </div>
                           <div class="col-md-4 col-lg-4 trovi-share ">
                              <ul>
                                 <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
                                 <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
                              </ul>
                           </div>
                           <div class="col-md-8 col-md-8   trovi-share no-padding">
                              <ul>
                                 <li><i class="fa fa-eye" aria-hidden="true"></i> <a class="font-small"> 11.2M views</a> </li>
                                 <li><i class="fa fa-clock-o" aria-hidden="true"></i> <a class="font-small">  60 min read</a> </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="tab-pane fade" id="Recent" role="tabpanel" aria-labelledby="Recent-tab" aria-expanded="false">
                  <div class="col-xs-12 col-lg-12">
                     <div class="col-xs-12 col-lg-12 trovi-point white">
                        <ul>
                           <li>Biggest event company in Pune to organize Exhibition</li>
                           <li>Idea and innovation to give better customer experience</li>
                           <li>Even better is to search for Property by property-type like</li>
                           <li>Even better is to search for Property by property-type like</li>
                           <li>Even better is to search for Property by property-type like</li>
                        </ul>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

