@extends('users.layouts.master')

@section('content')
	<section class="food-tabs">
    <div class="main-top blog-main" >

      <div class="container white  ">

        <div class="row padding">

          <!-- Post Content Column -->
          <div class="col-lg-8">

            <!-- Title -->
            <h1 class="mt-4">{{ $blogData['blog_title'] }}</h1>

            <!-- Author -->


            <hr>
            <div class="col-xs-12 trovi-news no-padding bg-grey">

              <div class="text-feedevent pull-left col-sm-6 col-xs-12 col-lg-5 col-xl-7 b-r">
                <a href="#">
                  <img class="img4" src="{{ $userDetails['ud_profile_img'] }}" width="40" height="40">
                </a>
                <p>
                  <a href="#" class="trovi-link">{{ $userData['name'] }}</a>
                </p>
                <div class="font-small" data-toggle="modal" data-target="#trips">
                  <a href="#"><span> {{ $blogCount }} blogs</span></a>

                </div>
              </div>

              <div class="  col-xs-12 offset-xs-1 offset-sm-0 col-sm-3 col-lg-4  col-xl-3  trovi-share no-padding">
                <ul>
                  <li><i class="fa fa-eye" aria-hidden="true"></i> <a class="font-small"> {{ $blogData['blog_visitor_count'] }} views</a> </li>
                  {{-- <li><i class="fa fa-clock-o" aria-hidden="true"></i> <a class="font-small">  60 min read</a> </li> --}}

                </ul>
              </div>
              <div class="col-xs-12 col-lg-2 col-sm-3  pull-right col-xl-2 ">
                @if(Session::get('sessionData.id')!=$userData['id'])
                        <input type="button" name="" value="Follow" onclick="postFollow({{ Session::get('sessionData.id') }},{{ $userData['id'] }})" class="btn  btn-follow col-xs-12 col-sm-12   font-small" >
                   {{--  <button class="btn  btn-follow col-xs-12 col-sm-12   font-small" style="font-weight: bolder;" onclick="postFollow({{ Session::get('sessionData.id') }},{{ $userData['id'] }})">Follow</button> --}}
                @elseif($followStatus=='No' && $followingStatus=='No')
                    <input type="button" name="" value="Follow" onclick="postFollow({{ Session::get('sessionData.id') }},{{ $userData['id'] }})" class="btn  btn-follow col-xs-12 col-sm-12   font-small" >
                @elseif($followStatus=='Yes' && $followingStatus=='No')
                    <button class="btn  btn-follow col-xs-12 col-sm-12   font-small" style="font-weight: bolder;">Followed</button>
                  @elseif($followStatus=='No' && $followingStatus=='Yes')
                    <button class="btn  btn-follow col-xs-12 col-sm-12   font-small" style="font-weight: bolder;">Following</button>
                    @elseif($followStatus=='Yes' && $followingStatus=='Yes')
                    <button class="btn  btn-follow col-xs-12 col-sm-12   font-small" style="font-weight: bolder;">Followed</button>
                @endif

              </div>
            </div>

            <div class="clearfix"></div>
            <hr>
            <div class="blog">
             <p>Posted on {{ $blogData['created_at']  }}</p>
             <div class="col-xs-12 col-sm-6 col-md-5 pull-right no-padding"> 

             {{--  <button class="btn button-fb col-xs-6 col-md-5">
                <span class="icon-social2 text-center"><i class="fa fa-facebook-square" aria-hidden="true"></i></span>
                <span class="text-social text-center"> Share</span>
              </button>
              <button class="btn button-twi col-xs-5 col-md-5">
                <span class="icon-social2 text-center"><i class="fa fa-twitter-square" aria-hidden="true"></i></span>
                <span class="text-social text-center"> Share</span>
              </button> --}}
            </div>
          </div>
          <hr>

          <!-- Preview Image -->
          <img class="img-fluid rounded" src="{{ ($blogData['blog_cover_img']=='No Image')? "http://placehold.it/900x300" :  $blogData['blog_cover_img'] }}"  alt="" style="width: 100%;height: 300px;">

          <hr>

          <!-- Post Content -->
          <p class="lead">{{ strip_tags($blogData['blog_story']) }}</p>

          

         {{--  <p>After her father had a heart attack, Erin Alderson (whose initials are ELLA) broke up with fast food and all processed meat. Her blog features seasonal vegetarian recipes that are pantry-inspired. Despite ditching Big Macs, Alderson doesn't believe in being obsessive or counting calories (her philosophy is simply to exercise and eat well). Given the results—beautifully photographed whole foods we want to gobble up immediately (curried vegetarian meatballs, we're looking at you!)—it's easy to see why her approach works.</p> --}}

          {{-- <blockquote class="blockquote">
            <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
            <footer class="blockquote-footer">Someone famous in
              <cite title="Source Title">Source Title</cite>
            </footer>
          </blockquote> --}}
          <div class="clearfix"></div>
          {{-- <div class="col-xs-12 ">

            <p class="travel-tag">Tags:
              <a>#​Food to Glow</a> <a>#​Blissful Basil</a> <a># ​Vegan in ﻿﻿﻿the﻿﻿﻿ ﻿﻿﻿Freezer</a> <a>#India is known for Its more than 7500 km coastline. Here are top 8 beaches that you really shouldn't miss out on.1.Candolim</a> <a># </a> </p>
            </div> --}}
            <div class="clearfix"></div> 


           {{--  <div class="col-xs-12 col-lg-8 offset-lg-2 abt-author">
              <h4 class="head">More about the Author</h4>
              <div class="border-line3"></div>
              <div class="col-lg-6" style="border-right:1px dashed #ccc;">
                <a href="account.php?uid=83" title="View Jasitha Udayakrishnan Profile">
                  <img class="img3" src="img/f1.jpg" data-pin-nopin="true">
                </a>
              </div>
              <div class="col-lg-6">
                <h3> 
                  <a style="text-decoration: none;" href="account.php?uid=83" title="View Jasitha Udayakrishnan Profile"> Jasitha Udayakrishnan</a>
                </h3>
                <button class="btn  btn-follow col-xs-12 col-sm-12 col-lg-6   font-small" style="font-weight: bolder;">Follow</button>
              </div>

            </div> --}}
            <div class="clearfix"></div> 










            <hr>

            <!-- Comments Form -->
            <div class="col-12 err_comment" style="display: none">
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  
                    <span class="err_msg_commet"></span>
                </div>
            </div>
            <div class="card my-4">
              <h5 class="card-header">Leave a Comment:</h5>
              <div class="card-body">
                <form method="post" action="" id="commentform">
                    <input type="hidden" name="c_user_id" value="{{ Session::get('sessionData.id') }}">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                    <input type="hidden" value="{{ $blogData['blog_id'] }}" name="c_blog_id" />
                  <div class="form-group">
                    <textarea class="form-control" rows="3" name="c_content"></textarea>
                  </div>
                  <input  type="button" class="btn btn-primary" value="Submit" onclick="postComment()">
                </form>
              </div>
            </div>

            <!-- Single Comment -->
            <section class="blogs_comments"> 
                @include('users.blog-comments')
            </section>
            <!-- Comment with nested comments -->
            {{-- <div class="media mb-4">
              <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
              <div class="media-body">
                <h5 class="mt-0">Commenter Name</h5>
                <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>

                <div class="media mt-4">
                  <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                  <div class="media-body">
                    <h5 class="mt-0">Commenter Name</h5>
                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                  </div>
                </div>

                <div class="media mt-4">
                  <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                  <div class="media-body">
                    <h5 class="mt-0">Commenter Name</h5>
                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                  </div>
                </div>

              </div>
            </div> --}}

          </div>

          <!-- Sidebar Widgets Column -->
          <div class="col-lg-4 col-xl-4  col-mg-5  col-xs-12">

            <h3 class="head">  Top Blogs</h3>
            <div class="border-line3"></div>
            @foreach($topBloggers as $tb=>$tbv)
            <div class="col-xs-12 col-sm-6 col-lg-12 col-md-6 trovi-card ">

              <div class="col-xs-12 trovi-news">
                <a href="#">
                  <img class="img3" src="{{ $tbv['userDetails']['ud_profile_img'] }}" width="40" height="40">
                </a>
                <div class=" pull-left">
                  <p>
                    <a href="#" class="trovi-link">{{ $tbv['userData']['name'] }}</a>
                  </p>
                  <div class="font-small">
                    <span> {{ $tbv['userDetails']['ud_followers'] }} Followers </span> |
                    <span> {{ $tbv['userDetails']['ud_followings'] }} Following </span> 


                  </div>
                </div>

              </div>



              <div class="trovi-image">
                <a href="#">
                  <img src="{{ $tbv['blog_cover_img'] }}">
                  <div class="image-overlay"></div>


                </a>
                <div class="clearfix"></div>
              </div>
              <div class="trovi-title">
               {{ $tbv['blog_title'] }}
             </div>

             {{-- <div class="col-md-4 col-lg-4 trovi-share ">
              <ul>
                <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
                <li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
                <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
              </ul>
            </div> --}}
            <div class="col-md-8 col-md-8   trovi-share no-padding">
              <ul>
                <li><i class="fa fa-eye" aria-hidden="true"></i> <a class="font-small"> {{ $tbv['blog_visitor_count'] }} views</a> </li>
                

              </ul>
            </div>
          </div>

            @endforeach


          
          <div class="clearfix"></div>








        </div>

      </div>
      <!-- /.row -->

    </div>
  </div>
</section>
@endsection
@push('footer')
    <script type="text/javascript">
        function postComment(){

            var url="{{ URL::to('/post-blogComment') }}";
          
                var fd = new FormData($('#commentform')[0]);
              
                 $.ajax({
                    url:url,
                    type:'post',
                    data:fd,
                    processData : false,
                    contentType : false,
                    headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    success:function(data){
                        alert(data);
                       if(data==0)
                        {
                             $('#myModal13').show();
                            $('.alert').show();
                            $('.err_msg').html('First you should login and register in our website.');
                            setTimeout(function() {
                                    $(".alert").hide()
                                }, 3000);
                         }
                        else
                        {
                            $('.err_comment').show();
                            $('.err_msg_commet').html('Commnet posetd successfully,wait for comment approval.');
                            $('.blogs_comments').html(data);
                            $('#commentform').trigger("reset"); 
                            setTimeout(function() {
                                    $(".alert").hide()
                                }, 3000);
                        }
                        
                    }
                 });
        }

        function postFollow(sessionId,bloguserid)
        {
            var path="?sessionId="+sessionId+"&bloguserId="+bloguserid;
            var url="{{ URL::to('/postfollow') }}";
            $.ajax({
                    url:url,
                    type:'post',
                    data:{sessionId:sessionId,bloguserid:bloguserid}
                    headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data){
                      alert(data);
                    },
                    error: function (data) {
                          console.log(data);
                      }
                 });
        }
    </script>
@endpush