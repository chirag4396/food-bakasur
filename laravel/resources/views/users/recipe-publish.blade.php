<div id="recipe-publish" class="col-sm-12"  style="position: relative;">
	@foreach($recepieDatapublish as $r=>$v)
		
		<div class="col-lg-6 col-md-6 col-sm-6 ">
	        <div class="card border portfolio-item food-card">
	           <a href="{{ URL::To('recipe-detail/'.$v->rec_id) }}"><img class="card-img-top" src="{{ $v->rec_img_path }}" alt=""></a>
	           <div class="card-body ">
	              <h4 >
	                 <a href="{{ URL::To('recipe-detail/'.$v->rec_id) }}">{{ $v->rec_title }}</a>
	              </h4>
	              <ul>
	              	@php $i=0 @endphp
	                 @foreach($v->ingrediants as $ri )
	                 @if($i!=4)
	                 	<li>{{ $ri->ri_title }}</li>
	                 	@php $i++ @endphp
	                 @endif
	                 @endforeach
	              </ul>
	              <div class="desc" >
	              <p class="rec" style="height: auto; ">{{ str_limit($v->rec_description,200) }}<a href="{{ URL::To('recipe-detail/'.$v->rec_id) }}" style="float: right;" >Read more</a><a href="{{ URL::to('/recipe-update/'.$v->rec_id) }}" style="padding-left: 50px; ">Edit</a></p></div>
	             
	              
	           </div>
	        </div>
	     </div>
		 
     @endforeach
     
</div><br>
<div id="paginationforrecipe" class="row" style="float: right;margin-right: 15px;">{{ $recepieDatapublish->render() }}</div>