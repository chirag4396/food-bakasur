<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
   <title>Template 9</title>
   <style type="text/css">
   @font-face {
      font-family: 'Roboto';
      font-style: normal;
      font-weight: 100;
      src: local('Roboto Medium'), local('Roboto-Medium'), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2');
      unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
   }

   body{
      overflow-x: hidden;
      font-family: 'Roboto' !important;
      font-weight: 100;
   }  

   #outlook a {padding:0;}
   body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
   .ExternalClass {width:100%;} 
   .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} 
   #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
   img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
   a img {border:none;}
   .image_fix {display:block;}
   p {margin: 0px 0px !important;}
   table td {border-collapse: collapse;}
   table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
   a {color: #9ec459;text-decoration: none;text-decoration:none!important;}
   /*STYLES*/
   table[class=full] { width: 100%; clear: both; }
   /*IPAD STYLES*/
   
   @media only screen and (max-width: 800px) {         
      table[class=devicewidth] {width: 90important;text-align:center!important;}         
   }
   /*IPHONE STYLES*/
   @media only screen and (max-width: 480px) {        
      table[class=devicewidth] {width: 95% !important;text-align:center!important;}
   }
</style>
</head>
<body>

   <!-- start textbox-with-title -->
   <table width="100%" bgcolor="#e8eaed" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
      <tbody>
         <tr>
            <td>
               <table width="60%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                  <tbody>
                     <tr>
                        <td width="100%" style="padding: 80px 0 60px;">
                           <table bgcolor="#ffffff" width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                              <tbody>
                                 <!-- Spacing -->
                                 <tr>
                                    <td width="100%" height="20"></td>
                                 </tr>
                                 <!-- Spacing -->                              
                                 <tr>
                                    <td>
                                       <table width="80%" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                          <tbody>  
                                             <tr>
                                                <td width="100%">
                                                   <table bgcolor="#e8eaed" width="100%" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                                      <tbody>
                                                         <tr>
                                                            <!-- start of image -->
                                                            <td align="center" style="background-color: #fff;">
                                                               <a target="_blank" href="#"><img border="0" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none; width: auto; height: auto; margin-top: -50px;border: 2px solid #3860ca;
                                                               border-radius: 50px;" src="{{ asset('img/logo.jpg') }}" class="logo"></a>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <!-- end of image -->
                                                </td>
                                             </tr>
                                             <!-- content -->
                                             <!-- Spacing -->
                                             <tr>
                                                <td width="100%" height="50"></td>
                                             </tr>
                                             <!-- Spacing -->
                                             <tr>
                                                <td style=" font-size: 17px; color: #333333; text-align:center;line-height: 24px;">
                                                   Hello <span style="font-size: 18px; color: #279b66; font-weight: 100;">{{ $username }}...</span> <br><br>
                                                   Thank you for choosing <span style="font-size: 18px; color: #279b66; font-weight: 100;">Food Bakasur</span>. <br><br>
                                                   Your {{ $b_name }} has Registered with us please make payment to publish your business on website.
                                                </td>
                                             </tr>
                                             <!-- End of content -->
                                             <!-- Spacing -->
                                             <tr>
                                                <td width="100%" height="50"></td>
                                             </tr>
                                             <!-- Spacing -->
                                             <!-- button -->
                                             <tr>
                                                <td style=" font-size: 14px; font-weight:100; color: #333333; text-align:left;line-height: 24px;">
                                                   <a href="#" style="color: #fff; text-decoration: none; font-weight: 500; display: block; background-color: #3860ca; width: 200px; margin: 0 auto; text-align: center; padding: 13px 0; font-size: 18px;border-radius: 50px;">Make Payment</a>
                                                </td>
                                             </tr>
                                             <!-- /button -->
                                             <!-- Spacing -->
                                             <tr>
                                                <td width="100%" height="20"></td>
                                             </tr>
                                             <!-- Spacing -->
                                             <tr>
                                                <td style=" font-size: 17px; color: #333333; text-align:center;line-height: 24px;">
                                                   Thank you for choosing <span style="font-size: 18px; color: #279b66; font-weight: 100;">Food Bakasur</span>.
                                                </td>
                                             </tr>
                                             <!-- Spacing -->
                                             <tr>
                                                <td width="100%" height="50"></td>
                                             </tr>
                                             <!-- Spacing -->

                                          </tbody>
                                       </table>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
      </tbody>
   </table>
   <!-- end of textbox-with-title -->

</body>
</html>