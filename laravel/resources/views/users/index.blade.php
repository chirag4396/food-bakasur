@extends('users.layouts.master')
@push('header')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endpush
@section('content')
@include('users.layouts.login-register');
<div class="slider">
   <figure>
      <img src="{{ asset('img/banner/55.jpg') }}">
      <img src="{{ asset('img/banner/11.jpg') }}">
      <img src="{{ asset('img/banner/33.jpg') }}">
      <img src="{{ asset('img/banner/44.jpg') }}">
      <img src="{{ asset('img/banner/22.jpg') }}">
   </figure>
   <div class="town-main ">
      <div class="container">
         <div class="col-sm-12 ">
            <h1>Order Delicious food online from the best restaurants near you!!</h1>
            <div class="">
            	<div class="err_box" style="display: none;background:rgba(255,255,255,.5);margin-bottom: 10px;border-radius: 3px;">
            		<span class="err_text" style="color: red;font-size: 20px;text-align: center;padding-left: 10px;"></span>
            	</div>
            	@if(Session::has('message'))
            		<div class="col-12">
						<div class="alert alert-danger alert-dismissible">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                  
		                  {{ Session::get('message') }}
		                </div>
					</div>
            	@endif
               <div class=" Search-bar col-sm-12">
                  <div class="input-group">
                     <form id="searchBar" method="post" action="{{ URL::to('/search_result') }}">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                        <div class="city">
                           <input type="hidden" name="search_city" id="hiddenCity">
                           <div type="button" id="searchCity" class="btn city-btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              Select City 
                           </div>
                           <ul class="dropdown-menu" id="cityBox">
                              <li id="searchCity-any" data-value="Any">Any</li>
                              <li id="searchCity-2499" data-value="Aurangabad">Aurangabad</li>
                              <li id="searchCity-2763" data-value="Pune">Pune</li>
                           </ul>
                        </div>
                        <div class="searchbar">
                           <input type="text" id="searchBox" data-value="" name="search_box" placeholder="Search your food online ..." autocomplete="off">
                        </div>
                        <div class="scrollbar style-4 col-xs-12 no-padding" style="display: none;">
                           <div class="my-searchlist">
                              <ul>
                                 <div id="back_result">
                                    <li class="selector"></li>
                                    {{--  
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Garacharma, Andaman and Nicobar Islands- India">Garacharma</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Gaddiannaram, Andhra Pradesh- India">Gaddiannaram</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Gajapathinagaram, Andhra Pradesh- India">Gajapathinagaram</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Gajularega, Andhra Pradesh- India">Gajularega</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Gajuvaka, Andhra Pradesh- India">Gajuvaka</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Gannavaram, Andhra Pradesh- India">Gannavaram</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Garacharma, Andhra Pradesh- India">Garacharma</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Garimellapadu, Andhra Pradesh- India">Garimellapadu</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Giddalur, Andhra Pradesh- India">Giddalur</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Godavarikhani, Andhra Pradesh- India">Godavarikhani</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Gopalapatnam, Andhra Pradesh- India">Gopalapatnam</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Gopalur, Andhra Pradesh- India">Gopalur</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Gorrekunta, Andhra Pradesh- India">Gorrekunta</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Gudivada, Andhra Pradesh- India">Gudivada</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Gudur, Andhra Pradesh- India">Gudur</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Guntakal, Andhra Pradesh- India">Guntakal</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Guntur, Andhra Pradesh- India">Guntur</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Guti, Andhra Pradesh- India">Guti</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Gauripur, Assam- India">Gauripur</li>
                                    <li onclick="document.getElementById('search').value = this.id;$('div.style-4').css({'display':'none'});" id="Goalpara, Assam- India">Goalpara</li>
                                    --}}
                                 </div>
                              </ul>
                              <div class="force-overflow"></div>
                           </div>
                        </div>
                        <div class="searchbtn">			
                           <a href="#"><button class="btn btn-danger search-btn" type="button" id="searchBtn" onclick="searchlist()"><i class="fa fa-search" aria-hidden="true"></i>
                           </button>		</a>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="clearfix"></div>
<div class="container">
   <h1 class="head">Popular Categories</h1>
   <div class="clearfix"></div>
   <div class="border-line3"></div>
   <div class="container festi-newlist ">
      <div class="col-md-4 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p>
               <img src="{{ asset('img/1.png') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Caterers</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p >
               <img src="{{ asset('img/2.png') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Cook</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p>
               <img src="{{ asset('img/3.png') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Restaurants</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p >
               <img src="{{ asset('img/4.png') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Nutrition</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p>
               <img src="{{ asset('img/5.png') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Hotels</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p>
               <img src="{{ asset('img/6.png') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Food Launching</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
   </div>
</div>
<div class="clearfix"></div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 festi-create res-padding">
   <div class="container">
      <div class="col-xs-12 res-padding ">
         <h1 class="head">Order Delicious food online</h1>
         <div class="container res-padding">
            <p>Bring people together, or turn your passion into a business. r, or turn your passion into a business. 
               gives you everything you need to host your best event yet.
            </p>
         </div>
         <button class="btn">Find your flavour!! </button>
      </div>
   </div>
</div>
<div class="container">
   <h1 class="head">Top Profiles</h1>
   <div class="clearfix"></div>
   <div class="border-line"></div>
   <div class="container festi-newlist ">
      <div class="row about-margin">
         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 read-more outer-div-right">
            <div class="inner-div">
               <img src="{{ asset('img/home1.png') }}" alt="Essential oils">
               <h4>Essential oils</h4>
               <p>
                  Essential Oils are obtained by steam distillation, and represent the aroma of spices.
               </p>
            </div>
            <!--end of inner div-->
         </div>
         <!--outer div-right -->
         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 read-more outer-div-center">
            <div class="inner-div">
               <img src="{{ asset('img/home2.png') }}" alt="Natural Food Colors">
               <h4>Natural Food Colors</h4>
               <p>
                  Stay natural and healthy with our complete set of natural food colors.
               </p>
            </div>
            <!--end of inner div-->
         </div>
         <!--outer div-center -->
         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 read-more outer-div-left">
            <div class="inner-div">
               <img src="{{ asset('img/home1.png') }}" alt="Oleoresins">
               <h4>Oleoresins</h4>
               <p>
                  Variety of spice extracts to enhance the right flavor to your products.
               </p>
            </div>
            <!--end of inner div-->
         </div>
         <!--outer div-left -->
      </div>
      <div class="clearfix"></div>
   </div>
</div>
<div class="clearfix"></div>
<div class="">
   <h1 class="head">Popular Recepies</h1>
   <div class="clearfix"></div>
   <div class="border-line"></div>
   <div class=" res-bg ">
      <div class="col-md-3 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p>
               <img src="{{ asset('img/11.jpg') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Caterers</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p >
               <img src="{{ asset('img/12.jpg') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Cook</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p>
               <img src="{{ asset('img/13.jpg') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Restaurants</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p >
               <img src="{{ asset('img/14.jpg') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Nutrition</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p >
               <img src="{{ asset('img/14.jpg') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Nutrition</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p >
               <img src="{{ asset('img/14.jpg') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Nutrition</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p >
               <img src="{{ asset('img/14.jpg') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Nutrition</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12 res-padding ">
         <div class="cuadro_intro_hover " style="background-color:#cccccc;">
            <p >
               <img src="{{ asset('img/14.jpg') }}" class="img-responsive" alt="">
            </p>
            <div class="caption">
               <div class="blur"></div>
               <div class="caption-text">
                  <h3 style="border-top:2px solid white; border-bottom:2px solid white; padding:10px;">Nutrition</h3>
                  <p>Loren ipsum dolor si amet ipsum dolor si amet ipsum dolor...</p>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
   </div>
</div>
<div class="clearfix"></div>
<div class="container col-xs-12 col-sm-12 col-md-12 col-lg-12 deff res-padding ">
   <h1 class="head">What Make us Different </h1>
   <div class="border-line"></div>
   <div class="clearfix"></div>
   <div class="container">
      <div class="col-xs-12 col-lg-5">
         <div class="d-cntnr2-img"></div>
      </div>
      <div class="col-xs-12 col-lg-7">
         <p>
            Bring people together, or turn your passion into a business. r, or turn your passion into a business. gives you everything you need to host your best event yet.We follow ISO 9001: 2008 standard for Quality assurance and ISO 22000: 2005 for Food Safety assurance. We adopt latest technologies for the extraction processes. Each process is under the supervision of Total Quality Management approach to ensure critical care for each product.
         </p>
      </div>
   </div>
</div>
<div class="clearfix"></div>
@endsection
@push('footer')
<script type="text/javascript">
   var data = [
     {
         id: 0,
         text: 'enhancement'
     },
     {
         id: 1,
         text: 'bug'
     },
     {
         id: 2,
         text: 'duplicate'
     },
     {
         id: 3,
         text: 'invalid'
     },
     {
         id: 4,
         text: 'wontfix'
     }
   ];
   
   $(".js-example-data-array").select2({
   data: data
   });
 //   setTimeout(function() {
	// 	$(".err_box").hide()
	// }, 3500);
 //   setTimeout(function() {
	// 	$(".alert").hide()
	// }, 3500);
   function searchlist()
   {
   		$searchbox_val=$('#searchBox').val();
   		if($searchbox_val=='')
   		{
   			$('.err_box').show();
   			$('.err_text').html("Enter item to search..");
   		}
   		else
   		{
   			$('#searchBar').submit();
   		}
   }
</script>
@endpush

