@extends('users.layouts.master')

@section('content')
	<div class="clearfix"></div>
      <br>
      <br>
      <br>
	 <section class="mt">
         <div class="container">
            <div class="row">
               <div class="col-md-9 col-xs-12 mt">
               		@if($recData['rec_img_path']!='No Image')
                  		<img src="{{ $recData['rec_img_path'] }}" class="image-responsive" height="" width="300px" height="200px" >
                  	@endif
                  <h1 class="mt">{{ $recData['rec_title'] }}</h1>
                  <p class="mt">Posted On.. <span><b>{{  $recData['created_at'] }}</b></span></p>
                  <P>
                    {{ $recData['rec_description'] }}
                  </P>
               </div>
               <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class=" well" style="border: 1px solid #ddd; background-color: #fff;">
                     <div class="useravatar">
                        <img alt="" src="{{ $userDetails['ud_profile_img'] }}" style="width:100%;">
                        <div style="text-align:center; " ><i class="fa fa-user" style="font-size: 16px; margin-top: 10px; margin-bottom: 10px;">  {{ $userData['name'] }}</i></div>
                        <a class="btn btn-primary" style="text-align:center; display:block;margin:0 auto; ">{{ ($userDetails['ud_followers']=='')? '0' :$userDetails['ud_followers'] }} Followers</a>
                     </div>
                     <div class="card-info" style="padding: 15px;margin-top: 10%; background-color: #FF1253;color: #fff;"> <span class="card-title" style="text-align:center;  display:block;margin:0 auto; ">{{ ($userDetails['ud_followings']=='')?'0':$userDetails['ud_followings'] }} Following </span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-8 col-sm-8 col-xs-12">
                  <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
                      @if(count($recIngre)>0)
                     <div class="btn-group" role="group">
                        <button type="button" id="stars" class="btn btn-primary" href="#tab1" style="width: 160px;" data-toggle="tab">
                           <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                           <div class="hidden-xs">Ingredients</div>
                        </button>
                     </div>
                     @endif
                     @if(count($recSteps)>0)
                     <div class="btn-group" role="group">
                        <button type="button" id="favorites" class="btn btn-default" href="#tab2" style="width: 160px;" data-toggle="tab">
                           <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                           <div class="hidden-xs">Steps</div>
                        </button>
                     </div>
                     @endif
                     @if($recData['rec_tip']!='')
                     <div class="btn-group" role="group">
                        <button type="button" id="following" class="btn btn-default" style="width: 160px;" href="#tab3" data-toggle="tab">
                           <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                           <div class="hidden-xs">Tip/Note</div>
                        </button>
                     </div>
                     @endif
                  </div>
                  <div class="well" style="border: 1px solid #ddd; background-color: #fff;">
                     <div class="tab-content">
                      @if(count($recIngre)>0)
                        <div class="tab-pane fade in active" id="tab1">
                           <div class="row mt">
                              <h4 class="head" style="font-size: 20px">Ingredient</h4>
                              <div class="border-line3"></div>
                              @foreach($recIngre as $ri=>$rin)
                              <p style="text-align: center;">{{ $rin->ri_title }}</p>
                              @endforeach
                           </div>
                        </div>
                        @endif
                        <div class="tab-pane fade in" id="tab2">
                           <div class="row mt">
                           	@php $i=1 @endphp
                           	@foreach($recSteps as $r=>$rs)
                              <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 10px;">
                                 <a href="#">
                                 	<div class="col-xs-12 col-sm-12 col-md-3" >
                                    	<img src="{{ $rs->rs_rec_image }}" class="img-responsive img-box img-thumbnail" style="width: 100px;height: 80px"> 
                                	</div>
                                	<div class="col-xs-12 col-sm-12 col-md-8" style="border-bottom: 1px solid gray">
                                    <p>Step {{ $i }}</p>
                                    <p>{{ $rs->rs_title }}</p>
                                		</div>
                                 </a>
                              </div>
                              
                             @php $i++ @endphp
                              @endforeach
                           </div>
                        </div>
                        <div class="tab-pane fade in" id="tab3">
                           <p>{{( $recData['rec_tip']!='')? $recData['rec_tip']:'No any tip' }}</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 err_comment" style="margin-top:10px;display:none">
		                <div class="alert alert-danger alert-dismissible">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		                  
		                    <span class="err_msg_commet"></span>
		                </div>
    		    	</div>
	            	<div class="card my-4">
	          			<h5 class="card-header">Leave a Comment:</h5>
	          				<div class="card-body">
	            				<form method="post" action="" id="commentform">
	                				<input type="hidden" name="rc_cuser_id" value="{{ Session::get('sessionData.id') }}">
	                				<input name="_token" type="hidden" value="{{ csrf_token() }}" />
	                				<input type="hidden" value="{{ $recData['rec_id'] }}" name="rc_rec_id" />
	              					<div class="form-group">
	                						<textarea class="form-control" rows="3" name="rc_content"></textarea>
                                            <input  type="button" id="postcomment" class="btn btn-primary" value="Submit" onclick="postComment()" style="margin-top:10px">
	              					</div>
	              						
	            				</form>
	          				</div>
	        		</div>

	        <!-- Single Comment -->
		        	<section class="recipe-comments"> 
		            	@include('users.recipe_comments')
		        	</section>	
               </div>
            </div>
         </div>
      </section>
@endsection
@push('footer')
    <script type="text/javascript">
        function postComment(){
            $('#postcomment').hide();
             $('.form-group').append('<button class="buttonload btn btn-primary" style="margin-top:10px"><i class="fa fa-spinner fa-spin"></i>Processing</button>');
            var url="{{ URL::to('/post-recipeComment') }}";
          
                var fd = new FormData($('#commentform')[0]);
                
                 $.ajax({
                    url:url,
                    type:'post',
                    data:fd,
                    processData : false,
                    contentType : false,
                    headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    success:function(data){
                        $('.buttonload').remove();
                        $('#postcomment').show();
                        
                        alert(data);
                       if(data==0)
                        {
                             
                             $('#myModal13').modal('show');
                                 
                            $('.alert').show();
                            $('.err_msg').html('First you should login and register in our website.');
                            setTimeout(function() {
                                    $(".alert").hide()
                                }, 3000);
                         }
                        else
                        {
                            
                            $('.err_comment').show();
                            $('.err_msg_commet').html('Commnet posetd successfully,wait for comment approval.');
                            $('.recipe_comments').html(data);
                            $('#commentform').trigger("reset"); 
                            setTimeout(function() {
                                    $(".alert").hide()
                                }, 3000);
                        }
                        
                    }
                 });
        }
    </script>
@endpush
