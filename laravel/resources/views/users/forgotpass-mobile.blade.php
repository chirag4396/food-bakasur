<div id="myModal124" class="modal fade" role="dialog">
    <div class="modal-dialog">

  <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Send OTP</h4>
            </div>
            <div class="modal-body">
                <div class="col-12 err_block_mobile" style="display: none">
                    <div class="alert alert-danger alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          
                          <span class="err_block_mobile_msg">dfdfg</span>
                    </div>
                </div>
                <form action="{{ URL::to('/password-recovery') }}" method="post" id="sendOTPFORM">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                    <p>
                        <label>Mobile No.  <span class="required">*</span></label>
                        <input type="text" placeholder="Mobile No." name="ud_mobile" id="so_mobile" class="form-control" onkeypress="return isNumberKey(event)" class="form-control" />
                        <p class="err_so_mobile err"><span class="err_so_mobile_txt"></span></p>
                    </p>
                    <div class="form-action">
                        <input type="button" class="text-center" value="Send OTP" onclick="formValidateSendOTP()" />

                    </div>
                </form>
          </div>
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
        </div>

    </div>
</div>
@push('footer')

    <script type="text/javascript">
      
        function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
 
         return true;
      }
      
        function formValidateSendOTP()
        {
            var result=1;
            var mobile=$('#so_mobile').val();

            
            var inputData=['so_mobile'];
            var inputErr=['err_so_mobile'];
            var inputErrTxt=['err_so_mobile_txt'];
            var inputErrMessage=["Please enter your mobile no.."];
            for(var i=0;i<inputData.length;i++)
            {
                if($('#'+inputData[i]).val()=='')
                {
                    $("."+inputErr[i]).show();
                    $("."+inputErrTxt[i]).html(inputErrMessage[i]);
                    result=0;
                    
                }
                else
                {
                    
                    $("."+inputErrTxt[i]).html('');
                        $("."+inputErr[i]).hide();
                }
            }
            if(result==1)
            {
                 var ajaxURL="{{ URL::to('/password-recovery') }}";

                var fd = new FormData($('#sendOTPFORM')[0]);
                 $.ajax({
                    url:ajaxURL,
                    type:'post',
                    data:fd,
                    processData : false,
                    contentType : false,
                    headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data){
                        $('#sendOTPFORM')[0].reset();
                        if(data=='1')
                        {
                             $('.err_block_mobile').show();
                            $('.err_block_mobile_msg').html("OTP sended on your mobile number please check it.");
                              setTimeout(function() {
                                $(".alert").hide();
                                window.location.href="{{ URL::to('/otp-verify') }}";
                            }, 3000);
                        }
                        else
                        {
                             $('.err_block_mobile').show();
                            $('.err_block_mobile_msg').html("Mobile No. is not registrated so, enter registrated mobile no.");
                              setTimeout(function() {
                                $(".alert").hide();
                            }, 3000);
                        }
                    },error:function(data){
                        console.log(data);
                    }
                 });
            }
        }
        //  setTimeout(function() {
        //     $(".alert").hide()
        // }, 3000);
    </script>
@endpush

