<div id="myModal12" class="modal fade" role="dialog">
   <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Recovery Password</h4>
    </div>
  <div class="modal-body">
   
     <form action="{{ URL::to('/password-recovery') }}" method="post" id="forgotPassEmail" enctype="multipart/form-data">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}" />
         <div class="col-12 err_block" style="display: none">
            <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  
                  <span class="err_block_msg">dfsdf</span>
            </div>
        </div>
        <div class="">

           <p>
              <label>Email address  <span class="required">*</span></label>
             <input type="text" placeholder="Email" name="email" id="fp_email" class="form-control" />
             <p class="err_fp_email err"><span class="err_fp_email_txt"></span></p>
          </p>


      </div>
      <div class="form-action">
          <button type="button" class="btn btn-default pull-left " data-toggle="modal" data-target="#myModal124"><u>Send OTP</u></button>
          <input type="button" class="text-center" value="submit" onclick="formValidateforFpEmail()" />
      </div>
  </form>
</div>
<div class="modal-footer">

    <!--     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
</div>
</div>

</div>
</div>
@include('users.forgotpass-mobile')
@push('footer')
<script type="text/javascript">
    $('#fp_email').keyup(function() {
          if ($('#fp_email').val() != '') {
            var email=$('#fp_email').val();
            if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
            {
               $('.err_fp_email').show();
                    $('.err_fp_email_txt').html('Please enter valid email address');
                    return false; 
            }
            else
            {
                
                $('.err_fp_email').hide();
            }
        } });
    function formValidateforFpEmail()
        {
            var result=1;
            var pwd=$('#password').val();

           
            var inputData=['fp_email'];
            var inputErr=['err_fp_email'];
            var inputErrTxt=['err_fp_email_txt'];
            var inputErrMessage=["Please enter your email."];
            for(var i=0;i<inputData.length;i++)
            {
                if($('#'+inputData[i]).val()=='')
                {
                    $("."+inputErr[i]).show();
                    $("."+inputErrTxt[i]).html(inputErrMessage[i]);
                    result=0;
                    
                }
                else
                {
                    
                    if ($('#fp_email').val() != '') {
                        var email=$('#fp_email').val();
                        if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
                        {
                           $('.err_fp_email').show();
                           $('.err_fp_email_txt').html('Please enter valid email address');
                               result=0;
                        }
                        else
                        {
                            $('.err_fp_email').hide();
                                    
                        }
                    }
                    $("."+inputErrTxt[i]).html('');
                        $("."+inputErr[i]).hide();
                }
            }
            if(result==1)
            {
                var ajaxURL="{{ URL::to('/password-recovery') }}";
                var fd = new FormData($('#forgotPassEmail')[0]);
                 $.ajax({
                    url:ajaxURL,
                    type:'post',
                    data:fd,
                    processData : false,
                    contentType : false,
                    headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data){
                        if(data=='1')
                        {
                             $('.err_block').show();
                            $('.err_block_msg').html("Reset Password link has been sended on your email address,Please check it.");
                              setTimeout(function() {
                                $(".alert").hide();
                                window.location.href="{{ URL::to('/') }}";
                            }, 3000);
                        }
                        else if(data=="2")
                        {
                             $('.err_block').show();
                            $('.err_block_msg').html("Email address is not registrated so, enter registrated email address");
                              setTimeout(function() {
                                $(".alert").hide();
                            }, 3000);
                        }
                       
                               
                       
                    }
                 });
            }
        }
</script>
@endpush