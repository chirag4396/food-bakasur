@extends('users.layouts.master')
@push('header')
	<style type="text/css">
		.err
		{
			display: none;
			color: red;
		}
	</style>
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('content')
<div class="main-top">
	<div class="container white" style="padding: 15px;">
		<h3 class="head">OTP Verification</h3>
		<div class="border-line3"></div>
<!-- checkout-area start -->
		<div class="checkout-area">
		   	<div class="container">
		      	<div class="row">
		      		
		         	<form action="{{ URL::to('/otp-verify') }}" method="post" id="sendotpform" enctype="multipart/form-data">
		         		<input name="_token" type="hidden" value="{{ csrf_token() }}" />
		         		<div class="col-lg-3"></div>
			            <div class="col-lg-6">
			               <div class="checkbox-form">
			                  <div class="row">
			                   	@if(Session::has('message'))
									<div class="col-12">
										<div class="alert alert-danger alert-dismissible">
						                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						                  
						                  {{ Session::get('message') }}
						                </div>
									</div>
								@endif
			                    <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>OTP<span class="required">*</span></label>
			                           <input type="text" placeholder="Enter OTP" name="otp" id="otp" class="form-control" onkeypress="return isNumberKey(event)"/>
			                           <p class="err_otp err"><span class="err_otp_txt"></span></p>
			                        </div>
			                     </div>
			                     
			                     <div class="col-md-12">
			                     	{{-- <div class="col-md-6"><a href="{{ URL::to('user-login') }}">Sign In</a></div><div class="col-md-6"><a href="{{ URL::to('/send-otp') }}">Back</a></div></div> --}}
			                     <div class="col-md-12">
			                        <div class="order-button-payment">
			                           <input type="button" value="OTP Verify" onclick="formValidateforMobile()" />
			                        </div>
			                        <div class="col-md-12">
			                        </div>
			                     </div>
			                  </div>
			               </div>
			            </div>
			            <div class="col-lg-3"></div>
		         	</form>
		        </div>
		    </div>
		</div>
      <!-- checkout-area end -->  
   </div>
</div>
@endsection
@push('footer')
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript">
		
	    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
 
         return true;
      }
      
		function formValidateforMobile()
		{
			var result=1;
	       	var inputData=['otp'];
			var inputErr=['err_otp'];
			var inputErrTxt=['err_otp_txt'];
			var inputErrMessage=["Please enter OTP.."];
			for(var i=0;i<inputData.length;i++)
			{
				if($('#'+inputData[i]).val()=='')
				{
					$("."+inputErr[i]).show();
					$("."+inputErrTxt[i]).html(inputErrMessage[i]);
					result=0;
					
				}
				else
				{
					
					$("."+inputErrTxt[i]).html('');
						$("."+inputErr[i]).hide();
				}
			}
			if(result==1)
			{
				$('#sendotpform').submit();
			}
		}
		 setTimeout(function() {
			$(".alert").hide()
		}, 3000);
	</script>
@endpush

