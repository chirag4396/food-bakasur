<div id="sendEnquiryModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <form id="enquiryForm" method="post"> 
        <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Send Enquiry</h4>
              </div>
              <div class="modal-body">
                    <div class="form-fields">
                           <input type="hidden" name="be_b_id" value="1">     
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">       
                        <p>
                           <label>Name: <span class="required">*</span></label>
                           <input type="text" name="be_name" id="e_name" />
                           <p class="err_e_name err"><span class="err_e_name_txt"></span></p>
                        </p>
                        <p>
                           <label>Email: <span class="required">*</span></label>
                           <input type="text" name="be_email" id="e_email" />
                           <p class="err_e_email err"><span class="err_e_email_txt"></span></p>
                        </p>
                        <p>
                           <label>Phone: <span class="required">*</span></label>
                           <input type="text" name="be_phone" id="e_phone"/>
                           <p class="err_e_phone err"><span class="err_e_phone_txt"></span></p>
                        </p>
                        <p>
                           <label>Message:  <span class="required">*</span></label>
                           <input type="text" name="be_msg" id="e_msg"/>
                           <p class="err_e_msg err"><span class="err_e_msg_txt"></span></p>
                        </p>
                       
                      
                     </div>

              </div>
              <div class="modal-footer enquirybutton">
                 <input type="button" name="" onclick="enquiryValidation()" value="Submit" class="btn btn-primary" id="enquirysubmit">
              </div>
        </div>
         </form> 
  </div>
</div>
<script type="text/javascript">
    $("#e_phone").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('#e_name').bind('keypress', function (event) {
          var regex = new RegExp("^[a-zA-Z, ,\t]+$");
          var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
          if (!regex.test(key)) {
             event.preventDefault();
             return false;
          }
      });
     $('#e_email').keyup(function() {
          if ($('#e_email').val() != '') {
            var email=$('#e_email').val();
            if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
            {
               $('.err_e_email').show();
                $('.err_e_email_txt').html('Please enter valid email address');
                return false; 
            }
            else
            {
                
                $('.err_e_email').hide();
            }
        } });

        function fieldValidation(id,err,err_txt,msg)
        {
            if($('#'+id).val()=='')
            {
                $("."+err).show();
                $("."+err_txt).html(msg);
                return 0;
                
            }
            else
            {
                $("."+err_txt).html('');
                $("."+err).hide();
                return 1;
            }
        }

        function enquiryValidation()
        {
            var name=fieldValidation('e_name','err_e_name','err_e_name_txt','Please enter your name');
            var email=fieldValidation('e_email','err_e_email','err_e_email_txt','Please enter your email');
            var phone=fieldValidation('e_phone','err_e_phone','err_e_phone_txt','Please enter your phone no.');
            var message=fieldValidation('e_msg','err_e_msg','err_e_msg_txt','Please enter your message');
            if(name==0 || email==0 || phone==0 || message==0)
            {
                return false;
            }
            else
            {
                $('#enquirysubmit').hide();
                $('.enquirybutton').append('<button class="buttonload btn btn-primary" style="float:right" ><i class="fa fa-spinner fa-spin"></i>Processing</button>');
                var ajaxURL="{{ URL::to('/user-businessEnquiry') }}";
                var fd = new FormData($('#enquiryForm')[0]);
                 $.ajax({
                    url:ajaxURL,
                    type:'post',
                    data:fd,
                    processData : false,
                    contentType : false,
                    headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data){
                      alert(data);
                      $('#loginform')[0].reset();
                      $('.enquirybutton').remove();
                      $('#enquirysubmit').show();
                      $('#sendEnquiryModal').close();
                       
                        
                    },
                    error: function (data) {
                          console.log(data);
                      }
                 });
            }
        }
</script>