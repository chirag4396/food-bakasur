@extends('users.layouts.master')
@push('header')
	<style type="text/css">
		.err
		{
			display: none;
			color: red;
		}
	</style>
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('content')
<div class="main-top">
	<div class="container white" style="padding: 15px;">
		<h3 class="head">Reset Password</h3>
		<div class="border-line3"></div>
<!-- checkout-area start -->
		<div class="checkout-area">
		   	<div class="container">
		      	<div class="row">
		      		
		         	<form action="{{ URL::to('/reset-password') }}" method="post" id="resetpasswordform" enctype="multipart/form-data">
		         		<input name="_token" type="hidden" value="{{ csrf_token() }}" />
		         		<input name="id" type="hidden" value="{{ $id }}" />
		         		<input name="otp" type="hidden" value="{{ $otp }}" />
		         		<div class="col-lg-3"></div>
			            <div class="col-lg-6">
			               <div class="checkbox-form">
			                  <div class="row">
			                   	@if(Session::has('message'))
									<div class="col-12">
										<div class="alert alert-danger alert-dismissible">
						                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						                  
						                  {{ Session::get('message') }}
						                </div>
									</div>
								@endif
			                    <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Password <span class="required">*</span></label>
			                           <input type="password" placeholder="Password" name="password" id="password"/>
			                           <p class="err_pwd err"><span class="err_pwd_txt"></span></p>
			                        </div>
			                     </div>
			                     <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Confirm Password <span class="required">*</span></label>                   
			                           <input type="password" placeholder="Confirm password" name="confirm_password" id="confirm_password" />
			                            <p class="err_c_pwd err"><span class="err_cpwd_txt"></span></p>
			                        </div>
			                       
			                     </div>
			                     <div class="col-md-12">
			                     	<div class="col-md-6"><a href="{{ URL::to('userregister') }}">Sign In</a></div><div class="col-md-6"><a href="{{ URL::to('forgot-password') }}">Forgot Password</a></div></div>
			                     <div class="col-md-12">
			                        <div class="order-button-payment">
			                           <input type="button" value="Reset Password" onclick="formValidate()" />
			                        </div>
			                        <div class="col-md-12">
			                        </div>
			                     </div>
			                  </div>
			               </div>
			            </div>
			            <div class="col-lg-3"></div>
		         	</form>
		        </div>
		    </div>
		</div>
      <!-- checkout-area end -->  
   </div>
</div>
@endsection
@push('footer')
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript">
		$('#confirm_password').keyup(function() {
	      if ($('#confirm_password').val() != '') {
	        var pwd=$('#password').val();
	        var c_pwd=$('#confirm_password').val();
	        if(pwd!=c_pwd)
	        {
	           $('.err_c_pwd').show();
	                $('.err_cpwd_txt').html('Password and Confirm Password should be same.');
	                return false; 
	        }
	        else
	        {
	            $('.err_c_pwd').hide();
	            return true; 
	        }
	    } });
		function formValidate()
		{
			var result=1;
	       	var pwd=$('#password').val();

	        var c_pwd=$('#confirm_password').val();
			var inputData=['password','confirm_password'];
			var inputErr=['err_pwd','err_c_pwd'];
			var inputErrTxt=['err_pwd_txt','err_cpwd_txt'];
			var inputErrMessage=["Please enter password.",'Please enter password again for confirmation.'];
			for(var i=0;i<inputData.length;i++)
			{
				if($('#'+inputData[i]).val()=='')
				{
					$("."+inputErr[i]).show();
					$("."+inputErrTxt[i]).html(inputErrMessage[i]);
					result=0;
					
				}
				else
				{
					if(pwd!=c_pwd && pwd!='')
			        {
			           $('.err_c_pwd').show();
			           $('.err_cpwd_txt').html('Password and Confirm Password should be same.');
			                result=0;
			        }
			        else
			        {
			            $('.err_c_pwd').hide();
			            
			        }
					$("."+inputErrTxt[i]).html('');
						$("."+inputErr[i]).hide();
				}
			}
			if(result==1)
			{
				$('#resetpasswordform').submit();
			}
		}
		 setTimeout(function() {
			$(".alert").hide()
		}, 3000);
	</script>
@endpush

