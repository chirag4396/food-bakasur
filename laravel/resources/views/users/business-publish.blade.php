<div class="business-publish" style="position: relative;">
   @foreach($businessesPublish as $rk=>$rv)
      <div class="row row1" >
         <div class="col-md-6 main-resto ">
            <a href="{{ URL::To('business-detail/'.$rv->b_id) }}">
               <img class="img-fluid rounded mb-3 mb-md-0" src="img/f1.jpg" alt="">
            </a>
         </div>
         <div class="col-md-6 ">
            <div class="titleDiv">
               <h4><a href="{{ URL::To('business-detail/'.$rv->b_id) }}">{{ $rv->b_name }}</a></h4>
               <div class="location">
                  <i class="fa fa-map-marker" aria-hidden="true"></i>
                  <a href="{{ URL::To('business-detail/'.$rv->b_id) }}">{{ $rv->b_address }}</a>
               </div>
            </div>
            <p>{{ str_limit($rv->b_desc,100) }}</p>
            <div class="clearfix"></div>
            <div class="col-md-12 con">
               <a class="btn btn-success" href="#"> <i class="fa fa-mobile" aria-hidden="true"></i>{{ $rv->b_contact_person_mobile }}</a>
               <a class="btn btn-success" href="#"> <i class="fa fa-user" aria-hidden="true"></i> {{ $rv->b_contact_person_name }}</a>
               
            </div>
         </div>
      </div>
   @endforeach
</div>
<div id="paginationforbus" class="row" style="float: right;margin-right: 15px;">{{ $businessesPublish->render() }}</div>