@extends('users.layouts.master')
@push('header')
<style type="text/css">
   .err
   {
	   display: none;
	   color: red;
   }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('content')
	<div class="main-top ">
		<div class="col-xs-12 saving"> Updating Your Blog!!!</div>
		<section class="food-tabs2">
		   <div class="container white">
		      <section  class="blog1 blog-margin-top">
		         <!--main starts-->
		         <div class="clearfix"></div>
		         <div class="container white blog-create">
		            <button class="btn btn-primary2" id="new" type="submit" name="create" value="new">Hey Foodie.. Create a new Blog... !!! </button>
		            @php
		            	$url=($mode=='Update')?'/update-blog':'/create-blog'
		            @endphp
		            <form name="add-blog" id="addblogform" method="post" enctype="multipart/form-data" action="{{ URL::to($url) }}">
		            	@if($mode=='Update')
		            		<input type="hidden" name="blog_id" value="{{ $blogData['blog_id'] }}">
		            	@endif
		            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
			            <div class="col-lg-12 col-xs-12  col-md-6 main-middle ">
			               <div class="col-xs-12 col-lg-5 b-r margin-top">
			                  <h1 class="heading-new"> <i class="fa fa-pencil" aria-hidden="true"></i> 
			                     {{ $mode }} your Blog 
			                  </h1>
			                  <input class="blog-form" placeholder="Enter a title for your story e.g. Backpacking in Africa" class="tripTitle" maxlength="100" type="text" name="blog_title" id="blog_title" value="@if($mode=='Update'){{ $blogData['blog_title'] }}@endif">
			                  <p class="err_title err"><span class="err_title_txt"></span></p>
			               </div>
			               <div class="col-xs-12 col-md-12 col-lg-2 b-r  margin-top">
			                  <h1 class="heading-new"> <i class="fa fa-list" aria-hidden="true"></i>
			                     Meal Type 
			                  </h1>
			                  <select class="blog-form" name="blog_meal_id" id="blog_meal_id">
			                     <option value="">--Select--</option>
			                     @foreach($meals as $m=>$mData)
			                     	<option value="{{ $mData->meal_id }}" @if($mode=='Update'){{ ($blogData['blog_meal_id']==$mData->meal_id)?'selected':'' }}@endif>{{ $mData->meal_title }}</option>
			                     @endforeach
			                  </select>
			                  <p class="err_meal err"><span class="err_meal_txt"></span></p>
			               </div>
			              <div class="col-xs-12 col-md-12 col-lg-2 b-r  margin-top">
			                  <h1 class="heading-new"> <i class="fa fa-list" aria-hidden="true"></i>
			                     Cuisine Type 
			                  </h1>
			                  <select class="blog-form" name="blog_cuisine_id" id="blog_cuisine_id">
			                  	<option value="">--Select--</option>
			                     @foreach($cuisines as $c=>$cData)
			                     	<option value="{{ $cData->cu_id }}" @if($mode=='Update'){{ ($blogData['blog_cuisine_id']==$cData->cu_id)?'selected':'' }}@endif>{{ $cData->cu_title }}</option>
			                     @endforeach
			                  </select>
			                  <p class="err_cuisines err"><span class="err_cuisines_txt"></span></p>
			               </div>
			               <div class="col-xs-12 col-md-12 col-lg-3 margin-top">
			                  <h1 class="heading-new"> <i class="fa fa-list" aria-hidden="true"></i>
			                     Food Type 
			                  </h1>
			                  <select class="blog-form" name="blog_veg" id="blog_veg">
			                  	<option value="">--Select--</option>
			                     	<option value="1" @if($mode=='Update'){{ ($blogData['blog_veg']=="1")?'selected':'' }}@endif>Veg</option>
			                     	<option value="0" @if($mode=='Update'){{ ($blogData['blog_veg']=="0")?'selected':'' }}@endif>Non Veg</option>
			                    
			                  </select>
			                  <p class="err_food err"><span class="err_food_txt"></span></p>
			               </div>
			               <div class="col-xs-12 margin-top">
			                  <h1 class="heading-new"> <i class="fa fa-picture-o" aria-hidden="true"></i>
			                     Set Cover Picture
			                  </h1>
			                  <div class="">
			                  
			                  		@if($mode=='Update')
			                  					@php 
			                  					$imgURL=($blogData['blog_cover_img']=='No Image')?'':$blogData['blog_cover_img'];
			                  					$imgURL=$blogData['blog_cover_img'];
			                  					@endphp
			                  				@else
				                  				@php 
				                  				$imgURL='';
				                  				@endphp
			                  		@endif
			                  
			                     <div class="col-xs-12 cover-pc margin-top" id="imagePreview" style="background-position: center 100%;
			                        background-size: cover;background-image: url('{{ $imgURL }}');">
			                        <div class="uploadLoad" id="uploadLoader"></div>
			                        {{-- <form id="confirmImage" method="post"> --}}
			                           <div class="errorImage">Upload Image of 1366 X 400 for Best Result</div>
			                           <div class="warning-error" id="errorImage" style="display: none;">Upload Image of 1366 X 400 for Best Result</div>
			                           <input name="blog_cover_img" class="upload blog_cover_img" id="file-2" type="file" data-multiple-caption="{count} files selected" multiple="" accept="image/png, image/jpeg, image/jpg" style="display: none;" onchange="readURL(this)">
			                           <label for="file-2" class="fileUpload upload" id="uploadBanner" title="Upload your Event Picture" style="display:block">
			                              <div>
			                                 <i class="fa fa-upload" aria-hidden="true"></i> Upload banner 
			                              </div>
			                           </label>
			                           <div class="mybanner" id="bannerBtn" style=";display:none">
			                              <span id="errLaMsg" class="someMsg">Something went Wrong. Please Try agian!</span>
			                              <br>
			                              <button type="submit" class="btn confirm" id="doneBtn" style="display:block">Confirm</button>            
			                              <label for="file-2" class="btn confirm2" title="Change Existing Banner of Event">Change</label>  
			                           </div>
			                           <p class="err_coverpic err"><span class="err_coverpic_txt"></span></p>
			                        {{-- </form> --}}
			                     </div>
			                  </div>
			               </div>
			               <div class="col-xs-12 margin-top">
			                  <h1 class="heading-new"> <i class="fa fa-align-justify" aria-hidden="true"></i>
			                     Add Your Blog Story
			                  </h1>
			                  
			                     <div class="col-xs-12 margin-top" >
			                       <textarea id="blog_story" class="blog_story" name="blog_story">@if($mode=='Update') {{ $blogData['blog_story'] }} @endif</textarea>
			                 		 <p class="err_story err"><span class="err_story_txt"></span></p>
			                      
			               		</div>
			               </div>
			               <div class="col-xs-12 margin-top">
			                  {{-- <h1 class="heading-new"> <i class="fa fa-globe" aria-hidden="true"></i>
			                     Overview 
			                  </h1>
			                  <div class="col-xs-12 margin-top">
			                     <div class="col-xs-12  col-md-4 col-lg-6">
			                        Estimate Time To read
			                     </div>
			                     <div class="col-xs-6 col-md-4 col-lg-3">
			                        <input type="number" class="readtime" name="blog_minutes" id="blogMin" value="">
			                     </div> --}}
			                     <div class="col-xs-6 col-md-4 col-lg-3">
			                       <input type="button" name="addBlog" id="addBlog" value="Create Blog" class="btn btn-success" class="blog-form" onclick="formValidate()">
			                     </div>
			                     <div class="clearfix"></div>
			                     <!-- <div class="col-xs-12 blog-writing margin-top"> -->
			                     <div class="editable style-4" id = "editable"></div>
			                     <!-- </div> -->
			                  </div>
			                  <div class="col-xs-12 margin-top">
			                  	
			                  		
			                  	
			                  </div>
			               </div>
			            </div>
			        </form>
		            <div class="col-lg-12 col-xs-12  col-md-6 main-middle ">
		            </div>
		          </div>
		      </section>
		      </div>
		</section>
	</div>
@endsection
@push('footer')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

	<script>
		CKEDITOR.replace( 'blog_story' );
		$( "#searchBox" ).on('click', function() {
			$( ".scrollbar" ).toggle( "slideDown"  );

		});
		
	    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.cover-pc').css('background-image', 'url(' + e.target.result + ')');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
		function formValidate()
		{
			var result=1;
			//alert(result);
			var strory = CKEDITOR.instances['blog_story'].getData();
			var inputData=['blog_title','blog_meal_id','blog_cuisine_id','blog_veg'];
			var inputErr=['err_title','err_meal','err_cuisines','err_food'];
			var inputErrTxt=['err_title_txt','err_meal_txt','err_cuisines_txt','err_food_txt'];
			var inputErrMessage=["Please enter blog name.","Please choose food meal.","Please choose food cuisines.","Please choose food type."];
			for(var i=0;i<inputData.length;i++)
			{
				if($('#'+inputData[i]).val()=='')
				{
					$("."+inputErr[i]).show();
					$("."+inputErrTxt[i]).html(inputErrMessage[i]);
					result=0;
				}
				else
				{
					$("."+inputErr[i]).hide();
					$("."+inputErrTxt[i]).html('');
				}
				
			}
			if(strory=='')
			{
				result=1;
			}
			if(result==1)
			{

				$('#addblogform').submit();
			}
		}
	</script>
@endpush

