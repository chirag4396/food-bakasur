@extends('users.layouts.master')
@push('header')

   
@endpush
@section('content')
<div class="main-top">
   <section class="food-tabs">
      <div class="container white">
         <div class="menu-tab">
            <ul class="nav nav-tabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" href="#profile" role="tab" data-toggle="tab"> <i class="fa fa-list" aria-hidden="true"></i> Listing</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#buzz" role="tab" data-toggle="tab"> <i class="fa fa-rss" aria-hidden="true"></i> Blog</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#references" role="tab" data-toggle="tab"> <i class="fa fa-folder" aria-hidden="true"></i> Recipes</a>
               </li>
            </ul>
         </div>
         <!-- Tab panes -->
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="profile">
                <div class="col-md-12">
                     <div class="col-md-4"></div>
                <div class="col-md-4">

                    <h3 class="head">List Details
                         </h3>
                </div>
                <div class="col-md-4" >
                 @if(Session::has('sessionData'))
                
                  <a class="btn create" href="{{ URL::to('/business-add') }}" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add Your Business</a>
                
                @endif
                </div>
                </div>
                <div class="clearfix"></div>
                 <div class="border-line3"></div>
                 @if(isset($business_listing))
                     
                    
                    @foreach($business_listing as $buk=>$bl)
                       
                          {{--   <h3 class="head">{{ $bl['bt_name'] }}
                             </h3> --}}
                           
                                <div class="row row1 ">
                                    <div class="col-md-4 main-resto ">
                                        <a href="{{ URL::To('business-detail/'.$bl['b_id']) }}">
                                            <img class="img-fluid rounded mb-3 mb-md-0" src="{{ $bl['b_photo'] }}" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6 border-left">
                                         <div class="titleDiv">
                                            <h4><a href="{{ URL::To('business-detail/'.$bl['b_id']) }}">{{ $bl['b_name'] }}</a></h4>
                                            <div class="location">
                                               <i class="fa fa-map-marker" aria-hidden="true"></i>
                                               <a>{{ $bl['b_address'] }}</a>
                                            </div>
                                         </div>
                                        <p>{{ $bl['b_desc'] }}</p>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12 con">
                                            <a class="btn btn-success" href="{{ URL::To('business-detail/'.$bl['b_id']) }}"> <i class="fa fa-mobile" aria-hidden="true"></i> {{ $bl['b_contact_person_mobile'] }}</a>
                                            <a class="btn btn-success" href="{{ URL::To('business-detail/'.$bl['b_id']) }}"> <i class="fa fa-user" aria-hidden="true"></i>  {{ $bl['b_contact_person_name'] }}</a>
                                       
                                        </div>
                                  </div>
                                  <div class="col-md-2 enq enq{{ $bl['b_id'] }}">
                                     <a class="btn btn-warning " data-toggle="modal" data-target="#sendEnquiryModal" href="#"> Enquiry</a>
                                  </div>
                               </div>
                         
                        @include('users.models.send_enquiry')
                       
                    @endforeach
                    @else
                        <div class="row row1" >
                            <center> <h2>No any Blogs Available</h2></center>
                       </div>
                    @endif
            </div>
            <div role="tabpanel" class="tab-pane fade" id="buzz">
                <div class="col-md-12">
                     <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <h3 class="head">Blog List Details </h3>
                    </div>
                    <div class="col-md-4">
                       @if(Session::has('sessionData'))
                      
                          <a class="btn create" href="{{ URL::to('/blog-create') }}" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add Your Blog</a>
                       
                        @endif
                    </div>
                </div>
                <div class="clearfix"></div>
               <div class="border-line3"></div>
               @if(isset($blog_list))
               <div class="jscroll"> 
                   @foreach($blog_list as $bk=>$blog)

                       <div class="row row1 ">
                          <div class="col-md-4 main-resto">
                             <a href="{{ URL::to('blog-detail/'.$blog->blog_id) }}">
                             <img class="img-fluid rounded mb-3 mb-md-0" src="{{ $blog->blog_cover_img }}" alt="">
                             </a>
                          </div>
                          <div class="col-md-8 titleDiv">
                             <h4>{{ str_limit($blog['blog_title'],40) }}</h4>
                             <p>{{ strip_tags(str_limit($blog['blog_story'],400)) }}
                             </p>
                             <a class="btn btn-blog" href="{{ URL::to('blog-detail/'.$blog->blog_id) }}">Read More →
                             </a>
                             
                          </div>
                       </div>
                        
                    @endforeach
                    {{ $blog_list->links() }}
                </div>
                @else
                    <div class="row row1" >
                         
                           <center> <h2>No any Blogs Available</h2></center>
                        
                       </div>
                @endif
               
            </div>
            <div role="tabpanel" class="tab-pane fade" id="references">
                <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                       <h3 class="head">Recipe List Details </h3>
                    </div>
                    
                     <div class="col-md-4">
                       @if(Session::has('sessionData'))
                       
                          <a class="btn create" href="{{ URL::to('/add-recipe') }}" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add Your Recipe</a>
                        
                        @endif
                    </div>
                </div>
               <div class="border-line3"></div>
               <div class="clearfix"></div>
               <div class="row rec1">
                @if(isset($reciepes_list))
                {{-- <div class="jscroll"> --}}
                    @foreach($reciepes_list as $rk=>$reciepes)
                        
                            <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item" >
                               <div class="card border" style="height: 620px">
                                  <a href="{{ URL::To('recipe-detail/'.$reciepes['rec_id']) }}"><img class="card-img-top" src="{{ $reciepes['rec_img_path'] }}" alt=""></a>
                                  <div class="card-body ">
                                     <h4>
                                        <a href="{{ URL::To('recipe-detail/'.$reciepes['rec_id']) }}">{{ $reciepes['rec_title'] }}</a>
                                     </h4>
                                     <ul>
                                        @foreach($reciepes['ingrediants'] as $steps)
                                        <li>{{ $steps['ri_title'] }}</li>
                                        @endforeach
                                     </ul>
                                     <p class="rec">{{ str_limit($reciepes['rec_description'],180) }}</p>
                                  </div>
                               </div>
                            </div>
                        
                    @endforeach
                       {{ $reciepes_list->links() }}

                    @else
                    <div class="row rec1">
                        
                            <center><h2>No any Recipes Available</h2></center>
                         
                       </div>
                @endif
                 
               </div>
            </div>
         </div>
      </div>
</div>
@push('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.3.7/jquery.jscroll.min.js"></script>
<script type="text/javascript">
    $('ul.pagination').hide();
    $(function() {
        $('.jscroll').jscroll({
            debug: true,
            autoTrigger : true,
            nextSelector : '.pagination li.active + li a',
            contentSelector : 'div.jscroll',
            callback: function(){
               
                $('.pagination:visible:first').hide();
            },
            loadingHtml : '<div class ="loading-forum">Loading...</div>'
        });
    });
</script> 
@endpush
@endsection


