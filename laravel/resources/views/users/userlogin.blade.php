@extends('users.layouts.master')
@push('header')
	<style type="text/css">
		.err
		{
			display: none;
			color: red;
		}
	</style>
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('content')
<div class="main-top">
	<div class="container white" style="padding: 15px;">
		<h3 class="head">Login</h3>
		<div class="border-line3"></div>
<!-- checkout-area start -->
		<div class="checkout-area">
		   	<div class="container">
		      	<div class="row">
		      		
		         	<form action="{{ URL::to('/user-login') }}" method="post" id="loginform" enctype="multipart/form-data">
		         		<input name="_token" type="hidden" value="{{ csrf_token() }}" />
		         		<div class="col-lg-3"></div>
			            <div class="col-lg-6">
			               <div class="checkbox-form">
			                  <div class="row">
			                   	@if(Session::has('message'))
									<div class="col-12">
										<div class="alert alert-danger alert-dismissible">
						                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						                  
						                  {{ Session::get('message') }}
						                </div>
									</div>
								@endif
			                    <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Email <span class="required">*</span></label>
			                           <input type="text" placeholder="Email" name="email" id="email"/>
			                           <p class="err_email err"><span class="err_email_txt"></span></p>
			                        </div>
			                     </div>
			                     <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Password <span class="required">*</span></label>
			                           <input type="password" placeholder="Password" name="password" id="password"/>
			                           <p class="err_pwd err"><span class="err_pwd_txt"></span></p>
			                        </div>
			                     </div>
			                     <div class="col-md-12">
			                     	<div class="col-md-6"><a href="{{ URL::to('userregister') }}">Sign Up</a></div><div class="col-md-6"><a href="{{ URL::to('forgot-password') }}">Forgot Password</a></div></div>
			                     <div class="col-md-12">
			                        <div class="order-button-payment">
			                           <input type="button" value="Login" onclick="formValidate()" />
			                        </div>
			                        <div class="col-md-12">
			                        </div>
			                     </div>
			                  </div>
			               </div>
			            </div>
			            <div class="col-lg-3"></div>
		         	</form>
		        </div>
		    </div>
		</div>
      <!-- checkout-area end -->  
   </div>
</div>
@endsection
@push('footer')
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript">
		$('#email').keyup(function() {
	      if ($('#email').val() != '') {
	        var email=$('#email').val();
	        if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
	        {
	           $('.err_email').show();
	                $('.err_email_txt').html('Please enter valid email address');
	                return false; 
	        }
	        else
	        {
	        	
			    $('.err_email').hide();
			            	 
			          
				           
	        }
	    } });
		function formValidate()
		{
			var result=1;
			var pwd=$('#password').val();

	       
			var inputData=['email','password'];
			var inputErr=['err_email','err_pwd'];
			var inputErrTxt=['err_email_txt','err_pwd_txt'];
			var inputErrMessage=["Please enter your email.","Please enter password."];
			for(var i=0;i<inputData.length;i++)
			{
				if($('#'+inputData[i]).val()=='')
				{
					$("."+inputErr[i]).show();
					$("."+inputErrTxt[i]).html(inputErrMessage[i]);
					result=0;
					
				}
				else
				{
					
					if ($('#email').val() != '') {
				        var email=$('#email').val();
				        if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
				        {
				           $('.err_email').show();
				           $('.err_email_txt').html('Please enter valid email address');
				               result=0;
				        }
				        else
				        {
				        	$('.err_email').hide();
						            
				        }
				    }
					$("."+inputErrTxt[i]).html('');
						$("."+inputErr[i]).hide();
				}
			}
			if(result==1)
			{
				$('#loginform').submit();
			}
		}
		 setTimeout(function() {
			$(".alert").hide()
		}, 3000);
	</script>
@endpush

