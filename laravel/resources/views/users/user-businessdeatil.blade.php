@extends('users.layouts.master')
@push('header')
	 <style type="text/css">
      .row{
      padding-left: 10px;
      padding-right: 10px;
      }
      .img123{
        width: 100px;
        height: 150px;
      }
      .text-123{
        position: absolute;
        top:20%;
        left:20%;

      }
      .bg-img{
      background-image: url("img/banner/22.jpg");
      height: 400px;
      background-size: cover;
      }
      .address {
		    color: #737882;
		    padding: 18px 0 0 16px;
		}
		.booking-details_wrap {
		    padding: 26px 0;
		}

		.booking-checkbox_wrap {
		    background: #fff;
		    padding: 30px;
		    h5 {
		        color: #3e434b;
		        text-align: center;
		        font-weight: 400;
		    }
		}

		.booking-checkbox {
		    color: #737882;
		    font-size: 15px;
		    p {
		        line-height: 1.8;
		    }
		}

		.customer-review_wrap {
		    display: inline-block;
		    width: 100%;
		}

		.customer-img {
		    float: left;
		    @media (max-width: 992px) {
		        text-align: center;
		        float: none;
		        margin: 0 0 15px;
		    }
		    img {
		        border-radius: 50%;
		    }
		    p {
		        color: #3e434b;
		        margin: 4px 0 0;
		        text-align: center;
		        font-size: 18px;
		        line-height: 1.2;
		    }
		    span {
		        font-size: 14px;
		        color: #737882;
		        text-align: center;
		    }
		}

		.customer-content-wrap {
		    margin: 0 0 0 135px;
		    @media (max-width: 992px) {
		        margin: 0 0 0 110px;
		    }
		    @media (max-width: 992px) {
		        margin: 0px;
		    }
		    ul {
		        margin: 24px 0 0;
		        padding: 0;
		        li {
		            list-style: none;
		            display: inline-block;
		            margin: 0 10px 0 0;
		            @media (max-width: 480px) {
		                margin: 0 0 10px 0;
		            }
		        }
		    }
		    span {
		        color: #737882;
		        display: inline-block;
		        margin: 26px 0 0;
		    }
		    a {
		        border: 1px solid #ccc;
		        color: #737882;
		        font-size: 14px;
		        border-radius: 3px;
		        padding: 5px 10px;
		        ;
		        margin: 0 0 0 10px;
		        &:hover {
		            text-decoration: none;
		        }
		        span {
		            margin: 0 6px 0 0;
		        }
		    }
		}

		.customer-text {
		    color: #737882;
		    margin: 15px 0 0;
		    line-height: 1.6;
		}

		.customer-content {
		    h6 {
		        color: #3e434b;
		    }
		    p {
		        color: #737882;
		        display: inline-block;
		        vertical-align: top;
		        line-height: 0.8;
		        margin: 0 0 0 8px;
		    }
		}

		.customer-review {
		    display: inline-block;
		    @media (max-width: 576px) {
		        display: block;
		    }
		    span {
		        width: 10px;
		        height: 10px;
		        background: #46cd38;
		        border-radius: 50%;
		        display: block;
		        float: left;
		        margin: 0 5px 0 0;
		    }
		}

		.round-icon-blank {
		    background: #d9dce7!important;
		}

		.round-icon-red {
		    background: #ff5a5a!important;
		}

		.round-icon-orange {
		    background: #ffbb27!important;
		}

		.customer-rating {
		    background: #46cd38;
		    padding: 8px 14px;
		    float: right;
		    font-size: 18px;
		    color: #fff;
		    border-radius: 3px;
		    @media (max-width: 576px) {
		        float: none;
		        display: inline-block;
		    }
		}

		.customer-rating-red {
		    background: #ff5a5a!important
		}

		.customer-description {
		    margin: 0 0 0 50px;
		}

		// Contact Info
		.contact-info {
		    background: #fff;
		    padding: 0 0 6px;
		    margin: 0 0 15px;
		    img {
		        width: 100%;
		    }
		    label {
		        color: #46cd38;
		        font-size: 14px;
		        padding: 0 0 0 50px;
		        @media (max-width: 992px) {
		            display: block;
		        }
		    }
		}

		.address {
		    color: #737882;
		    padding: 18px 0 0 16px;
		    >span {
		        font-size: 18px;
		        float: left;
		        margin: 4px 0 0;
		    }
		    p {
		        margin: 0 0 0 34px;
		    }
		    .featured-open {
		        font-size: 13px;
		        padding: 0 0 0 34px;
		    }
		}

		.btn-contact {
		    display: block;
		    padding: 16px;
		    margin: 12px 16px;
		    border: 2px solid #ff3a6d;
		    color: #ff3a6d;
		    font-weight: 400;
		    &:hover {
		        background: #ff3a6d;
		        border: 2px solid #ff3a6d;
		    }
		}
		.follow ul {
		    margin: 0;
		    padding: 0;
		    border-top: 1px solid #ededed;
		    border-bottom: 1px solid #ededed;
		    display: -webkit-box;
		    display: -webkit-flex;
		    display: -ms-flexbox;
		    display: flex;
		    -webkit-box-orient: horizontal;
		    -webkit-box-direction: normal;
		    -webkit-flex-direction: row;
		    -ms-flex-direction: row;
		    flex-direction: row;
		    -webkit-justify-content: space-around;
		    -ms-flex-pack: distribute;
		    justify-content: space-around;
		    text-align: center;
		}

		.follow ul li:first-child, .follow ul li:nth-child(2) {
		    border-right: 1px solid #ededed;
		}
		.follow ul li {
		    list-style: none;
		    padding: 13px 0;
		    width: 33%;
		}

		.follow {
		    background: #fff;
		    ul {
		        margin: 0;
		        padding: 0;
		        border-top: 1px solid #ededed;
		        border-bottom: 1px solid #ededed;
		        display: flex;
		        flex-direction: row;
		        justify-content: space-around;
		        text-align: center;
		        li {
		            list-style: none;
		            padding: 13px 0;
		            width: 33%;
		            @media (max-width: 1200px) {
		                padding: 12px 18px;
		            }
		            &:first-child,
		            &:nth-child(2) {
		                border-right: 1px solid #ededed;
		            }
		            h6 {
		                margin: 0;
		            }
		        }
		    }
		    a {
		        color: #ff3a6d;
		        text-align: center;
		        display: block;
		        font-size: 14px;
		        font-weight: 400;
		        padding: 14px 0;
		    }
		}

		.follow-img {
		    text-align: center;
		    padding: 20px 16px;
		    color: #3e434b;
		    img {
		        border-radius: 50%;
		    }
		    h6 {
		        margin: 11px 0 0;
		    }
		}

		.social-counts span {
		    font-size: 14px;
		    color: #737882;
		}

		.map-wrap {
		    padding: 0;
		}

		.map-fix {
		    position: fixed;
		    top: 0;
		    right: 0;
		    bottom: 0;
		    width: 41.7%;
		    @media (max-width: 992px) {
		        display: none;
		        top: 17%;
		        left: 0;
		        width: 100%;
		    }
		    @media (max-width: 480px) {
		        top: 24%;
		    }
		    @media (max-width: 365px) {
		        top: 29.3%;
		    }
		}

		#map {
		    height: 100%;
		    width: 100%;
		}
		.reserve-block {
		    padding: 30px 0;
		    h5 {
		        color: #3e434b;
		        font-weight: 400;
		        display: inline-block;
		        margin: 0;
		    }
		    p {
		        display: inline-block;
		        font-size: 18px;
		        color: #b2b8c3;
		        font-weight: 400;
		        padding: 0 0 0 8px;
		        margin: 0 0 8px;
		        @media (max-width: 992px) {
		            margin: 0;
		        }
		        span {
		            color: #ffb006;
		        }
		    }
		}

		.reserve-description {
		    font-size: 15px !important;
		    font-weight: 400 !important;
		    padding: 0 !important;
		    margin: 0 !important;
		}

		.reserve-seat-block {
		    text-align: right;
		    @media (max-width: 992px) {
		        margin: 15px 0 0;
		    }
		    @media (max-width: 768px) {
		        text-align: left;
		    }
		    @media (max-width: 576px) {
		        text-align: center;
		    }
		}

		.reserve-rating {
		    font-size: 23px;
		    background: #46cd38;
		    padding: 10px 14px;
		    color: #fff;
		    border-radius: 3px;
		    display: inline-block;
		    vertical-align: top;
		}

		.review-btn {
		    display: inline-block;
		    text-align: center;
		    margin: 3px 14px 0 14px;
		    @media (max-width: 1200px) {
		        margin: 3px 4px 0 4px;
		    }
		    @media (max-width: 576px) {
		        margin: 3px 0px 0 0px;
		    }
		    span {
		        display: block;
		        color: #8a8e96;
		        font-size: 13px;
		        margin-top: 3px;
		    }
		    .btn-outline-danger {
		        color: #ff3a6d;
		        border-color: #ff3a6d;
		        &:hover {
		            color: #fff;
		            background: #ff3a6d;
		        }
		    }
		}

		.reserve-btn {
		    display: inline-block;
		    vertical-align: top;
		    @media (max-width: 768px) {
		        margin: -7px 0 0;
		    }
		    @media (max-width: 576px) {
		        margin: 0 0 0;
		        width: 100%;
		    }
		}

		.custom-checkbox {
		    color: #737882;
		    font-size: 14px;
		    line-height: 1.8;
		    margin-right: 0;
		    span {
		        padding: 0 10px 0 0;
		    }
		}

		.customer-content-wrap ul {
		    margin: 24px 0 0;
		    padding: 0;
		}

		.customer-content-wrap ul li {
		    list-style: none;
		    display: inline-block;
		    margin: 0 10px 0 0;
		}


		.btn-contact:hover {
		    background: #ff3a6d;
		    border: 2px solid #ff3a6d;}

		     .follow a {
		    color: #ff3a6d;
		    text-align: center;
		    display: block;
		    font-size: 14px;
		    font-weight: 400;
		    padding: 14px 0;
		}
		.review-btn .btn-outline-danger:hover {
		    color: #fff;
		    background: #ff3a6d;
		}
		.review-btn span {
		    display: block;
		    color: #8a8e96;
		    font-size: 13px;
		    margin-top: 3px;
		}


   </style>
   <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
@endpush
@section('content')
	<div class="clearfix"></div>
      <section class="bg-img" style="background-image: url('{{ $businessData['b_photo'] }}')">
         <div class="text-123">
         <h1>{{ $businessData['b_name'] }}</h1>
        @if(isset($businessData['b_city'])) <p><b>{{ $businessData['b_city']['city_name'] }}  </b><span>{{ $businessData['b_state']['state_name'] }}   </span><span>{{ $businessData['b_country']['country_name'] }}</span></p>@endif

         </div>
      </section>
    <section class="reserve-block">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h5>{{ $businessData['b_name'] }}</h5>
                   
                    <p class="reserve-description">{{ $businessData['business_type'] }}  {{ $businessData['food_type'] }}</p>
                </div>
                {{-- <div class="col-md-6">
                    <div class="reserve-seat-block">
                        <div class="reserve-rating">
                            <span>9.5</span>
                        </div>
                        <div class="review-btn">
                            <a href="#" class="btn btn-outline-danger" data-toggle="modal" data-target="#myModal58">WRITE A REVIEW</a>
                            <span>34 reviews</span>
                        </div>
                    
                    </div>
                </div> --}}
            </div>
        </div>
    </section>
    <section class="light-bg booking-details_wrap" style="background-color: #ddd;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 responsive-wrap">
                    <div class="booking-checkbox_wrap">
                        <div class="booking-checkbox">
                            <p>{{ $businessData['b_desc'] }}</p>
                            <hr>
                        </div>
                        <div class="row">
                         <h5>Available Meals</h5>
                         	@foreach($business['meals'] as $b=>$c)
                         		<div class="col-md-3">
                                	<label class="custom-checkbox">
                           				<i class="fa fa-check"></i>
                        				<span class="custom-control-description">{{ $c }}</span>
                      				</label> 
                      			</div>
                         	@endforeach
                            
                           
                        </div>
                        <div class="row">
                         <h5>Available Cuisines</h5>
                         	@foreach($business['cuisines'] as $b=>$v)
                         		<div class="col-md-3">
                                	<label class="custom-checkbox">
                           				<i class="fa fa-check"></i>
                        				<span class="custom-control-description">{{ $v }}</span>
                      				</label> 
                      			</div>
                         	@endforeach
                            
                           
                        </div>
                    </div>
                    <div class="booking-checkbox_wrap mt-4">
                        <h5>Reviews</h5>
                        <hr>
                        <div class="col-12 err_comment" style="margin-top:10px;display:none">
			                <div class="alert alert-danger alert-dismissible">
			                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			                  
			                    <span class="err_msg_commet"></span>
			                </div>
	    		    	</div>
                        <h5 class="card-header">Write your review:</h5>
                        <form method="post" action="" id="commentform">
                				<input type="hidden" name="busc_cuser_id" value="{{ Session::get('sessionData.id') }}">
                				<input name="_token" type="hidden" value="{{ csrf_token() }}" />
                				<input type="hidden" value="{{ $businessData['b_id'] }}" name="busc_b_id" />
              					<div class="form-group">
                						<textarea class="form-control" rows="3" name="busc_content"></textarea>
                                        <input  type="button" id="postcomment" class="btn btn-primary" value="Submit" onclick="postComment()" style="margin-top:10px">
              					</div>
              						
            				</form>
                       		
                        <hr>
                        <section class="business-comments"> 
		            			@include('users.business-comments')
		        			</section>	
                    </div>
                </div>
                <div class="col-md-4 responsive-wrap">
                    <div class="contact-info">
                    	<input type="hidden" id="address" value="{{ $businessData['b_address'] }}">
						<iframe id="map" width="600" height="450" class="img-responsive" style="height: 300px;"></iframe>
                    	{{-- <iframe width="600" height="450" frameborder="0" style="border:0"
							src="https://www.google.com/maps/embed/v1/place?q=410+walker+street+Lowell+MA+01851&key=AIzaSyDuT1VWD-UWyjlwcFNo3Z_cuPG-00LSzoU"></iframe> --}}
                      {{-- <img src="img/map.jpg" class="img-responsive " alt="#"> --}}
                        <div class="address">
                            <span class="icon-location-pin"></span>
                            <p> @if(isset($businessData['b_city'])) <p><b>{{ $businessData['b_city']['city_name'] }}  </b>@endif<br> {{ $businessData['b_address'] }}</p>
                        </div>
                        @if(isset($businessData['b_contact_person_mobile']))
                        <div class="address">
                            <span class="icon-screen-smartphone"></span>
                            <p>{{ $businessData['b_contact_person_mobile'] }}</p>
                        </div>
                        @endif
                        @if($businessData['b_weblink']!='')
                        <div class="address">
                            <span class="icon-link"></span>
                            <p>{{ $businessData['b_weblink'] }}</p>
                        </div>
                        @endif
                        <div class="address">
                            <span class="icon-clock"></span>
                            @foreach($businessTime as $vt=>$btv)
                            		<p>{{ $btv->daytitle }} {{ $btv->startTime }} - {{ $btv->closeTime }}</p>
                            @endforeach
                            
                        </div>
<!--                         <a href="#" class="btn btn-outline-danger btn-contact">SEND A MESSAGE</a>
 -->                    </div>
                   {{--  <div class="follow">
                        <div class="follow-img">
                            <img src="img/f1.jpg" class="img-fluid" alt="#">
                            <h6>Christine Evans</h6>
                            <span>New York</span>
                        </div>
                        <ul class="social-counts">
                            <li>
                                <h6>26</h6>
                                <span>Listings</span>
                            </li>
                            <li>
                                <h6>326</h6>
                                <span>Followers</span>
                            </li>
                            <li>
                                <h6>12</h6>
                                <span>Followers</span>
                            </li>
                        </ul>
                        <a href="#">FOLLOW</a>
                    </div> --}}
                </div>
            </div>
        </div>
    </section>
@endsection
@push('footer')

	<script type="text/javascript">
		$( document ).ready(function() {
    
		$('.profile-photo').hide();
		});
		function myFunction() {
		    var input, filter, ul, li, a, i;
		    input = document.getElementById("myInput");
		    filter = input.value.toUpperCase();
		    ul = document.getElementById("myUL");
		    li = ul.getElementsByTagName("li");
		    for (i = 0; i < li.length; i++) {
		        a = li[i].getElementsByTagName("a")[0];
		        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
		            li[i].style.display = "";
		        } else {
		            li[i].style.display = "none";
		        }
		    }
		}
		 $( "#searchBox" ).on('click', function() {
          $( ".scrollbar" ).toggle( "slideDown"  );
         
         });
         var q=encodeURIComponent($('#address').val());
       $('#map')
        .attr('src','https://www.google.com/maps/embed/v1/place?key=AIzaSyAd3dZN25HqsQJWq_kCaQSG4hyU8LxN0l8&q='+q);

         function postComment(){
            $('#postcomment').hide();
             $('.form-group').append('<button class="buttonload btn btn-primary" style="margin-top:10px"><i class="fa fa-spinner fa-spin"></i>Processing</button>');
            var url="{{ URL::to('/post-businessComment') }}";
          
                var fd = new FormData($('#commentform')[0]);
                
                 $.ajax({
                    url:url,
                    type:'post',
                    data:fd,
                    processData : false,
                    contentType : false,
                    headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                    success:function(data){
                        $('.buttonload').remove();
                        $('#postcomment').show();
                        
                       if(data==0)
                        {
                             
                             $('#myModal13').modal('show');
                                 
                            $('.alert').show();
                            $('.err_msg').html('First you should login and register in our website.');
                            setTimeout(function() {
                                    $(".alert").hide()
                                }, 3000);
                         }
                        else
                        {
                            
                            $('.err_comment').show();
                            $('.err_msg_commet').html('Commnet posetd successfully,wait for comment approval.');
                            $('.business-comments').html(data);
                            $('#commentform').trigger("reset"); 
                            setTimeout(function() {
                                    $(".alert").hide()
                                }, 3000);
                        }
                        
                    }
                 });
        }
	</script>
@endpush