@include('users.layouts.header')
@if(!Session::get('sessionData'))
	@include('users.layouts.login-register')
@endif
@yield('content')

@include('users.layouts.footer')