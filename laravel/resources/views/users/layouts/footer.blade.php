<nav class=" footer down text-xs-center navbar navbar-dark foot navbar-full" id="#nav-main">
	<ul class="nav navbar-nav">
		<li class="nav-item">
			<a class="nav-link" href="#">About Us</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="#">Contact Us</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="#">Privacy policy</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="#">Terms &amp; use</a>
		</li>

	</ul>
	<ul class="nav navbar-nav float-sm-right">
		<li class="nav-item">
			<a class="nav-link" href="#"> 
			connect with us -</a>
		</li>
		<li class="nav-item">
			<a class="nav-link fb" href="#">
				<i class="fa fa-facebook-official" aria-hidden="true"></i>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link goo" href="#">
				<i class="fa fa-google-plus-square" aria-hidden="true"></i>
			</a>
		</li>

	</ul>
	<div class="clearfix"></div>
	<footer class="footer-copyright">Copyright © 2009-2017 sungare.com All rights reserved.</footer>

</nav>
<script type="text/javascript" src = "{{ asset('js/bootstrap.js') }}"></script>

<script>

	$( "#searchBox" ).on('click', function() {
		$( ".scrollbar" ).toggle( "slideDown"  );

	});

</script>
@stack('footer')

</body>
</html>