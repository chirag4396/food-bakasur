<style type="text/css">
    .err
    {
        color: red !important;
    }
    .form-action input[type="button"] {
        background: #252525 none repeat scroll 0 0;
        border: medium none;
        border-radius: 0;
        box-shadow: none;
        color: #fff;
        display: inline-block;
        float: right;
        font-family: Montserrat,Arial,Helvetica,sans-serif;
        font-size: 12px;
        font-weight: bold;
        height: 40px;
        line-height: 40px;
        padding: 0 15px;
        text-shadow: none;
        text-transform: uppercase;
        transition: all 0.3s ease 0s;
}

 .form-action button {
        background: #252525 none repeat scroll 0 0;
        border: medium none;
        border-radius: 0;
        box-shadow: none;
        color: #fff;
        display: inline-block;
        float: right;
        font-family: Montserrat,Arial,Helvetica,sans-serif;
        font-size: 12px;
        font-weight: bold;
        height: 40px;
        line-height: 40px;
        padding: 0 15px;
        text-shadow: none;
        text-transform: uppercase;
        transition: all 0.3s ease 0s;
}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="modal fade" id="myModal13" tabindex="-1" role="dialog" 
 aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
              &times;</button>
              <h4 class="modal-title" id="myModalLabel">
                 Login Here
             </h4>
         </div>
         <div class="modal-body">
            <div class="row">
                 <div class="col-md-8 extra-w3layouts" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" id="nav">
                       <li class="active"><a href="#Login" data-toggle="tab" id="login-tab">Login</a></li>
                       <li ><a href="#Registration" data-toggle="tab">Register</a></li>
                   </ul>
                   <!-- Tab panes -->
                   <div class="tab-content">
                       <div class="tab-pane fade in active" id="Login">
                       
                          <form action="{{ URL::to('/user-login') }}" method="post" id="loginform" enctype="multipart/form-data">
                             @if(Session::has('message'))
                                <div class="col-12">
                                    <div class="alert alert-danger alert-dismissible">
                                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                      
                                      {{ Session::get('message') }}
                                    </div>
                                </div>
                            @endif
                            <div class="col-12">
                                <div class="alert alert-danger alert-dismissible" style="display: none;">
                                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                      
                                      <span class="err_msg">{{ Session::get('message') }}</span>
                                </div>
                            </div>
                             <div class="form-fields">
                                <h2>Login</h2>
                                <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                                <p>
                                   <label>Username or email address <span class="required">*</span></label>
                                   <input type="text" placeholder="Email" name="email" id="email" value="{{ (count($cookie)>0)? $cookie['cookie_email']:'' }}" />
                                   <p class="err_email err"><span class="err_email_txt"></span></p>
                               </p>
                               <p>
                                   <label>Password <span class="required">*</span></label>
                                   <input type="password" placeholder="Password" name="password" id="password" value="{{ (count($cookie)>0)? $cookie['cookie_password']:'' }}" />
                                   <p class="err_pwd err"><span class="err_pwd_txt"></span></p>
                               </p>
                           </div>
                           <div class="form-action">
                              <p class="lost_password"><a href="#" data-toggle="modal" data-target="#myModal12">Lost your password?</a></p>
                              <div class="buttonshow">
                              <input type="button" value="Login" onclick="formValidate()" class="loginbutton" /></div>
                              <label><input type="checkbox" name="remember_me" />  Remember me </label>
                          </div>
                       </form>
                     </div>
                        <div class="tab-pane fade in " id="Registration">
                            <form action="{{ URL::to('/user-register') }}" method="post" id="registerform" enctype="multipart/form-data">
                                @if(Session::has('message'))
                                    <div class="col-12">
                                        <div class="alert alert-danger alert-dismissible">
                                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                          
                                          {{ Session::get('message') }}
                                        </div>
                                    </div>
                                @endif
                                <div class="form-fields">
                                    <h2>Register</h2>
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                                    <p>
                                       <label>Name  <span class="required">*</span></label>
                                       <input type="text" placeholder="Name" name="name" id="reg_name"/>
                                       <p class="err_reg_name err"><span class="err_reg_name_txt"></span></p>
                                   </p>
                                   <p>
                                       <label>Email address  <span class="required">*</span></label>
                                       <input type="text" placeholder="Email" name="email" id="reg_email"/>
                                       <p class="err_reg_email err"><span class="err_reg_email_txt"></span></p>
                                   </p>
                                   <p>
                                       <label>Mobile No  <span class="required">*</span></label>
                                       <input type="text" placeholder="Mobile No." name="ud_mobile" id="ud_mobile"/>
                                       <p class="err_phone err"><span class="err_phone_txt"></span></p>
                                   </p>
                                   <p>
                                       <label>Password <span class="required">*</span></label>
                                       <input type="password" placeholder="Password" name="password" id="reg_password"/>
                                       <p class="err_reg_password err"><span class="err_reg_password_txt"></span></p>
                                   </p>
                                   <p>
                                       <label>Confirm Password <span class="required">*</span></label>
                                       <input type="password" placeholder="Confirm password" name="confirm_password" id="reg_confirm_password" />
                                       <p class="err_reg_confirm_password err"><span class="err_reg_confirm_password_txt"></span></p>
                                   </p>
                               </div>
                               <div class="form-action">
                                <div class="regibuttondiv">
                                <input type="button" value="Register" onclick="formValidateforRegister()" class="regibutton" />
                              </div>
                              </div>
                            </form>
                        </div>
                   </div>
                    <div id="OR"  >
                       OR
                    </div>
                </div>
                <div class="col-md-4 extra-agileits">
                    <div class="row text-center sign-with">
                       <div class="col-md-12">
                          <h3 class="other-nw">
                             Sign in with
                         </h3>
                        </div>
                        <div class="col-md-12">
                            <div class="btn-group btn-group-justified">
                                <a href="#" class="btn btn-primary">Facebook</a> <a href="#" class="btn btn-danger">
                             Google +</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
@include('users.forgotpass-email');
@push('footer')

    <script type="text/javascript">
        $('#email').keyup(function() {
          if ($('#email').val() != '') {
            var email=$('#email').val();
            if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
            {
               $('.err_email').show();
                    $('.err_email_txt').html('Please enter valid email address');
                    return false; 
            }
            else
            {
                
                $('.err_email').hide();
                             
                      
                           
            }
        } });
        function formValidate()
        {
            var result=1;
            var pwd=$('#password').val();

            
            var inputData=['email','password'];
            var inputErr=['err_email','err_pwd'];
            var inputErrTxt=['err_email_txt','err_pwd_txt'];
            var inputErrMessage=["Please enter your email.","Please enter password."];
            for(var i=0;i<inputData.length;i++)
            {
                if($('#'+inputData[i]).val()=='')
                {
                    $("."+inputErr[i]).show();
                    $("."+inputErrTxt[i]).html(inputErrMessage[i]);
                    result=0;
                    
                }
                else
                {
                    
                    if ($('#email').val() != '') {
                        var email=$('#email').val();
                        if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
                        {
                           $('.err_email').show();
                           $('.err_email_txt').html('Please enter valid email address');
                               result=0;
                        }
                        else
                        {
                            $('.err_email').hide();
                                    
                        }
                    }
                    $("."+inputErrTxt[i]).html('');
                        $("."+inputErr[i]).hide();
                }
            }
            if(result==1)
            {
              $('.loginbutton').hide();
               $('.buttonshow').append('<button class="buttonload btn btn-primary" style="float:right" ><i class="fa fa-spinner fa-spin"></i>Processing</button>');
                //$('#loginform').submit();
                var ajaxURL="{{ URL::to('/user-login') }}";
                var fd = new FormData($('#loginform')[0]);
                 $.ajax({
                    url:ajaxURL,
                    type:'post',
                    data:fd,
                    processData : false,
                    contentType : false,
                    xhrFields: { withCredentials: true },
                    headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data){
                      if(typeof(data)=='object')
                        {
                          $('.buttonload').remove();
                           location.reload();
                        }
                        else
                        {
                          $('.buttonload').remove();
                          $('.loginbutton').show();
                            $('.alert').show();
                                $('.err_msg').html(data);
                                  setTimeout(function() {
                                    $(".alert").hide()
                                }, 3000);
                        }
                        console.log(data);
                        
                    },
                    error: function (data) {
                          console.log(data);
                      }
                 });
            }
        }
        //  setTimeout(function() {
        //     $(".alert").hide()
        // }, 5000);


         /*---- Register ---*/
         $('#reg_confirm_password').keyup(function() {
          if ($('#reg_confirm_password').val() != '') {
            var pwd=$('#reg_password').val();
            var c_pwd=$('#reg_confirm_password').val();
            if(pwd!=c_pwd)
            {
               $('.err_reg_confirm_password').show();
                    $('.err_reg_confirm_password_txt').html('Password and Confirm Password should be same.');
                    return false; 
            }
            else
            {
                $('.err_reg_confirm_password').hide();
                return true; 
            }
        } });
         function formValidateforRegister()
        {
            var result=1;
            var pwd=$('#reg_password').val();

            var c_pwd=$('#reg_confirm_password').val();
            var inputData=['reg_name','reg_email','reg_password','reg_confirm_password'];
            var inputErr=['err_reg_name','err_reg_email','err_reg_password','err_reg_confirm_password'];
            var inputErrTxt=['err_reg_name_txt','err_reg_email_txt','err_reg_password_txt','err_reg_confirm_password_txt'];
            var inputErrMessage=["Please enter your name.","Please enter your email.","Please enter password.",'Please enter password again for confirmation.'];
            for(var i=0;i<inputData.length;i++)
            {
                if($('#'+inputData[i]).val()=='')
                {
                    $("."+inputErr[i]).show();
                    $("."+inputErrTxt[i]).html(inputErrMessage[i]);
                    result=0;
                    
                }
                else
                {
                    
                    if ($('#reg_email').val() != '') {
                        var email=$('#reg_email').val();
                        if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
                        {
                           $('.err_reg_email').show();
                           $('.err_reg_email_txt').html('Please enter valid email address');
                               result=0;
                        }
                        else
                        {
                            fd=[email];
                            var url="{{ URL::to('/email-check') }}"
                            $.ajax({
                            type:'POST',
                            url:url,
                            data:{'email':email},
                            async: false,
                            headers: {
                                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success:function(data){
                                
                                    if(data=='exists')
                                    {
                                        
                                         $('.err_reg_email').show();
                                         $('.err_reg_email_txt').html('Email already exists, please enter another one.');
                                         result=0;
                                        
                                    }else
                                    {
                                         $('.err_reg_email').hide();
                                    }
                                }
                            });

                        }
                    }
                    if(pwd!=c_pwd && pwd!='')
                    {
                       $('.err_reg_confirm_password').show();
                       $('.err_reg_confirm_password_txt').html('Password and Confirm Password should be same.');
                            result=0;
                    }
                    else
                    {
                        //$('.err_reg_confirm_password').hide();
                        $("."+inputErrTxt[i]).html('');
                        $("."+inputErr[i]).hide();
                        
                    }
                    /*$("."+inputErrTxt[i]).html('');
                    $("."+inputErr[i]).hide();*/
                }
            }

            if(result==1)
            {
              $('.regibutton').hide();
               $('.regibuttondiv').append('<button class="buttonload btn btn-primary" style="float:right" ><i class="fa fa-spinner fa-spin"></i>Processing</button>');
                var ajaxURL="{{ URL::to('/user-register') }}";
                var fd = new FormData($('#registerform')[0]);
                alert
                 $.ajax({
                    url:ajaxURL,
                    type:'post',
                    data:fd,
                    processData : false,
                    contentType : false,
                    async: true,
                    headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data){
                        
                       if(data=="success")
                       {
                        $('.regibutton').show();
                        $('.buttonload').remove();
                        $("#registerform").trigger("reset");
                       $('.nav a[href="#Login"]').trigger('click');
                                    
                                  
                            $("#Registration").removeClass("active");
                            $("#Login").addClass("active");  
                          
                                    
                               
                            $('.alert').show();
                            $('.err_msg').html("Thank you for register with food bakasur,to confirm your account pleasecheck your mail."); 
                             setTimeout(function() {
                                    $(".alert").hide()
                                }, 3000);
                       }
                        
                    }
                 });
               
            }
            return false;
        }
        
        $('#reg_email').keyup(function() {
          if ($('#reg_email').val() != '') {
            var email=$('#reg_email').val();
            if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
            {
               $('.err_reg_email').show();
                $('.err_reg_email_txt').html('Please enter valid email address');
                    return false; 
            }
            else
            {
                fd=[email];
                var url="{{ URL::to('/email-check') }}"
                $.ajax({
                type:'POST',
                url:url,
                data:{'email':email},
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                   
                        if(data=='exists')
                        {
                            
                             $('.err_reg_email').show();
                             $('.err_reg_email_txt').html('Email already exists, please enter another one.');
                            
                             return false;
                        }else
                        {
                             $('.err_reg_email').hide();
                              return true; 
                        }
                    }
                });
              
                           
            }
        } });
        $("#ud_mobile").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
        $('#reg_name').bind('keypress', function (event) {
          var regex = new RegExp("^[a-zA-Z, ,\t]+$");
          var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
          if (!regex.test(key)) {
             event.preventDefault();
             return false;
          }
      });
    </script>
@endpush