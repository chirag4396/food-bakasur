<html>
<head>
	<title>online portal</title>
</head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/boot.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<link href="{{ asset('font/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
@stack('header')
<header>
	<div class="container clearfix">
		<nav class="navbar navbar-fixed-top navbar bg-inverse">
			<a class="navbar-brand col-lg-4 col-md-5 col-xs-12 col-sm-3 col-xl-3" >
				<div class="logo">
					<img src="{{ asset('img/logo.jpg') }}">
				</div>
			</a>
			<ul class="nav navbar-nav col-lg-6  col-md-6  col-xl-3 col-xs-12 col-sm-8 float-xs-right">
				<li>
					<a class="btn create col-sm-6  col-xs-6 col-xl-5" href="{{ URL::to('/foodbakasur-listing') }}" {{-- data-toggle="modal" data-target="#credentialModel" --}}> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Freelisting</a>
				</li>
				<li>
					@if(Session::has('sessionData'))
						<a href="{{ URL::to('user-profile') }}"><button class="btn btn-info col-sm-3 col-xs-6"  class="login-regi">Profile</button></a>
						<a href="{{ URL::to('user-logout') }}"><button class="btn btn-primary col-sm-3 col-xs-6"  class="login-regi">LogOut</button></a>
					@else
					 <button class="btn btn-primary col-sm-6 col-xs-6" data-toggle="modal" data-target="#myModal13" class="login-regi">Login / Sign Up</button>
					 @endif
				</li>
			</ul>
		</nav>

	</div>
</header>