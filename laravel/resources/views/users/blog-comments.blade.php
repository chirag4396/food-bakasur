<div id="blogcomment-div"  style="position: relative;">
@foreach($blogComments as $b=>$blogcom)
<div class="media mb-4">
  	<img class="d-flex mr-3 rounded-circle" src="{{ $blogcom->cu_image }}" alt="" style="height: 50px;width: 50px;">
	  <div class="media-body">
	    <h5 class="mt-0">{{ $blogcom->comment_user }}</h5>
	    <p>{{ $blogcom->c_content }}</p>
	  </div>
</div>
@endforeach
</div>