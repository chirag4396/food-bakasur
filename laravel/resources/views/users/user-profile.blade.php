@extends('users.layouts.master')
@push('header')
	<style type="text/css">
	
	.loader
{
display : none;
}
.loader
{
display : block;
position : fixed;
z-index: 100;
background-image : url('http://loadinggif.com/images/image-selection/3.gif');
background-color:#666;
opacity : 0.4;
background-repeat : no-repeat;
background-position : center;
left : 0;
bottom : 0;
right : 0;
top : 0;
}
.loader
{
left : 50%;
top : 50%;
position : absolute;
z-index : 101;
width : 32px;
height : 32px;
margin-left : -16px;
margin-top : -16px;
}
</style>
@endpush
@section('content')
	 <section class="food-tabs" style="margin-top: 10%;">
         <div class="container">
            <div class="col-lg-4  col-xl-3 col-xs-12 main-left-edit ">
               <div class="col-lg-12 col-md-6 col-sm-6  " style="">
                  <center><img class="img4" src="{{ $userDetails['ud_profile_img'] }}" alt="images/users/1470242246.jpg" title="Chirag Khatri"></center>
               </div>
               <div class="col-lg-12">
                  <h3 class="dark " style="text-align: center;"> {{-- <i class="fa fa-user" aria-hidden="true"></i> --}} {{ $userData['name'] }}</h3>
               </div>
               <div class="col-xs-12 col-sm-5 col-lg-12 ">
                  <button class="btn col-xs-5 col-md-5 col-lg-5  btn-follow  font-small"> {{ $userDetails['ud_followers'] }}  Follow</button>
                  <button class="btn col-xs-7 col-md-7 col-lg-7  btn-follow  font-small"> {{ $userDetails['ud_followings'] }}  Following</button>
               </div>
            </div>
            <div class="col-lg-8 col-sm-8 col-xs-12">
               <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
                  <div class="btn-group" role="group">
                     <button type="button" id="stars" class="btn btn-default" href="#tab1" style="width: 170px;" data-toggle="tab">
                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        <div class="hidden-xs">About Us</div>
                     </button>
                  </div>
                  <div class="btn-group" role="group">
                     <button type="button" id="favorites" class="btn btn-default" href="#tab2" style="width: 170px;" data-toggle="tab">
                        <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                        <div class="hidden-xs">blogs posted</div>
                     </button>
                  </div>
                  <div class="btn-group" role="group">
                     <button type="button" id="following" class="btn btn-default" style="width: 170px;" href="#tab3" data-toggle="tab">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        <div class="hidden-xs">Posted Recipie </div>
                     </button>
                  </div>
                  <div class="btn-group" role="group">
                     <button type="button" id="following" class="btn btn-default" style="width: 170px;" href="#tab4" data-toggle="tab">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        <div class="hidden-xs">Listed Buisness</div>
                     </button>
                  </div>
               </div>
               <div class="well" style="border: 1px solid #ddd; background-color: #fff;">
                  <div class="tab-content">
                     <div class="tab-pane fade in active" id="tab1">
                     	<form action="{{ URL::to('update-userprofile') }}" method="post" enctype="multipart/form-data">
                        <h4 class="head" style="font-size: 20px;margin-left: 10px;">About Me</h4>
                         @if(Session::has('message'))
	                        <div class="col-md-offset-4 col-md-12 col-xs-12">
	                            <div class="alert alert-danger alert-dismissible">
	                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                              
	                              {{ Session::get('message') }}
	                            </div>
	                        </div>
	                    @endif
                        <div class="border-line3"></div>
                        <input type="hidden" name="id" value="{{ $userData['id'] }}">
                        <input type="hidden" name="ud_id" value="{{ $userDetails['ud_id'] }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row" style="display: block;margin:0 auto;">
                           <div class="col-md-offset-4 col-md-12 col-xs-12" >
                              <div class="checkout-form-list">
                                 <input type="text" placeholder="Name" value="{{ $userData['name'] }}" name="ud_name" maxlength="50" onkeypress="return isCharacterKey(event)" />
                              </div>
                           </div>
                           <div class="col-md-offset-3 col-md-12 col-xs-12">
                              <div class="checkout-form-list">
                                 <input type="text" placeholder="Mobile No" value="{{ $userDetails['ud_mobile'] }}" name="ud_mobile"  onkeypress="return isNumberKey(event)"  maxlength="10"/>
                              </div>
                           </div>
                           <div class="col-md-offset-4 col-md-12 col-xs-12">
                              <div class="checkout-form-list">
                                 <div class="row">
                                    <div class="col-sm-4 col-xs-12" style="padding-left: 15px;padding-right: 0px;">
                                       <label for="male">Male</label>
                                       <input type="radio" name="ud_gender" id="male" value="0" {{ ($userDetails['ud_gender']==0)?'checked':'' }}>
                                    </div>
                                    <div class="col-sm-4 col-xs-12" style="padding-left:15px;padding-right: 0px;">
                                       <label for="female">Female</label>
                                       <input type="radio" name="ud_gender" id="female" value="1" {{ ($userDetails['ud_gender']==1)?'checked':'' }}>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-offset-3 col-md-12 col-xs-12">
                              <div class="checkout-form-list">
                                 <label>Upload  Photo </label>
                                 @if($userDetails['pic_status']==0)

                                 <input type="file" placeholder="image" name="ud_profile_img"/>
                                 @else
                                 	{{-- <img src="{{ $userDetails['ud_profile_img'] }}" width="200px" height="200px"><br> --}}
                                 	<input type="file" placeholder="image" name="ud_profile_img"/>
                                 @endif
                              </div>
                           </div>
                           <div class="col-md-offset-3 col-md-12 col-xs-12">
                              <div class="checkout-form-list">
                                 <textarea id="checkout-mess" rows="5" placeholder="About yourself
                                    "  style="width: 100%;" name="ud_about">{{ $userDetails['ud_about'] }}</textarea>
                              </div>
                           </div>
                           <div class="col-md-offset-3 col-md-12 col-xs-12">
                       			<input type="submit" name="" value="Update Profile" class="btn btn-primary mt">
                              {{-- <button class="">Update</button> --}}
                           </div>
                        </div>
                    	</form>
                     </div>
                     <div class="tab-pane fade in" id="tab2">
                        <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
                           <div class="btn-group" role="group">
                              <button type="button" id="stars" class="btn btn-primary" href="#t3" style="width: 160px;" data-toggle="tab">
                                 <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                 <div class="hidden-xs">Published</div>
                              </button>
                           </div>
                           <div class="btn-group" role="group">
                              <button type="button" id="favorites" class="btn btn-default" href="#t4" style="width: 160px;" data-toggle="tab">
                                 <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                                 <div class="hidden-xs">Unpublished</div>
                              </button>
                           </div>
                        </div>
                        <div class="tab-content" >
                           <div id="t3" class="tab-pane fade in active">
                              <div class="row row1">
                              		@if(count($blogDatapublish)>0)
	                              		<section class="articles">
	                                 		@include('users.blogs-load')
	                                 	</section>
	                                 @else
                                 		<center><h2>No any blog available</h2></center>
                                 	@endif
								</div>
                            </div>
                             <div id="t4" class="tab-pane fade" >
                             	 <div class="row row1">
                             	 	@if(count($blogDataunpublish)>0)
	                             	 	<section class="articles1">
	                                 		@include('users.blog-load-unpublish')
	                                 	</section>
	                                  @else
                                 		<center><h2>No any blog available</h2></center>
                                 	@endif
                             	 </div>
                             </div>
                            </div>
                        </div>
                     <div class="tab-pane fade in" id="tab3">
                        <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
                           <div class="btn-group" role="group">
                              <button type="button" id="stars" class="btn btn-default" href="#tab21" style="width: 160px;" data-toggle="tab">
                                 <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                 <div class="hidden-xs">Published</div>
                              </button>
                           </div>
                           <div class="btn-group" role="group">
                              <button type="button" id="favorites" class="btn btn-default" href="#tab22" style="width: 160px;" data-toggle="tab">
                                 <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                                 <div class="hidden-xs">Unpublished</div>
                              </button>
                           </div>
                        </div>
                        <div class="tab-content">
                           <div id="tab21" class="tab-pane fade in active">
                              <div class="row row1">
                              		@if(count($recepieDatapublish)>0)
		                                 <section class="publish-recipe">
		                                 	@include('users.recipe-publish')
		                                 </section>
		                            @else
                                 		<center><h2>No any Recipe available</h2></center>
                                 	@endif
                             </div>

                           </div>
                           <div id="tab22" class="tab-pane fade in">
                               <div class="row row1">
                               	@if(count($recepieDataunpublish)>0)
                                 <section class="unpublish-recipe">
                                 	@include('users.recipe-unpublish')
                                 </section>
                                @else
                             		<center><h2>No any Recipe available</h2></center>
                             	@endif
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade in" id="tab4">
                        <div class="container">
                           <ul class="nav nav-tabs">
                              <li><a data-toggle="tab" href="#tabd" style="border:1px solid #eee; background: #FF1253; color:#fff;
                                 padding: 15px;">Publisheed</a></li>
                              <li><a data-toggle="tab" href="#taba" style="border:1px solid #eee; background: #FF1253; color:#fff; padding: 15px;">Unpublished</a></li>
                           </ul>
                           <div class="tab-content">
                              <div id="tabd" class="tab-pane fade in active">
                                 <div class="row row1">
                                 	@if(count($businessesPublish)>0)
	                                    <section class="publish-bus">
	                                    	@include('users.business-publish')
	                                    </section>
                                     @else
	                             		<center><h2>No any Business available</h2></center>
	                             	@endif
                                 </div>
                                 
                              </div>
                              <div id="taba" class="tab-pane fade in">
                                 <div class="row row1 ">
                                     @if(count($businessesunPublish)>0)
	                                    <section class="unpublish-bus">
	                                    	@include('users.business-unpublish')
	                                    </section>
                                     @else
	                             		<center><h2>No any Business available</h2></center>
	                             	@endif
                                 </div>
                              </div>
                           </div>
                        </div>
                        <hr>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      @push('footer')

    <script type="text/javascript">
    	/* Pagination for blog publish */
    	$('body').on('click', '#pagination a', function(e) {
                e.preventDefault();
               
                

                var url = $(this).attr('href');
                getArticles(url+'&for='+'publish');
                window.history.pushState("", "", url);
            });
    	 function getArticles(url) {
		   

		   $.ajax({
	            url : url ,
	        }).done(function (data) {
	            $('.articles').html(data);  
	        });
		}
		/* Pagination for blog unpublish */
		$('body').on('click', '#pagination1 a', function(e) {
                e.preventDefault();

                

                var url = $(this).attr('href');
                getArticlesforUnpublish(url+'&for='+'unpublish');
                window.history.pushState("", "", url);
            });
    	 function getArticlesforUnpublish(url) {
		   

		   $.ajax({
	            url : url ,
	        }).done(function (data) {
	            $('.articles1').html(data);  
	        });
		}

		/* Pagination for recipe publish */
		$('body').on('click', '#paginationforrecipe a', function(e) {
                e.preventDefault();

               

                var url = $(this).attr('href');
                getRecipeforPublish(url+'&for='+'recipe-publish');
                window.history.pushState("", "", url);
            });
    	 function getRecipeforPublish(url) {
		   

		   $.ajax({
	            url : url ,
	        }).done(function (data) {
	        	
	            $('.publish-recipe').html(data);  
	        });
		}

		/* Pagination for recipe unpublish */
		$('body').on('click', '#paginationforunpublishrecipe a', function(e) {
                e.preventDefault();

               

                var url = $(this).attr('href');
                getRecipeforUnPublish(url+'&for='+'recipe-publish');
                window.history.pushState("", "", url);
            });
    	 function getRecipeforUnPublish(url) {
		   

		   $.ajax({
	            url : url ,
	        }).done(function (data) {
	            $('.unpublish-recipe').html(data);  
	        });
		}

		/* Pagination for business publish */
		$('body').on('click', '#paginationforbus a', function(e) {
                e.preventDefault();
            var url = $(this).attr('href');
                getBusiforPublish(url+'&for='+'bus-publish');
                window.history.pushState("", "", url);
            });
    	 function getBusiforPublish(url) {
		   

		   $.ajax({
	            url : url ,
	        }).done(function (data) {
	            $('.publish-bus').html(data);  
	        });
		}

		/* Pagination for business unpublish */
		$('body').on('click', '#paginationforunbus a', function(e) {
                e.preventDefault();
            var url = $(this).attr('href');
                getBusforUnPublish(url+'&for='+'bus-unpublish');
                window.history.pushState("", "", url);
            });
    	 function getBusforUnPublish(url) {
		      $.ajax({
	            url : url ,
	        }).done(function (data) {
	            $('.unpublish-bus').html(data);  
	        });
		}

      	setTimeout(function() {
            $(".alert").hide()
        }, 3000)
        function isNumberKey(evt)
	      {
	         var charCode = (evt.which) ? evt.which : event.keyCode
	         if (charCode > 31 && (charCode < 48 || charCode > 57))
	            return false;
	 
	         return true;
	      }
	      function isCharacterKey(evt)
	      {
	      	  var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
		          ((evt.which) ? evt.which : 0));
		       if (charCode > 31 && (charCode < 65 || charCode > 90) &&
		          (charCode < 97 || charCode > 122)) {
		          
		          return false;
		       }
		       return true;
	      }
  </script>
@endpush
@endsection
