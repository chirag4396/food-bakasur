@extends('users.layouts.master')
@push('header')
	<style type="text/css">
		.thumb{
		    margin: 10px 5px 10px 0;
		    width: 200px;
		    height: 200px;
		}
		.err
		{
			display: none;
			color: red;
		}
		table {
		    border-collapse: collapse;
		    width: 100%;
		}

		table, th, td {
		    border: 1px solid #e5e5e5;
		}
		th,td
		{
			padding: 10px;
			text-align: center;
		}
		.form-control
		{
			font-size:16px !important;
		}
	</style>
	
	<link rel="stylesheet" type="text/css" href="{{ asset('backend/clockpicker/dist/bootstrap-clockpicker.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('backend/clockpicker/assets/css/github.min.css') }}">
	
	 {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> --}}
@endpush
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="main-top">
	<div class="container white" style="padding: 15px;">
		<h3 class="head">{{ $mode }} Your Business</h3>
		<div class="border-line3"></div>
<!-- checkout-area start -->
		<div class="checkout-area">
		   	<div class="container">
		      	<div class="row">
		      		@if($mode=='Update')
		      			@php $url='/update-business'; @endphp
		      		@else
		      			@php $url='/add-business'; @endphp
		      		@endif
		         	<form action="{{ URL::to($url) }}" method="post" id="businessAddForm" enctype="multipart/form-data">
		         		@if($mode=='Update')
		         			<input type="hidden" name="b_id" value="{{ $busData['b_id'] }}">
		         		@endif
		         		<input name="_token" type="hidden" value="{{ csrf_token() }}" />
		         		<input type="hidden" name="search_city" id="hiddenCity">
			            <div class="col-lg-6 col-md-6">
			               <div class="checkbox-form">
			                  <div class="row">
			                     <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Name <span class="required">*</span></label>
			                           <input type="text" placeholder="Name" name="b_name" id="b_name" value="{{ ($mode=='Update')? $busData['b_name'] : '' }}" />
			                           <p class="err_b_name err"><span class="err_bname_txt"></span></p>
			                        </div>
			                     </div>
			                     <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Type of Business <span class="required">*</span></label>
			                           <select class="form-control" name="b_bt_id" id="b_bt_id">
			                           		<option value="">--Select business type--</option>
			                           		@foreach($businesstypes as $k=>$bt)
			                           			<option value="{{ $bt->bt_id }}" @if($mode=='Update'){{ ($busData['b_bt_id']==$bt->bt_id)?'selected':'' }}@endif>{{ $bt->bt_name }}</option>
			                           		@endforeach
			                           		<option value="0" @if($mode=='Update'){{ ($busData['b_bt_id']=="0")?'selected':'' }}@endif>Other</option>
			                           </select>
			                           <p class="err_bt_id err"><span class="err_btid_txt"></span></p>
			                        </div>
			                     </div>
			                      <div class="col-md-12 othername_div" @if($mode=='Update' && $busData['b_bt_id']==0)style="display: block @else style='display: none' @endif">
			                        <div class="checkout-form-list">
			                           <label>Other Business Name<span class="required">*</span></label>
			                           <input type="text" name="b_other_name" id="b_other_name" value="{{ ($mode=='Update')? $busData['b_other_name'] : '' }}">
			                           <p class="err_other err"><span class="err_other_txt"></span></p>
			                        </div>
			                     </div>
			                     
			                     	<div class="col-md-6">
				                        <div class="checkout-form-list">
				                           <label>Upload your Logo </label>
				                           <div id="thumb-output"></div>
				                           <div id="thumb-default"><img src="@if($mode=='Update' && $busData['b_logo']!='') {{ $busData['b_logo'] }} @else {{ asset('img/thumb-place2.png') }} @endif"  class="thumb"></div>
				                           <input type="file" placeholder="Address"  name="b_logo" class="b_logo" id="file-input"/>
				                           <p class="err_logo err"><span class="err_logo_txt">Only jpeg,png or jpg files are allowed.</span></p>
				                        </div>
				                        
				                    </div>
				                    <div class="col-md-6">
				                        <div class="checkout-form-list">
				                           <label>Upload Photo</label>
				                           <div id="thumb-outputphoto"></div>
				                           <div id="thumb-defaultphoto"><img src="@if($mode=='Update' && $busData['b_photo']!='') {{ $busData['b_photo'] }} @else {{ asset('img/thumb-place2.png') }} @endif"   class="thumb"></div>
				                           <input type="file" placeholder="Address" id="file-inputphoto" name="b_photo" class="b_photo" />
				                           <p class="err_image err"><span class="err_image_txt">Only jpeg,png or jpg files are allowed.</span></p>
				                        </div>
				                    </div>
				                    <div class="col-md-12">
				                        <div class="checkout-form-list">
				                           <label>Available Meals <span class="required">*</span></label>
				                          <select class="form-control" name="b_meal_id[]" id="b_meal_id" multiple="">
						                     
						                     @foreach($meals as $m=>$mData)
						                     	<option value="{{ $mData->meal_id }}" @if($mode=='Update'){{ in_array($mData->meal_id,$busData['b_meal_id'])?'selected':'' }}@endif>{{ $mData->meal_title }}</option>
						                     @endforeach
						                  </select>
				                           <p class="err_meal err"><span class="err_meal_txt"></span></p>
				                        </div>
				                     </div>
				                     <div class="col-md-12">
				                        <div class="checkout-form-list">
				                           <label>Available Cuisines <span class="required">*</span></label>
				                          <select class="form-control" name="b_cuisine_id[]" id="b_cuisine_id" multiple="">
						                     @foreach($cuisines as $c=>$cData)
						                     	<option value="{{ $cData->cu_id }}" @if($mode=='Update'){{ in_array($cData->cu_id,$busData['b_cuisine_id'])?'selected':'' }}@endif>{{ $cData->cu_title }}</option>
						                     @endforeach
						                  </select>
				                           <p class="err_cuisines err"><span class="err_cuisines_txt"></span></p>
				                        </div>
				                     </div>
				                      <div class="col-md-12">
				                        <div class="checkout-form-list">
				                           <label>Available Food Type <span class="required">*</span></label>
				                          <select class="form-control" name="b_food_type[]" id="b_food_type" multiple="">
						                     <option value="0" @if($mode=='Update'){{ in_array("0",$busData['b_food_type'])?'selected':'' }}@endif>Veg</option>
						                     <option value="1" @if($mode=='Update'){{ in_array("1",$busData['b_food_type'])?'selected':'' }}@endif>Non Veg</option>
						                  </select>
				                           <p class="err_food err"><span class="err_food_txt"></span></p>
				                        </div>
				                     </div>
			                     <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Contact Person Name</label>
			                           <input type="text" placeholder="Contact Person Name" name="b_contact_person_name" id="b_contact_person_name" value="{{ ($mode=='Update')? $busData['b_contact_person_name'] : '' }}"/>
			                           <p class="err_contactname err"><span class="err_contactname_txt"></span></p>
			                        </div>
			                     </div>

			                     
			                    
			                  </div>
			               </div>
			            </div>
			            <div class="col-lg-6 col-md-6">
			               <div class="checkbox-form">
			                  <div class="row">
			                  	<div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Contact No.</label>                   
			                           <input type="text" placeholder="Contact Number" name="b_contact_person_mobile" id="b_contact_person_mobile"  value="{{ ($mode=='Update')? $busData['b_contact_person_mobile'] : '' }}"/>
			                            <p class="err_c_mobile err"><span class="err_c_mobile_txt"></span></p>
			                        </div>
			                       
			                     </div>
			                  	<div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Address <span class="required">*</span></label>
			                           <input type="text" placeholder="Address" name="b_addresses" id="b_addresses" value="{{ ($mode=='Update')? $busData['b_address'] : '' }}"/>
										<p class="err_addressess err"><span class="err_address_txt"></span></p>
			                        </div>
			                     </div>
			                  	<div class="col-md-12 order-notes">
			                        <div class="checkout-form-list">
			                           <label>Description <span class="required">*</span></label>
			                           <textarea  cols="30" rows="10" placeholder="Business Description" name="b_desc" id="b_desc">{{ ($mode=='Update') ? $busData['b_desc'] : ''}}</textarea>
			                           <p class="err_desc err"><span class="err_desc_txt"></span></p>
			                        </div>
			                     </div>
			                     <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Website Link </label>                   
			                           <input type="text" placeholder="Website Link" name="b_weblink" id="b_weblink" value="{{ ($mode=='Update') ? $busData['b_weblink'] : ''}}" />
			                           <p class="err_weblink err"><span class="err_weblink_txt"></span></p>
			                        </div>
			                     </div>
			                     <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Country </label>                   
			                           <select  name="country_id" id="country_id" class="form-control" onchange="getState()">
			                           			<option value="" selected="">--Select Country---</option>
			                           		@foreach($countries as $c=>$cdes)
			                           				<option value="{{ $cdes->country_id }}" @if($mode=='Update'){{ ( $cdes->country_id ==$busData['b_country_id'])?'selected':'' }}@endif>{{ $cdes->country_name }}</option>
			                           		@endforeach
			                           </select>
			                           <p class="err_country err"><span class="err_country_txt"></span></p>
			                        </div>
			                     </div>
			                      <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>State </label>                   
			                           <select name="state_id" class="form-control" id="state_id" onchange="getCity()">
			                           	<option value="" selected="">--Select State---</option>
			                           		@if($mode=='Update')
			                           			@foreach($states as $s=>$sdes)
			                           				<option value="{{ $sdes->state_id }}" {{ ( $sdes->state_id ==$busData['b_state_id'])?'selected':'' }}>{{ $sdes->state_name }}</option>
			                           			@endforeach
			                           		@endif
			                           </select>
			                           <p class="err_state err"><span class="err_state_txt"></span></p>
			                        </div>
			                     </div>
			                     <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>City </label>                   
			                           <select name="b_city_id" class="form-control" id="city_id">
			                           		<option value="" selected="">--Select City---</option>
			                           		@if($mode=='Update')
			                           			@foreach($cities as $s=>$csdes)
			                           				<option value="{{ $csdes->city_id }}"  {{ ( $csdes->city_id ==$busData['b_city_id'])?'selected':'' }}>{{ $csdes->city_name }}</option>
			                           			@endforeach
			                           		@endif
			                           </select>
			                           <p class="err_city err"><span class="err_city_txt"></span></p>
			                        </div>
			                     </div>
			                      <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Open Days and Their Timing</label>         <table>
			                           	<thead><tr><th>Open Days</th><th>Open Time</th><th>Close Time</th></tr></thead>
			                           	<tbody>
			                           		@foreach($days as $d=>$v)
			                           			<tr>
			                           				<td>{{ $v->day_title }}</td>
			                           				<td><div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true"><input type="text" name='start[{{ $v->day_id }}]'   readonly='true' value="@if($mode=='Update') @if(in_array($v->day_id, $timeArr['day']))@php $index=array_search($v->day_id, $timeArr['day']); @endphp {{ $timeArr['open_time'][$index] }} @endif @endif"></div></td> 
			                           				<td><div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true"><input type="text" name='close[{{ $v->day_id }}]'   readonly='true' value="@if($mode=='Update') @if(in_array($v->day_id, $timeArr['day']))@php $index=array_search($v->day_id, $timeArr['day']); @endphp {{ $timeArr['close_time'][$index] }} @endif @endif"></div></td>
			                           			</tr>
			                           		@endforeach
			                           		
			                  		 </tbody>
			                           </table>          
			                           
			                        </div>
			                     </div>
			                  </div>
			                </div>
			            </div>
			            <div class="col-md-12">
			            <div class="col-md-3"></div>
			            <div class="col-md-6">
	                        <div class="order-button-payment">
	                           <center><input type="button" value="Add Your Business" onclick="formValidate()" /></center>
	                        </div>
	                    
	                     </div>
			            <div class="col-md-3"></div>
			           </div>
		         	</form>
		        </div>
		    </div>
		</div>
      <!-- checkout-area end -->  
   </div>
</div>
@endsection
@push('footer')

	<script type="text/javascript" src="{{ asset('backend/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
	<script type="text/javascript">

		$('.clockpicker').clockpicker();
	    $('#b_contact_person_mobile').keyup(function() {
	      if ($('#b_contact_person_mobile').val() != '') {
	        var a = $('#b_contact_person_mobile').val();
	         var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
	        if(a.match(filter)==null)
	        {
	           $('.err_c_mobile').show();
	                $('.err_c_mobile_txt').html('Phone Number must be in digit and have atleast 10 digits.');
	                return false;
	        }
	        else
	        {
	            $('.err_c_mobile').hide();
	        }
	    } });
		 $('#b_bt_id').change(function(){
		 
	        if($('#b_bt_id').val() == 0) {
	            $('.othername_div').show(); 
	        } else {
	            $('.othername_div').hide(); 
	        } 
	    });
		$(window).on('load', function(){
		    $('#file-input').val('');
		    $('#file-inputphoto').val('');
		    $(".btn-sm").html('Save');
		});
		$(document).ready(function(){
			
		    $('#file-input').on('change', function(){ //on file input change
		        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
		        {
		            $('#thumb-output').html(''); //clear html of output element
		            var data = $(this)[0].files; //this file data
		           
		            $.each(data, function(index, file){ //loop though each file
		                if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
		                    var fRead = new FileReader(); //new filereader
		                    fRead.onload = (function(file){ //trigger function on successful read
		                    return function(e) {
		                        var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
		                        $('#thumb-default').hide(); 
		                        $('#thumb-output').append(img); //append image to output element
		                    };
		                    })(file);
		                    fRead.readAsDataURL(file); //URL representing the file's data.
		                }
		            });
		           
		        }else{
		        	$('#file-input').html('');
		             //if File API is absent
		        }
		    });
		    $('#file-inputphoto').on('change', function(){ //on file input change
		        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
		        {
		            $('#thumb-outputphoto').html(''); //clear html of output element
		            var data = $(this)[0].files; //this file data
		           
		            $.each(data, function(index, file){ //loop though each file
		                if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
		                    var fRead = new FileReader(); //new filereader
		                    fRead.onload = (function(file){ //trigger function on successful read
		                    return function(e) {
		                        var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
		                        $('#thumb-defaultphoto').hide(); 
		                        $('#thumb-outputphoto').append(img); //append image to output element
		                    };
		                    })(file);
		                    fRead.readAsDataURL(file); //URL representing the file's data.
		                }
		            });
		           
		        }else{
		            alert("Your browser doesn't support File API!"); //if File API is absent
		        }
		    });
		   
		});
		function formValidate()
		{
			var result=1;
			var logo=$('.b_logo').val();
			var photo=$('.b_photo').val();
			var inputData=['b_name','b_addresses','b_desc','b_bt_id','b_meal_id','b_cuisine_id','b_food_type','city_id','country_id','state_id'];
			var inputErr=['err_b_name','err_addressess','err_email','err_desc','err_bt_id','err_meal','err_cuisines','err_food','err_city','err_country','err_state'];
			var inputErrTxt=['err_bname_txt','err_address_txt','err_desc_txt','err_btid_txt','err_meal_txt','err_cuisines_txt','err_food_txt','err_city_txt','err_country_txt','err_state_txt'];
			var inputErrMessage=["Please enter your business name.","Please enter your business address.","Please enter your business description.",'Please choose business type.',"Please choose food meal.","Please choose food cuisines.","Please choose food type.","Please choose business city.","Please Choose business Country.","Please Choose business State."];
			
			for(var i=0;i<(inputData.length);i++)
			{
				if($('#'+inputData[i]).val()=='')
				{
					
					$("."+inputErr[i]).show();
					$("."+inputErrTxt[i]).html(inputErrMessage[i]);
					result=0;
				}
				else
				{
					$("."+inputErrTxt[i]).html('');
					$("."+inputErr[i]).hide();
					result=1;
				}
			}
			if($('#state_id').val()=='')
			{
					$('.err_state').show();
					$('.err_state_txt').html('Please Choose business State.');
					result=0;
			}
			else
			{
					$('.err_state').hide();
					$('.err_state_txt').html('');
					result=1;
			}
			if($('#city_id').val()=='')
			{
					$('.err_city').show();
					$('.err_city_txt').html('Please Choose business City.');
					result=0;
			}
			else
			{
					$('.err_city').hide();
					$('.err_city_txt').html('');
					result=1;
			}
			if($('#b_weblink').val()!='')
				{
					var url_pattern= /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
					if(!url_pattern.test($('#b_weblink').val()))
					{
						result=0;
						$('.err_weblink').show();
						$('.err_weblink_txt').html('Please enter valid website link.');
					}
					else
					{
						
						$('.err_weblink').hide();
						$('.err_weblink_txt').html('');
					}
				}
				if($('#b_bt_id').val()==0)
				{
					if($('#b_other_name').val()=='')
					{
						result=0;
						$('.err_other').show();
						$('.err_other_txt').html('Please enter other business type.');
					}
					else
					{
						
						$('.err_other').hide();
						$('.err_other_txt').html('');
					}
				}
				if ($('#b_contact_person_mobile').val() != '') {
			        var a = $('#b_contact_person_mobile').val();
			         var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
			        if(a.match(filter)==null)
			        {
			           $('.err_c_mobile').show();
			            $('.err_c_mobile_txt').html('Phone Number must be in digit and have atleast 10 digits.');
			                result=0;
			        }
			        else
			        {
			            $('.err_c_mobile').hide();
			        }
			    }
			if(logo!='')
			{
				if (!logo.match(/(?:jpg|png|jpeg)$/)) {
				    // inputted file path is not an image of one of the above types
				    $('.err_logo').show();
				    result=0;
				}
				else
				{
					 $('.err_logo').hide();
				}
			}
			if(photo!='')
			{
				if (!photo.match(/(?:jpg|png|jpeg)$/)) {
				    // inputted file path is not an image of one of the above types
				    $('.err_image').show();
				    result=0;
				}
				else
				{
					 $('.err_image').hide();
				}
			}
			if(result==1)
			{
				$('#businessAddForm').submit();
			}
		}

		function getState()
		{
				var countryId=$('#country_id').val();

				var url="{{ URL::to('/getstate') }}"+'/'+countryId;

				 $.ajax({
	            		url : url ,
	            		 type:'post',
	            		headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
		         success:function(data){
		           $('#state_id').html(data);
		        },
		        error: function (data) {
                          console.log(data);
                      }
		    });	
		 }
		 function getCity()
		{
				var stateId=$('#state_id').val();

				var url="{{ URL::to('/getcity') }}"+'/'+stateId;

				 $.ajax({
	            		url : url ,
	            		 type:'post',
	            		headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
		         success:function(data){
		           $('#city_id').html(data);
		        },
		        error: function (data) {
                          console.log(data);
                      }
		    });	
		 }
	</script>
	
@endpush

