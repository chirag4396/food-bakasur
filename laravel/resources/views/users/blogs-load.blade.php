@php
$j=0 
@endphp
<div id="row"  style="position: relative;">
		@foreach($blogDatapublish as $b=>$bd)
				<div class="col-md-4 main-resto">
					<a href="#">
						<img class="img-fluid rounded mb-3 mb-md-0" src="{{ $bd->blog_cover_img }}" alt="">
					</a>
				</div>
				<div class="col-md-8 titleDiv">
					<h4>{{ $bd->blog_title }}</h4>
					<p>{{ strip_tags(str_limit($bd->blog_story,300)) }}
					</p>
					<a class="btn btn-blog" href="{{ URL::to('blog-detail/'.$bd->blog_id) }}">Read More →
					</a>
					<a class="btn btn-blog" href="{{ URL::to('/blog-update/'.$bd->blog_id) }}">Edit 
					</a>
				</div>
				


		@endforeach
	
</div>
<div id="pagination" style="float: right">{{ $blogDatapublish->render() }}</div>
