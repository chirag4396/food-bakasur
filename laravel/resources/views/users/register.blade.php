@extends('users.layouts.master')
@push('header')
	<style type="text/css">
		.err
		{
			display: none;
			color: red;
		}
	</style>
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endpush
@section('content')
<div class="main-top">
	<div class="container white" style="padding: 15px;">
		<h3 class="head">Registration</h3>
		<div class="border-line3"></div>
<!-- checkout-area start -->
		<div class="checkout-area">
		   	<div class="container">
		      	<div class="row">
		         	<form action="{{ URL::to('/user-register') }}" method="post" id="registerform" enctype="multipart/form-data">
		         		<input name="_token" type="hidden" value="{{ csrf_token() }}" />
		         		<input type="hidden" name="search_city" id="hiddenCity">
		         		<div class="col-lg-3"></div>
			            <div class="col-lg-6">
			               <div class="checkbox-form">
			                  <div class="row">
			                     <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Name <span class="required">*</span></label>
			                           <input type="text" placeholder="Name" name="name" id="name"/>
			                           <p class="err_name err"><span class="err_name_txt"></span></p>
			                        </div>
			                     </div>
			                    <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Email <span class="required">*</span></label>
			                           <input type="text" placeholder="Email" name="email" id="email"/>
			                           <p class="err_email err"><span class="err_email_txt"></span></p>
			                        </div>
			                     </div>
			                     <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Password <span class="required">*</span></label>
			                           <input type="password" placeholder="Password" name="password" id="password"/>
			                           <p class="err_pwd err"><span class="err_pwd_txt"></span></p>
			                        </div>
			                     </div>
			                     <div class="col-md-12">
			                        <div class="checkout-form-list">
			                           <label>Confirm Password <span class="required">*</span></label>                   
			                           <input type="password" placeholder="Confirm password" name="confirm_password" id="confirm_password" />
			                            <p class="err_c_pwd err"><span class="err_cpwd_txt"></span></p>
			                        </div>
			                       
			                     </div>
			                     <div class="col-md-12"><div class="checkout-form-list"><a href="{{ URL::to('user-login') }}">Sign In</a></div></div>
			                     <div class="col-md-12">
			                        <div class="order-button-payment">
			                           <input type="button" value="Register" onclick="formValidate()" />
			                        </div>
			                        <div class="col-md-12">
			                        </div>
			                     </div>
			                  </div>
			               </div>
			            </div>
			            <div class="col-lg-3"></div>
		         	</form>
		        </div>
		    </div>
		</div>
      <!-- checkout-area end -->  
   </div>
</div>
@endsection
@push('footer')
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript">
		$('#email').keyup(function() {
	      if ($('#email').val() != '') {
	        var email=$('#email').val();
	        if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
	        {
	           $('.err_email').show();
	                $('.err_email_txt').html('Please enter valid email address');
	                return false; 
	        }
	        else
	        {
	        	fd=[email];
	        	var url="{{ URL::to('/email-check') }}"
	        	$.ajax({
	            type:'POST',
	            url:url,
	            data:{'email':email},
	            headers: {
	                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
	            success:function(data){
	            	
			            if(data=='exists')
			            {
			            	
			            	 $('.err_email').show();
			            	 $('.err_email_txt').html('Email already exists, please enter another one.');
			            	
			            	 return false;
			            }else
			            {
			            	 $('.err_email').hide();
			            	  return true; 
			            }
		            }
		        });
			  
				           
	        }
	    } });
	    $('#confirm_password').keyup(function() {
	      if ($('#confirm_password').val() != '') {
	        var pwd=$('#password').val();
	        var c_pwd=$('#confirm_password').val();
	        if(pwd!=c_pwd)
	        {
	           $('.err_c_pwd').show();
	                $('.err_cpwd_txt').html('Password and Confirm Password should be same.');
	                return false; 
	        }
	        else
	        {
	            $('.err_c_pwd').hide();
	            return true; 
	        }
	    } });
		function formValidate()
		{
			var result=1;
			var pwd=$('#password').val();

	        var c_pwd=$('#confirm_password').val();
			var inputData=['name','email','password','confirm_password'];
			var inputErr=['err_name','err_email','err_pwd','err_c_pwd'];
			var inputErrTxt=['err_name_txt','err_email_txt','err_pwd_txt','err_cpwd_txt'];
			var inputErrMessage=["Please enter your name.","Please enter your email.","Please enter password.",'Please enter password again for confirmation.'];
			for(var i=0;i<inputData.length;i++)
			{
				if($('#'+inputData[i]).val()=='')
				{
					$("."+inputErr[i]).show();
					$("."+inputErrTxt[i]).html(inputErrMessage[i]);
					result=0;
					
				}
				else
				{
					
					if ($('#email').val() != '') {
				        var email=$('#email').val();
				        if(email.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)==null)
				        {
				           $('.err_email').show();
				           $('.err_email_txt').html('Please enter valid email address');
				               result=0;
				        }
				        else
				        {
				        	fd=[email];
				        	var url="{{ URL::to('/email-check') }}"
				        	$.ajax({
				            type:'POST',
				            url:url,
				            data:{'email':email},
  							async: false,
				            headers: {
				                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				            },
				            success:function(data){
				            	
						            if(data=='exists')
						            {
						            	
						            	 $('.err_email').show();
						            	 $('.err_email_txt').html('Email already exists, please enter another one.');
						            	 result=0;
						            	
						            }else
						            {
						            	 $('.err_email').hide();
						            }
					            }
					        });

				        }
				    }
			        if(pwd!=c_pwd && pwd!='')
			        {
			           $('.err_c_pwd').show();
			           $('.err_cpwd_txt').html('Password and Confirm Password should be same.');
			                result=0;
			        }
			        else
			        {
			            $('.err_c_pwd').hide();
			            
			        }
					$("."+inputErrTxt[i]).html('');
						$("."+inputErr[i]).hide();
				}
			}
			if(result==1)
			{
				$('#registerform').submit();
			}
		}
		 
	</script>
@endpush

