@extends('users.layouts.master')
@push('header')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style type="text/css">
   .thumb{
   margin: 10px 5px 10px 0;
   width: 560px;
   height: 200px;
   }
   .step{
       margin-bottom: 15px;
       width: 50%;
       height: 100px;
   }
   .err
   {
	   display: none;
	   color: red;
   }
   .form-control
   {
    font-size: 16px !important;
   }
</style>
@endpush
@section('content')
<section class="food-tabs">
   <div class="main-top">
      <div class="container white" style="padding: 15px;">
         <h3 class="head">{{ $mode }} Your Recipe</h3>
         <div class="border-line3"></div>
         <!-- checkout-area start -->
         <div class="checkout-area">
            <div class="container">
               <div class="row">
                @php
                        $url=($mode=='Update')?'/update-recipe':'/add-recipe';
                    @endphp
                  <form action="{{ URL::to($url) }}" method="post" enctype="multipart/form-data" id="recipeAdd">
                    @if($mode=='Update')
                            <input type="hidden" name="rec_id" value="{{ $recData['rec_id'] }}">
                        @endif
                     <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                     <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="checkbox-form">
                           <div class="row">
                              <div class="col-md-12 col-xs-12">
                                 <div class="checkout-form-list">
                                    <label>Recipe Name </label>
                                    <input type="text" placeholder="Recipe Name" name="rec_title" id="rec_title" value="{{ ($mode=='Update')? $recData['rec_title']:''  }}" />
                                    <p class="err_title err"><span class="err_title_txt"></span></p>
                                 </div>
                              </div>
                              <div class="col-md-12 col-xs-12">
                                 <div class="checkout-form-list">
                                    <label>Upload Cover Photo </label>
                                    <div id="thumb-output"></div>
                                    <div id="thumb-default">@if($mode=='Update' && $rec_image_status==1)<img src="{{ $recData['rec_img_path'] }}"  class="thumb">@else<img src="{{ asset('img/thumb-place2.png') }}"  class="thumb">@endif</div>
                                    <input type="file"  name="rec_img_path" class="rec_img_path" id="file-input"/>
                                    <p class="err_image err"><span class="err_image_txt">Only jpeg,png or jpg files are allowed.</span></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="col-md-12 col-xs-12">
                           <div class="checkout-form-list">
                              <label>Meal Type </label>
                              <select class="form-control" name="rec_meal_id" id="rec_meal_id">
                                 <option value="">--Select--</option>
                                 @foreach($meals as $m=>$mData)
                                 <option value="{{ $mData->meal_id }}" @if($mode=='Update'){{ ($recData['rec_meal_id']==$mData->meal_id)?'selected':'' }}@endif>{{ $mData->meal_title }}</option>
                                 @endforeach
                              </select>
                              <p class="err_meal err"><span class="err_meal_txt"></span></p>
                           </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                           <div class="checkout-form-list">
                              <label>Cuisine Type </label>
                              <select class="form-control" name="rec_cuisine_id" id="rec_cuisine_id">
                                 <option value="">--Select--</option>
                                 @foreach($cuisines as $c=>$cData)
                                 <option value="{{ $cData->cu_id }}" @if($mode=='Update'){{ ($recData['rec_cuisine_id']==$cData->cu_id)?'selected':'' }}@endif>{{ $cData->cu_title }}</option>
                                 @endforeach
                              </select>
                              <p class="err_cuisines err"><span class="err_cuisines_txt"></span></p>
                           </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                           <div class="checkout-form-list">
                              <label>Recipe Type </label>
                              <select class="form-control" name="rec_veg" id="rec_veg">
                                 <option value="">--Select--</option>
                                 <option value="1"  @if($mode=='Update'){{ ($recData['rec_veg']=="1")?'selected':'' }}@endif>Veg</option>
                                 <option value="0" @if($mode=='Update'){{ ($recData['rec_veg']=="0")?'selected':'' }}@endif>Non Veg</option>
                              </select>
                              <p class="err_cuisines err"><span class="err_cuisines_txt"></span></p>
                           </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                           <div class="checkout-form-list">
                              <label>Tip </label>
                              <textarea id="checkout-mess" rows="5" placeholder="Tip" name="rec_tip" style="width: 100%;">@if($mode=='Update') {{ $recData['rec_tip'] }} @endif</textarea>
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-12" style="border:1px solid gray;padding:10px;margin-top: 10px;">
                        <div class="col-lg-12 col-md-12 col-xs-12" >
                           <div class="checkbox-form">
                              <div class="row">
                                 <div class="col-md-12 col-xs-12">
                                    <div class="checkout-form-list">
                                       <label>Description</label>
                                       <textarea name="rec_description" placeholder="recipe in detail" style="width:100%" rows="4" id="rec_description">@if($mode=='Update') {{ $recData['rec_description'] }} @endif</textarea>
                                       <p class="err_rec_desc err"><span class="err_recdesc_txt"></span></p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                      <div class="col-xs-12 addsteps" style="border:1px solid gray;padding:10px;margin-top: 10px;">
                        <div class="col-lg-12 col-md-12 col-xs-12" >
                           <div class="checkbox-form">
                              <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="checkout-form-list">
                                       <input type="button" name="" class="btn btn-info" id="Add" value="Add Step">
                                       
                                           <input type="button" name="" class="btn btn-danger deletestep" id="Remove" value="Remove Step" style="display: none" >
                                       
                                    </div>
                                 </div>
                                </div>
                            </div>
                        </div>
                        @php  $i=1; @endphp
                      @if($mode=='Update')
                       
                        @foreach($recStep as $k=>$v)
                            
                            <div class="col-lg-6 col-md-6 col-xs-12 step{{ $i }}" >
                              <input type="hidden" name="rs_id[]" value="{{ $v->rs_id }}">
                               <div class="checkbox-form">
                                  <div class="row">
                                     <div class="col-md-12 col-xs-12">
                                        <div class="checkout-form-list">
                                           <label>Step {{ $i }}</label>
                                           <textarea id="checkout-mess" rows="2" placeholder="Step {{ $i }}" name="rs_title[]" style="width: 100%;">{{ $v->rs_title }}</textarea>
                                        </div>
                                     </div>
                                  </div>
                               </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 stepimage{{ $i }}">
                               <div class="checkbox-form">
                                  <div class="row">
                                     <div class="col-md-8 col-xs-12">
                                        <div class="checkout-form-list">
                                           <label>Step {{ $i }} Image</label>
                                           <div id="thumb-outputphoto{{ $i }}"></div>
                                           <div id="thumb-defaultphoto{{ $i }}"><img src="@if($v->rs_rec_image!='') {{ asset('uploads/recipe_steps/'.$v->rs_id.'/'.$v->rs_rec_image) }} @else {{ asset('img/thumb-place2.png') }} @endif"  class="step"></div>
                                           <input type="file"  name="rs_rec_image[]" class="file-input" id="file-input{{ $i }}"/>
                                        </div>
                                     </div>
                                     
                                       <div class="col-md-4 col-xs-12">
                                          <div class="checkout-form-list">
                                           
                                             <center><button type="button" name="" class="btn btn-danger" id="Remove" c onclick="removeRecipeSteps('{{ $v->rs_id }}','step{{ $i }}','stepimage{{ $i }}')"><i class="fa fa-close"></i></button></center>
                                          </div>
                                       </div>
                                    
                                  </div>
                               </div>
                            </div>
                            @php $i++ @endphp
                          
                        @endforeach
                     
                      
                       {{--  <div class="col-lg-6 col-md-6 col-xs-12">
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                        </div> --}}
                         
                      @endif
                    
                       
                        {{-- <div class="col-lg-6 col-md-6 col-xs-12 addsteps">
                        </div> --}}
                        {{-- <div class="col-lg-6 col-md-6 col-xs-12 addstepimage">
                        </div> --}}
                     </div>
                     <div class="col-xs-12 addind" style="border:1px solid gray;padding:10px;margin-top: 10px;">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="checkbox-form">
                                <div class="col-md-6 col-xs-12">
                                    <div class="checkout-form-list">
                                        <label></label>
                                            <input type="button" name="" class="btn btn-info" id="AddInd" value="Add Ingrediant" style="margin-top: 30px;width: 100%;">
                                                       
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="checkout-form-list">
                                              
                                        <input type="button" name="" class="btn btn-danger deleteing" id="RemoveInd" value="Remove Ingrediant" style="margin-top: 30px;margin-left: 10px;width: 100%;display: none">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        @php $j=1; @endphp
                        @if($mode=='Update' && count($recIng)>0)
                                @foreach($recIng as $ri=>$iv)
                                
                                    <div class="col-lg-12 col-md-12 col-xs-12 stepIng{{ $j }}">
                                      <input type="hidden" name="ri_id[]" value="{{ $iv->ri_id }}">
                                       <div class="checkbox-form">
                                            <div class="row">
                                                <div class="col-md-6 col-xs-12">
                                                    <div class="checkout-form-list">
                                                       <label>Ingrediant {{ $j }}</label>
                                                       <input type="text" id="checkout-mess" placeholder="Ingrediant" name="ri_title[]" value="{{ $iv->ri_title }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-xs-12">
                                                <div class="checkout-form-list">
                                                  
                                                   <button type="button" name="" class="btn btn-danger" id="RemoveInd" value="Remove Ingrediant" style="margin-top: 30px;margin-left: 10px;" onclick="removeRecipeIngredients('{{ $iv->ri_id }}','stepIng{{ $j }}')"><i class="fa fa-close"></i></button>
                                                </div>
                                             </div>
                                            </div>
                                       </div>
                                    </div>

                                  
                                     
                            @php $j++ @endphp
                           @endforeach     
                            
                        @endif
                        
                        
                     </div>
                     
                     <div class="clearfix"></div>
                     <div class="col-md-6 offset-md-3 col-xs-12">
                        <div class="order-button-payment">
                           <input type="button" value="Add Recipe" onclick="formValidation()" />
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <!-- checkout-area end -->  
      </div>
   </div>
</section>
@push('footer')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	
   $(document).ready(function(){
        
        if('{{ $mode }}'=='Update')
        {
           //
        }
       $('#file-input').on('change', function(){ //on file input change
           if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
           {
               $('#thumb-output').html(''); //clear html of output element
               var data = $(this)[0].files; //this file data
              
               $.each(data, function(index, file){ //loop though each file
                   if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                       var fRead = new FileReader(); //new filereader
                       fRead.onload = (function(file){ //trigger function on successful read
                       return function(e) {
                           var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
                           $('#thumb-default').hide(); 
                           $('#thumb-output').append(img); //append image to output element
                       };
                       })(file);
                       fRead.readAsDataURL(file); //URL representing the file's data.
                   }
               });
              
           }else{
           	$('#file-input').html('');
                //if File API is absent
           }
       });
      
       $(document).on("change", ".file-input", function(event) {
       	var id=this.id;
       	var lastChar = id[id.length -1];
        //$("#file-input"+count+"").on('change', function(){ //on file input change
           if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
           {
               $('#thumb-outputphoto'+lastChar).html(''); //clear html of output element
               var data = $(this)[0].files; //this file data
              
               $.each(data, function(index, file){ //loop though each file
                   if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                       var fRead = new FileReader(); //new filereader
                       fRead.onload = (function(file){ //trigger function on successful read
                       return function(e) {
                           var img = $('<img/>').addClass('step').attr('src', e.target.result); //create image element
                           $('#thumb-defaultphoto'+lastChar).hide(); 
                           $('#thumb-outputphoto'+lastChar).append(img); //append image to output element
                       };
                       })(file);
                       fRead.readAsDataURL(file); //URL representing the file's data.
                   }
               });
              
           }else{
           	$(this.id).html('');
                //if File API is absent
           }
       });
    });
   		var count=('{{ $mode }}'=='Update')?{{ $i }}:1;
   var indcount=('{{ $mode }}'=='Update')?{{ $j }}:1;
    $("#Add").on("click", function() {  
             $('.deletestep').show();
          
          $(".addsteps").append('<div class="col-lg-6 col-md-6 col-xs-12 step'+count+'" ><div class="checkbox-form step'+count+'"><div class="row"><div class="col-md-12 col-xs-12"><div class="checkout-form-list"><label>Step '+(count)+' </label><textarea id="checkout-mess" rows="2" placeholder="Step '+count+'" name="rs_title[]" style="width: 100%;"></textarea></div></div></div></div><div>');  
           $(".addsteps").append('<div class="col-lg-6 col-md-6 col-xs-12 stepimage'+count+'" ><div class="checkbox-form"><div class="row"><div class="col-md-8 col-xs-12"><div class="checkout-form-list"><label>Step '+(count)+' Image</label><div id="thumb-outputphoto'+count+'"></div><div id="thumb-defaultphoto'+count+'"><img src="{{ asset('img/thumb-place2.png') }}"  class="step"></div><input type="file"  name="rs_rec_image[]" class="file-input" id="file-input'+count+'"/></div></div><div class="col-md-4 col-xs-12"><div class="checkout-form-list"></div></div></div></div></div></div>');  
           count++;
      }); 
      $("#Remove").on("click", function() { 
        count--;
        if(count=={{ $i }})
        {
          $('.deletestep').hide();  
        }
        $('.step'+count).hide();
        $('.stepimage'+count).hide();
        // $(".addsteps").children().last().remove();  
         // $(".addstepimage").children().last().remove();  
       
      }); 
        /*$("body").on("click", ".Remove", function () {
        $(this).closest("div.div.step").remove();
    });*/
     $("#AddInd").on("click", function() {  
        $('.deleteing').show();  
     	 $(".addind").append('<div class=" col-md-12 col-xs-12 ind'+indcount+'" ><div class="checkbox-form"><div class="row"><div class="col-md-6 col-xs-12"><div class="checkout-form-list"><label>Ingrediant '+indcount+' </label><input type="text" id="checkout-mess" placeholder="Ingrediant" name="ri_title[]"></div></div><div class="col-md-3 col-xs-12"><div class="checkout-form-list"></div></div></div></div></div>'); 
         indcount++
     });
      $("#RemoveInd").on("click", function() {  
      indcount--;
        if(indcount=={{ $j }})
        {
          $('.deleteing').hide();  
        }
          $(".addind").children().last().remove();  
        
      }); 
        // $("body").on("click", ".removeing", function () {
        //     $(this).closest("div.checkbox-form").remove();
        //     indcount--;
        // });
      function formValidation()
      	{
            result=1;
      		if('{{ $mode }}'=='Update')
            {
                    var inputData=['rec_title','rec_meal_id','rec_cuisine_id','rec_veg','rec_description'];
                var inputErr=['err_title','err_meal','err_cuisines','err_food','err_rec_desc'];
                var inputErrTxt=['err_title_txt','err_meal_txt','err_cuisines_txt','err_food_txt','err_recdesc_txt'];
                var inputErrMessage=["Please enter recipe name.","Please choose food meal.","Please choose food cuisines.","Please choose food type.","Please enter description"];
            }
            else
            {
                var inputData=['rec_title','rec_meal_id','rec_cuisine_id','rec_veg','rec_description','file-input'];
                var inputErr=['err_title','err_meal','err_cuisines','err_food','err_rec_desc','err_image'];
                var inputErrTxt=['err_title_txt','err_meal_txt','err_cuisines_txt','err_food_txt','err_recdesc_txt','err_image_txt'];
                var inputErrMessage=["Please enter recipe name.","Please choose food meal.","Please choose food cuisines.","Please choose food type.","Please enter description","Please choose image."];
            }
			for(var i=0;i<inputData.length;i++)
			{
				if($('#'+inputData[i]).val()=='')
				{
					$("."+inputErr[i]).show();
					$("."+inputErrTxt[i]).html(inputErrMessage[i]);
					result=0;
				}
				else
				{
					$("."+inputErr[i]).hide();
					$("."+inputErrTxt[i]).html('');
				}
				
			}
			if($('#file-input').val()!='')
			{
				if (!$('#file-input').val().match(/(?:jpg|png|jpeg)$/)) {
				    // inputted file path is not an image of one of the above types
				    $('.err_image').show();
				    result=0;
				}
				else
				{
					 $('.err_image').hide();
				}
			}
			if(result==1)
			{
				$('#recipeAdd').submit();
			}
			else
			{
				return false;
			}
      	}
      	$(window).on('load', function(){
		    $('#file-input').val('');
		});
		$('#file-input').on('change',function(){
			if($('#file-input').val()!='')
			{
				if (!$('#file-input').val().match(/(?:jpg|png|jpeg)$/)) {
				    // inputted file path is not an image of one of the above types
				    $('.err_image').show();
				    
				}
				else
				{
					 $('.err_image').hide();
				}
			}
		});
        function removeRecipeSteps(id,step,stepImage)
        {
            var id=id;
            var ajaxURL="{{ URL::to('/recipestep/delete') }}";
             $.ajax({
                url:ajaxURL,
                type:'post',
                data:{id:id},
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    $('.'+step).remove();
                    $('.'+stepImage).remove();
                }
             });
        }
        function removeRecipeIngredients(id,step)
        {
            var id=id;
            var ajaxURL="{{ URL::to('/recipeingrediant/delete') }}";
             $.ajax({
                url:ajaxURL,
                type:'post',
                data:{id:id},
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    $('.'+step).remove();
                }
             });
        }
</script>
@endpush
@endsection