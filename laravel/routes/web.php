<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@loggedin');
Route::get('blad',function(){
	return view('users.template.businessenquirytemplate');
});
Route::get('user-logout','Auth\LoginController@userLogout');
Route::get('/home', function () {
    return view('backend.dashboard');
});
Route::post('/postfollow','UserController@follow');
Route::post('/user-businessEnquiry','BusinessEnquiryController@postEnquiry');

/* --- Add Business --- */

	Route::get('/business-add', 'BusinessController@businessAdd');
	Route::post('/add-business','BusinessController@businessAdd');
	Route::get('/business-update/{id}','BusinessController@updateBusiness');
	Route::post('/update-business','BusinessController@editBusiness');

	/* User Profile Display*/
	Route::get('/user-profile','UserController@userDisplayProfile');
	Route::post('/update-userprofile','UserController@userChangeProfile');

	Route::get('/blog-create','BlogController@createBlog');
	Route::post('/create-blog','BlogController@createBlog');
	Route::get('/blog-update/{id}','BlogController@updateBlog');
	Route::post('/update-blog','BlogController@editBlog');

	Route::get('/add-recipe','RecipeController@create');
	Route::post('/add-recipe','RecipeController@create');
	Route::get('/recipe-update/{id}','RecipeController@updateRecipe');
	Route::post('/update-recipe','RecipeController@editRecipe');
	Route::post('/recipestep/delete','RecipeStepController@deleteRecipeStep');
	Route::post('/recipeingrediant/delete','RecipeController@deleteRecipeIngrediant');





Route::get('/business-detail/{id}','BusinessController@businessDetails');
Route::post('/post-businessComment','BusinessCommentController@postComment');

/* --- User Registration --- */
Route::get('/userregister', function () {
    return view('users.register');
});
Route::post('/user-register', 'Auth\RegisterController@userRegister');
Route::post('/email-check','Auth\RegisterController@emailAlreadyExists');
Route::get('/verify-account/{id}/{otp}','Auth\RegisterController@verifyAccount');


/* User Login */
Route::get('/user-login','Auth\LoginController@userLogin');
Route::post('/user-login','Auth\LoginController@userLogin');




/* --- Reset password  Link ---*/
Route::get('/forgot-password','Auth\ForgotPasswordController@passwordRecovery');
Route::post('/password-recovery','Auth\ForgotPasswordController@passwordRecovery');
Route::get('/reset-password/{id}/{otp}','Auth\ForgotPasswordController@resetPassword');
Route::post('/reset-password','Auth\ForgotPasswordController@passwordReset');
Route::get('/send-otp','Auth\ForgotPasswordController@sendOTP');
Route::get('/otp-verify','Auth\ForgotPasswordController@verifyOTP');
Route::post('/otp-verify','Auth\ForgotPasswordController@verifyOTP');


/* --- Blog Create ---*/

Route::get('/blog-detail/{id}','BlogController@blogDetails');
Route::post('/post-blogComment','BlogCommentController@postComment');


/*--- Recipe---*/

Route::get('/recipe-detail/{id}','RecipeController@recipeDetails');
 Route::post('/post-recipeComment','RecipeCommentController@postComment');

Route::get('/listing', function () {
    return view('users.listing');
})->name('listing');

Route::get('/foodbakasur-listing','ListingController@list');

Route::post('/search_result','ListingController@searchResult');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/sitemap','BlogController@sitemap');

//Route::get('/', 'BlogController@index')->name('home');

Route::get('/blogs/{type}/{id?}/{name?}', 'BlogController@getBlogs')->name('blogs');

Route::post('/GetSearch', 'BlogController@getSearch');

Route::resource('/blog', 'BlogController');

Route::post('/UploadImage/{id?}', 'BlogController@uploadImage');

Route::post('/DeleteImage', 'BlogController@deleteImage');

Route::post('/getstate/{id}','BusinessController@ajaxState');
Route::post('/getcity/{id}','BusinessController@ajaxcity');

/*-- Backend Routes--*/

//user routes

Route::get('/backend/users','UserController@index');
Route::get('/backend/user-details/{id}','UserController@userDetails');
Route::get('/backend/user-blog/{id}','UserController@userBlogDetails');
Route::get('/backend/user-recipe/{id}','UserController@userRecipeDetails');
Route::get('/backend/user-business/{id}','UserController@userBusinessDetails');

Route::get('/backend/blogs','BlogController@index');
Route::post('/backend/ajax-blog-data','BlogController@ajaxBlogData');
Route::post('/backend/change-blog-status','BlogController@approveOrNot');

Route::get('/backend/recipes','RecipeController@index');
Route::post('/backend/ajax-recipe-data','RecipeController@ajaxRecipeData');
Route::post('/backend/change-recipe-status','RecipeController@approveOrNot');

Route::get('/backend/business','BusinessController@index');

Route::get('/backend/blog-comments','BlogCommentController@index');
Route::post('/backend/ajax-blogcomments-data','BlogCommentController@ajaxBlogCommentData');
Route::post('/backend/change-blogcomments-status','BlogCommentController@approveOrNot');

Route::get('/backend/recipe-comments','RecipeCommentController@index');
Route::post('/backend/ajax-recipecomments-data','RecipeCommentController@ajaxRecipeCommentData');
Route::post('/backend/change-recipecomments-status','RecipeCommentController@approveOrNot');

Route::get('/backend/business-comments','BusinessCommentController@index');
Route::post('/backend/ajax-businesscomments-data','BusinessCommentController@ajaxBusinessCommentData');
Route::post('/backend/change-businesscomments-status','BusinessCommentController@approveOrNot');

