<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
	protected $primaryKey = 'ud_id';

	protected $fillable = ['ud_user', 'ud_gender', 'ud_birthday', 'ud_hometown', 'ud_profile_img', 'ud_followings', 'ud_followers', 'ud_about','ud_mobile'];

	public $timestamps = false;
    
    public function user()
    {
    	return $this->belongsTo(\App\User::class, 'ud_user');
    }

    public function city()
    {
    	return $this->belongsTo(\App\City::class, 'ud_hometown');
    }
}
