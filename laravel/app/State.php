<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{    
    protected $primaryKey = 'state_id';

    protected $fillable = ['state_name', 'state_country_id'];

    public $timestamps = false;

    public function city()
    {
    	return $this->hasMany(\App\City::class, 'city_state_id');
    }

    public function country()
    {
    	return $this->belongsTo(\App\Country::class, 'state_country_id');
    }
}
