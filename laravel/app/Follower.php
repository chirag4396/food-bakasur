<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
	protected $primaryKey = 'f_id';

    protected $fillable = ['f_uo_id', 'f_us_id','f_follow_status','f_following_status'];

    public $timestamps = false;
}