<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeIngredient extends Model
{
	protected $primaryKey = 'ri_id';

	protected $fillable = ['ri_title', 'ri_rec_id'];

	public $timestamps = false;

	public function recipe()
	{
		return $this->belongsTo(\App\Recipe::class, 'rs_rec_id');
	}
}
