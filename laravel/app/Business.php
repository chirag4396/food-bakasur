<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $primaryKey = 'b_id';

	protected $fillable = ['b_name','b_bt_id','b_logo','b_contact_person_name','b_contact_person_mobile','b_desc','b_address','b_weblink','b_photo','b_payment_status','b_other_name','b_user_id','b_meal_id','b_cuisine_id','b_food_type','b_city_id'];
	
	CONST CREATED_AT = 'b_created_at'; 

	CONST UPDATED_AT = 'b_updated_at';
}
