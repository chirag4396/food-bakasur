<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'visible_password', 'type', 'status','otp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function recipes()
    {
        return $this->hasMany(\App\Recipe::class, 'rec_user');
    }
    
    public function blogs()
    {
        return $this->hasMany(\App\Blog::class, 'blog_user_id');
    }
    
    public function detail()
    {
        return $this->hasOne(\App\UserDetail::class, 'ud_user');
    }
}
