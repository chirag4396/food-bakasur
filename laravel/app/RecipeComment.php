<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeComment extends Model
{
    protected $primaryKey = 'rc_id';

    protected $fillable = ['rc_cuser_id', 'rc_content','rc_rec_id','rc_admin_status'];

    CONST CREATED_AT = 'rc_created_at'; 

    CONST UPDATED_AT = 'rc_updated_at';

    public function recipe_details()
    {
    	return $this->belongsTo('\App\Recipe');
    }

   
}
