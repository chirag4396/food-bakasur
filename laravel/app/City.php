<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $primaryKey = 'city_id';

    protected $fillable = ['city_name', 'city_state_id'];

    public $timestamps = false;

    public function details()
    {
    	return $this->hasMany(\App\UserDetail::class, 'ud_hometown');
    }

    public function state()
    {
    	return $this->belongsTo(\App\State::class, 'city_state_id');
    }

    public function localities()
    {
    	return $this->hasMany(\App\Locality::class, 'loc_city_id');
    }
}
