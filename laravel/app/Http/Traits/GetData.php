<?php
namespace App\Http\Traits;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Auth;
use App\Events\SendMail;
use App\Models\User;
use App\Models\Blog;
use Illuminate\Http\Request;
use Mail;
use File;

trait GetData{
	protected $clientEmail = 'ch.chhuchha@gmail.com';

	protected $clientUser = 'Chirag Chhuchha';
	
	public function resizeImage($image, $quality = 35){    
		$sourceImage = $image;
		$targetImage = $image;

		list($maxWidth, $maxHeight, $type, $attr) = getimagesize($image);
		
		if (!$image = @imagecreatefromjpeg($sourceImage)){
			return false;
		}

		list($origWidth, $origHeight) = getimagesize($sourceImage);

		if ($maxWidth == 0){
			$maxWidth  = $origWidth;
		}

		if ($maxHeight == 0){
			$maxHeight = $origHeight;
		}

		$widthRatio = $maxWidth / $origWidth;
		$heightRatio = $maxHeight / $origHeight;

		$ratio = min($widthRatio, $heightRatio);

		$newWidth  = (int)$origWidth  * $ratio;
		$newHeight = (int)$origHeight * $ratio;

		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
		imageinterlace($image, 1);
		imagejpeg($newImage, $targetImage, $quality);

		imagedestroy($image);
		imagedestroy($newImage);
	}	

	public function adminMail($subject,$message){
		
		$adminData = (object)[];
		$adminData->name = $this->clientUser;
		$adminData->email = $this->clientEmail;

		$adminData->file = 'notification';
		$adminData->subject = 'Troviday - '.$subject;
		$adminData->data = (object)[]; 

		$adminData->data->message = $message;
		
		Event::fire(new SendMail($adminData));
	}

	public function userMail($name, $email, $subject, $message){	

		$userData = (object)[];
		$userData->name = $name;
		$userData->email = $email;

		$userData->file = 'notification';
		$userData->subject = 'Troviday - '.$subject;

		$userData->data = (object)[]; 

		$userData->data->message = $message;
		
		Event::fire(new SendMail($userData)); 
	}

	public function deleteFiles($path){
		if (is_dir($path) === true) {
			$files = array_diff(scandir($path), array('.', '..'));			

			foreach ($files as $file) {
				$this->deleteFiles(realpath($path) . '/' . $file);
			}

			return rmdir($path);
		} else if (is_file($path) === true) {
			return unlink($path);
		}
		return false;
	}

	public function createBlog($title = null){
		$file = 'blogs/story-'.(Blog::max('blog_id')+1).'.txt';
		$my = fopen($file , "w");		
		fclose($my);
		// $file = storage_path('blogs/'.(Blog::max('blog_id')+1).'.txt');
		// File::get($file);
		// fopen($file , "w");
		// fclose($file);
		return Blog::create([
			'blog_title' => $title,
			'blog_story' => $file,
			'blog_city_id' => null,
			'blog_cat_id' => null,
			'blog_user_id' => Auth::user()->id,
			'blog_minutes' => null,              
			]);	
	}

	public function getImagePath($subfolder,$filename,$folder,$parent_folder='uploads')
   {
        $base_path=$_SERVER['DOCUMENT_ROOT'];
        $base_url=$_SERVER['HTTP_HOST'];
        $url=$base_path.'/'.$parent_folder.'/'.$folder.'/'.$subfolder.'/'.$filename;
        //echo $url;exit;
        if(file_exists($url) && $filename!='')
        {
            $file_path='http://'.$base_url.'/'.$parent_folder.'/'.$folder.'/'.$subfolder.'/'.$filename;
        }
        else
        {
            $file_path='http://'.$base_url.'/'.$parent_folder.'/'.$folder.'/no_image.png';
        }
        return $file_path;
   }

   public function fileUpload($folder,$folderId,$image)
   {
		$base_path=$_SERVER['DOCUMENT_ROOT'].'/uploads/'.$folder.'/'.$folderId;
        $result = File::makeDirectory($base_path, 0777, true, true);
        $name = date('YmdHis').'_'.$image;
		$file_name = preg_replace("/[^a-zA-Z0-9.]/", "_", $name);
        $filepath = $base_path.'/'.$file_name;
        return $filepath .'|'. $file_name;
   }

   public function deleteImage($image_path)
   {
		if(File::exists($image_path)) {
		    //File::delete($image_path);
		    unlink($image_path);
		    return 1;
		}
		return 0;
   }

    public function send_mail($data,$emailtemplate,$subject)
   {
     // $attachment=$attachment;

        Mail::send($emailtemplate, $data, function($message) use ($data,$subject){
        	//echo $subject;exit;
            /* $message->to('shyam.sungare@gmail.com', 'Shyam Magar')->subject
             ($subject);
              $message->to('sungaretechnologies@gmail.com', 'Sungare Technologies')->subject
             ($subject);*/
            $message->from('kinjal.sungare@gmail.com', 'Kinjal Prajapati');
            $message->to($data['email'],$data['name'])->subject($subject);;
            

          });
        return;
      
   	}
   	 public function sendSMS($mobile,$msg){

        $url = 'http://trans.smsfresh.co/api/sendmsg.php';

        $fields = array(
            'user' => 'magarsham',
            'pass' => 'festivito5555',
            'sender' => 'FESTVT' ,
            'phone' => $mobile,
            'text' => $msg,
            'priority' => 'ndnd',
            'stype' => 'normal'       
        );


        $ch = curl_init();


        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));


        $res = curl_exec($ch);


        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_close($ch);

        return $res;
    }
    function dateTimeFormatWithYear($datetime){
        if ($datetime != '0000-00-00 00:00:00') {
           $date = date_create_from_format("Y-m-d",date('Y-m-d',strtotime($datetime)));
            $time = date_create_from_format("H:i:s",date('H:i:s',strtotime($datetime)));
            return date_format($date,"j M Y").' at '.date_format($time, 'G:i A');
        }
        else
        {
            return '';
        }
    }

    public function removeImage($subfolder,$folder,$filename)
    {
        $image_path=$_SERVER['DOCUMENT_ROOT'].'/uploads/'.$folder.'/'.$subfolder.'/'.$filename;
        return $this->deleteImage($image_path);
    }

    function number_format_short( $n, $precision = 1 ) {
	if ($n < 900) {
		// 0 - 900
		$n_format = number_format($n, $precision);
		$suffix = '';
	} else if ($n < 900000) {
		// 0.9k-850k
		$n_format = number_format($n / 1000, $precision);
		$suffix = 'K';
	} else if ($n < 900000000) {
		// 0.9m-850m
		$n_format = number_format($n / 1000000, $precision);
		$suffix = 'M';
	} else if ($n < 900000000000) {
		// 0.9b-850b
		$n_format = number_format($n / 1000000000, $precision);
		$suffix = 'B';
	} else {
		// 0.9t+
		$n_format = number_format($n / 1000000000000, $precision);
		$suffix = 'T';
	}
  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
  // Intentionally does not affect partials, eg "1.50" -> "1.50"
	if ( $precision > 0 ) {
		$dotzero = '.' . str_repeat( '0', $precision );
		$n_format = str_replace( $dotzero, '', $n_format );
	}
	return $n_format .''. $suffix;
}

}