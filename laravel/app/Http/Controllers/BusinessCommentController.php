<?php 

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\BusinessComment;
use App\Business;
use Yajra\Datatables\Facades\Datatables;
use App\User;
use App\UserDetail;
use App\Http\Traits\GetData;

class BusinessCommentController extends Controller
{
    use GetData;
	public function index(Request $request)
	{
		$all_commments=BusinessComment::orderBy('busc_id','desc')->paginate(10);

		foreach ($all_commments as $key => $value) {
            
			$business=Business::find($value->busc_b_id)->b_name;
            
             $all_commments[$key]->rec='';
             if(isset($business))
			     $all_commments[$key]->rec=$business;
			
		}
        return view('backend.list_business-comments')->with(['all_commments'=>$all_commments]);	
	}

	 function ajaxBusinessCommentData()
    {
        $recipecomments=BusinessComment::orderBy('busc_id','desc')->paginate(10);
        $str='<tbody class="table_data">';
        foreach ($recipecomments as $key => $value) {
        	$bus_details=Business::find($value->busc_b_id)->b_name;
            $bus_title='';
             if(isset($bus_details))
                $bus_title=$bus_details;
            $url='/backend/user-business/'.$value->busc_b_id;
           	$status=($value->busc_admin_status==1)?'Approved':'Disapproved';
            $valueforbutton=($value->busc_admin_status==1)?"Disapprove":"Approve";
            $classforbutton=($value->busc_admin_status==1)?"btn-danger":"btn-info";
            $textcolor=($value->busc_admin_status==0)?"color:red":"";
           $str.='<tr><td>'.$value->busc_content.'</td><td>'.$bus_title.'</td><td><center><a href='.$url.'><button class="btn btn-warning">View</button></a><input class="btn '.$classforbutton.'" type="button" name="" value='.$valueforbutton.' style="margin-left: 20px;" onclick="changeStatus('.$value->busc_id.')"></center></td></tr>';
        }
        $str.='</tbody>';
        echo $str;exit;
        return (datatables()->of($str)->toJson());
    }

    function approveOrNot(Request $request)
    {
        if($request->id)
        {

            $recipeData=BusinessComment::find($request->id);
          
            $updateArr=array();
            if($recipeData->busc_admin_status==1)
            {
                $updateArr['busc_admin_status']=0;
            }
            else
            {
                $updateArr['busc_admin_status']=1;   
            }
            BusinessComment::find($request->id)->fill($updateArr)->save();
            echo "Status Chnaged successfully";exit;
        }
       
    }

     function postComment(Request $request)
    {
        //print_r($request->all());exit;
        if($request->all())
        {
            if($request['busc_cuser_id']=='')
            {
                echo '0';exit;
            }
            else
            {
                $updateArr=$request->all();
                $updateArr['busc_admin_status']=0;
                   
                $id=BusinessComment::create($updateArr)->busc_id;
                
                if(!is_null($id))
                {
                    $businessComment=BusinessComment::where('busc_b_id',$request['busc_b_id'])->where('busc_admin_status',1)->orderBy('busc_created_at','desc')->get();
                    
                    foreach ($businessComment as $key => $value) {
                        $businessComment[$key]->comment_user=User::find($value->busc_cuser_id)->name;
                        $comment_user_pic=UserDetail::where('ud_user',$value->busc_cuser_id)->first();
                        if(isset($comment_user_pic))
                        {
                            $businessComment[$key]->cu_image=$this->getImagePath($value->busc_cuser_id,$comment_user_pic->ud_profile_img,'users');
                        }
                        else
                        {
                            $businessComment[$key]->cu_image=$this->getImagePath($value->busc_cuser_id,'','users');
                        }
                    }
                    
                   return view('users.business-comments', ['businessComment' => $businessComment])->render();  
                }
            }
        }
    }
}