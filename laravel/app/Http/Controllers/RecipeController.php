<?php

namespace App\Http\Controllers;

use App\Recipe;
use App\RecipeStep;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Meal;
use App\Cuisine;
use App\RecipeIngredient;
use App\User;
use App\UserDetail;
use App\RecipeComment;

class RecipeController extends Controller
{
    use GetData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recs = Recipe::orderBy('rec_id','desc')->get();
        foreach ($recs as $key => $value) {
            $recs[$key]->meal=Meal::find($value->rec_meal_id)->meal_title;
            $recs[$key]->cuisines=Cuisine::find($value->rec_cuisine_id)->cu_title;
        }
        $data=array('recData'=>$recs);
        return view('backend.list_recipe')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->session()->get('sessionData.id')=='')
        {
            return redirect('/');
        }
        if($request->all())
        {
            $rec_data=$request->all();
             $rec_data['rec_user']=3;
             $rec_id=Recipe::create($rec_data)->rec_id;
             if(!is_null($rec_id))
             {
                if($request->rec_img_path)
                {
                    $update_arr=array();
                    if ($request->hasFile('rec_img_path')) {
                        
                        $path=$this->fileUpload("recipe",$rec_id,$request->file('rec_img_path')->getClientOriginalName());
                       
                        $split=explode("|", $path);
                        $img_path=chop($split[0],$split[1]);
                        $res =  $request->file('rec_img_path')->move($img_path, $split[1]);
                        $request->rec_img_path = $split[1];
                        $update_arr['rec_img_path'] = $request->rec_img_path;
                        
                    }
                    Recipe::find($rec_id)->fill($update_arr)->save();

                }
                if(count($request->all('ri_title'))>0)
                {
                    $j=0;
                    foreach($request->all()['ri_title'] as $i => $it) {
                        $rec_ingre['ri_rec_id']=$rec_id;
                        $rec_ingre['ri_title']=$it;
                        $ri_id=RecipeIngredient::create($rec_ingre)->ri_id;
                    }
                }
                 if(count($request->all()['rs_title'])>0)
                 {
                    $i=0;
                    foreach($request->all()['rs_title'] as $key => $value) {
                        $rec_step['rs_rec_id']=$rec_id;
                        $rec_step['rs_title']=$value;
                        $rs_id=RecipeStep::create($rec_step)->rs_id;
                        if(isset($request->all()['rs_rec_image']))
                        {
                            foreach ($request->all()['rs_rec_image'] as $k => $v) {
                                if($key==$k)
                                {
                                    if($v!='')
                                    {
                                       $updateimage_arr=array();
                                       
                                        if ($v!='') {
                                            
                                            $path=$this->fileUpload("recipe_steps",$rs_id,$v->getClientOriginalName());
                                           
                                            $split=explode("|", $path);
                                            $img_path=chop($split[0],$split[1]);
                                            $res =  $v->move($img_path, $split[1]);
                                            $filename = $split[1];
                                            $updateimage_arr['rs_rec_image'] = $filename;
                                            
                                        }
                                        RecipeStep::find($rs_id)->fill($updateimage_arr)->save(); 
                                    }
                                }
                            }
                        }
                        
                      
                    }
                 }
                 $request->session()->flash('message','Recipe created successfully, and wait for approval to publish.');
                 return redirect('/');
             }
            
        }
        $mealData=Meal::all();
        $cuisinesData=Cuisine::all();
        $data=array('meals'=>$mealData,'cuisines'=>$cuisinesData,'mode'=>'Add');
        return view('users.recipe-create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipe $recipe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe)
    {
        //
    }

     function ajaxRecipeData()
    {
        $recs=Recipe::orderBy('rec_id','desc')->get();
        $str='<tbody class="table_data">';

        foreach ($recs as $key => $value) {
            $url='/backend/user-rec/'.$value->rec_id;
           $meal=Meal::find($value->rec_meal_id)->meal_title;
            $cuisines=Cuisine::find($value->rec_cuisine_id)->cu_title;
            $status=($value->rec_admin_status==1)?'Approved':'Disapproved';
            $vegnonveg=($value->rec_veg==1)?'Veg':'Non Veg';
            $valueforbutton=($value->rec_admin_status==1)?"Disapprove":"Approve";
            $classforbutton=($value->rec_admin_status==1)?"btn-danger":"btn-info";
            $textcolor=($value->rec_admin_status==0)?"color:red":"";
           $str.='<tr><td>'.$value->rec_title.'</td><td>'.$meal.'</td><td>'.$cuisines.'</td><td>'.$vegnonveg.'</td><td>'.$status.'</td><td><center><a href='.$url.'><button class="but btn-info">View</button></a><input class="btn '.$classforbutton.'" type="button" name="" value='.$valueforbutton.' style="margin-left: 20px;" onclick="changeStatus('.$value->rec_id.')"></center></td></tr>';
        }
        $str.='</tbody>';
        return datatables()->of($str)->toJson();
    }

    function approveOrNot(Request $request)
    {
        if($request->id)
        {

            $recData=Recipe::find($request->id);
           
            $updateArr=array();
            if($recData->rec_admin_status==1)
            {
                $updateArr['rec_admin_status']=0;
            }
            else
            {
                $updateArr['rec_admin_status']=1;   
            }
            Recipe::find($request->id)->fill($updateArr)->save();
            echo "Status Chnaged successfully";exit;
        }
       
    }

    function updateRecipe($id,Request $request)
    {
        if($request->session()->get('sessionData.id')=='')
        {
            return redirect('/');
        }
        $recData=Recipe::find($id);
        $recStep=RecipeStep::where('rs_rec_id',$id)->get();
        $recIng=RecipeIngredient::where('ri_rec_id',$id)->get();
        $rec_img_path=$this->getImagePath($id,$recData['rec_img_path'],'recipe');

          $imageArr=explode("/", $rec_img_path);
            $lastElement=end($imageArr);
            
            if($lastElement=='no_image.png')
            {
                $rec_image_status=0;
            }
            else
            {
                $rec_image_status=1;
                $recData['rec_img_path']=$rec_img_path;
            }
        foreach ($recStep as $key => $value) {
           //$recStep[$key]->rs_rec_image=$this->getImagePath($value->rs_id,$value->rs_rec_image,'recipe_steps');
        }
        
        $mealData=Meal::all();
        $cuisinesData=Cuisine::all();
        $data=array('meals'=>$mealData,'cuisines'=>$cuisinesData,'recData'=>$recData,'mode'=>'Update','recStep'=>$recStep,'rec_image_status'=>$rec_image_status,'recIng'=>$recIng);
        return view('users.recipe-create')->with($data);
    }

    function editRecipe(Request $request)
    {
      if($request->session()->get('sessionData.id')=='')
        {
            return redirect('/');
        }
        $rec_id=$request->rec_id;
        $coverpic=Recipe::find($rec_id)->rec_img_path;
        $updateRecipe=$request->all();
        $updateRecipe['rec_admin_status']=0;
        Recipe::find($rec_id)->fill($updateRecipe)->save();
        
        if(count($request->all()['rs_title'])>0)
         {
            $i=0;
            
            foreach($request->all()['rs_title'] as $key => $value) {
               
                if(isset($request->all()['rs_id'][$key]))
                {
                   foreach ($request->all()['rs_id'] as $rs => $ids) {
                   
                        if($key==$rs)
                            {
                                $rs_id=RecipeStep::find($ids);
                               
                                   if(isset($rs_id))
                                    {
                                        $updateArr=array();
                                        $updateArr['rs_title']=$value;
                                       // print_r($updateArr);
                                        if(isset($request->all()['rs_rec_image'][$key]))
                                        {
                                            $v=$request->all()['rs_rec_image'][$key];
                                            $path=$this->fileUpload("recipe_steps",$ids,$v->getClientOriginalName());
                                           
                                            $split=explode("|", $path);
                                            $img_path=chop($split[0],$split[1]);
                                            $res =  $v->move($img_path, $split[1]);
                                            $filename = $split[1];
                                            $updateArr['rs_rec_image'] = $filename;
                                        }
                                       
                                        RecipeStep::find($ids)->fill($updateArr)->save();
                                    }
                                    else
                                    {
                                        $stepArr=array();
                                        $updateArr=array();
                                        $stepArr=array('rs_rec_id'=>$request->rec_id,'rs_title'=>$value);
                                        $id=RecipeStep::create($stepArr)->rs_id;  
                                        if(isset($request->all()['rs_rec_image'][$key]))
                                        {
                                            $v=$request->all()['rs_rec_image'][$key];
                                            $path=$this->fileUpload("recipe_steps",$id,$v->getClientOriginalName());
                                            //echo $path;exit;
                                            $split=explode("|", $path);
                                            $img_path=chop($split[0],$split[1]);
                                            $res =  $v->move($img_path, $split[1]);
                                            $filename = $split[1];
                                            $updateArr['rs_rec_image'] = $filename;
                                             RecipeStep::find($id)->fill($updateArr)->save();
                                        }
                                       
                                    }
                            }
                        } 
                }
               else{
                 $stepArr=array();
                    $stepArr=array('rs_rec_id'=>$request->rec_id,'rs_title'=>$value);
                    $updateArr=array();
                   
                    $id=RecipeStep::create($stepArr)->rs_id;
                    if(isset($request->all()['rs_rec_image'][$key]))
                        {
                            $v=$request->all()['rs_rec_image'][$key];
                            $path=$this->fileUpload("recipe_steps",$id,$v->getClientOriginalName());
                          // echo $path;exit;
                            $split=explode("|", $path);
                            $img_path=chop($split[0],$split[1]);
                            $res =  $v->move($img_path, $split[1]);
                            $filename = $split[1];
                            $updateArr['rs_rec_image'] = $filename;
                            RecipeStep::find($id)->fill($updateArr)->save();
                        }
                                    
                }
               

              
                
              
            }
         }
          foreach($request->all()['ri_title'] as $key => $value) {
               
                if(isset($request->all()['ri_id'][$key]))
                {
                   foreach ($request->all()['ri_id'] as $rs => $ids) {
                   
                        if($key==$rs)
                            {
                               // echo "match";
                                $rs_id=RecipeIngredient::find($ids);
                               
                                   if(isset($rs_id))
                                    {
                                        $updateArr=array();
                                        $updateArr['ri_title']=$value;
                                       // print_r($updateArr);
                                        RecipeIngredient::find($ids)->fill($updateArr)->save();
                                    }
                                    else
                                    {
                                        $stepArr=array();
                                        $stepArr=array('ri_rec_id'=>$request->rec_id,'ri_title'=>$value);
                                        RecipeIngredient::create($stepArr)->rs_id;  
                                    }
                            }
                        } 
                }
               else{
                 $stepArr=array();
                    $stepArr=array('ri_rec_id'=>$request->rec_id,'ri_title'=>$value);
                 
                    RecipeIngredient::create($stepArr)->rs_id;
                }
               

              
                
              
            }
         
        if($request->rec_img_path)
        {
            $update_arr=array();
            if ($request->hasFile('rec_img_path')) {
                if($coverpic!='')
                {
                     $this->removeImage($rec_id,'recipe',$coverpic);
                }
                $path=$this->fileUpload("recipe",$rec_id,$request->file('rec_img_path')->getClientOriginalName());
               
                $split=explode("|", $path);
                $img_path=chop($split[0],$split[1]);
                $res =  $request->file('rec_img_path')->move($img_path, $split[1]);
                $request->rec_img_path = $split[1];
                $update_arr['rec_img_path'] = $request->rec_img_path;
                
            }
            Recipe::find($rec_id)->fill($update_arr)->save();
        }
        $request->session()->flash("message",'Recipe Updated successfully,wait for admin approval to publish.');
        return redirect('/');
    }

    function deleteRecipeIngrediant(Request $request)
    {
        RecipeIngredient::find($request->id)->delete();
        echo "1";exit;
    }

    function recipeDetails($id,Request $request)
    {

        $userDetails=array();
        
        $recData=Recipe::find($id);
        $userData=User::where('id',$recData['rec_user'])->first();
        $userDetails=UserDetail::where('ud_user',$recData['rec_user'])->first();
        if($userDetails==NULL)
        {
            $userDetails['ud_followers']=0;
            $userDetails['ud_followings']=0;
            $userDetails['ud_mobile']='';
            $userDetails['ud_gender']=0;
            $userDetails['ud_about']='';
            $userDetails['pic_status']=0;
            $userDetails['ud_id']=0;
            $userDetails['ud_profile_img']=$this->getImagePath($userData['id'],'','users');
        }
        else
        {
            $ud_profile_img=$this->getImagePath($userData['id'],$userDetails['ud_profile_img'],'users');
            $pic_arr=explode("/", $ud_profile_img);
            if(last($pic_arr)=='no_image.png')
            {
                $userDetails['pic_status']=0;
            }
            else
            {
                $userDetails['pic_status']=1;
            }
            $userDetails['ud_profile_img']=$ud_profile_img;
            
        }

        $recSteps=RecipeStep::where('rs_rec_id',$id)->get();
        $recIngre=RecipeIngredient::where('ri_rec_id',$id)->get();
        foreach($recSteps as $r=>$rs)
        {
            $recSteps[$r]->rs_rec_image=$this->getImagePath($rs->rs_id,$rs->rs_rec_image,'recipe_steps');
         
        }
        $recComments=RecipeComment::where('rc_rec_id',$id)->where('rc_admin_status',1)->orderBy('rc_created_at','desc')->get();
        foreach ($recComments as $key => $value) {
            $recComments[$key]->comment_user=User::find($value->rc_cuser_id)->name;
            $comment_user_pic=UserDetail::where('ud_user',$value->rc_cuser_id)->first();
            if(isset($comment_user_pic))
            {
                $recComments[$key]->cu_image=$this->getImagePath($value->rc_cuser_id,$comment_user_pic->ud_profile_img,'users');
            }
            else
            {
                $recComments[$key]->cu_image=$this->getImagePath($value->rc_cuser_id,'','users');
            }
        }
       
     //   $recComments['created_at']=date('F j, Y \a\t h:i A',strtotime($recComments['rc_created_at']));
        if($recData['rec_img_path']=='')
        {
            $recData['rec_img_path']="No Image";
        }
        else
        {
            $rc_cover_img=$this->getImagePath($id,$recData['rec_img_path'],'recipe');
          
            $imageArr=explode("/", $rc_cover_img);
            $lastElement=end($imageArr);
            
            if($lastElement=='no_image.png')
            {
                $recData['rec_img_path']="No Image";
            }
            else
            {
                $recData['rec_img_path']=$rc_cover_img;
            }
           // print_r($recData['rec_img_path']);exit;
        }
         //print_r($recData['rec_img_path']);exit;
        $recData['created_at']=date('F j, Y \a\t h:i A',strtotime($recData['rec_created_at']));
       // print_r($userDetails);exit;
       // echo $recData['rec_img_path'];exit;
        $data=array('recData'=>$recData,'userData'=>$userData,'userDetails'=>$userDetails,'recComments'=>$recComments,'recSteps'=>$recSteps,'recIngre'=>$recIngre,'cookie'=>array());
        return view('users.user-recipedetail')->with($data);
    }
    
}
