<?php 

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\RecipeComment;
use App\Recipe;
use App\User;
use App\UserDetail;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Traits\GetData;

class RecipeCommentController extends Controller
{
    use GetData;
	public function index(Request $request)
	{
		$all_commments=RecipeComment::orderBy('rc_id','desc')->paginate(10);

		foreach ($all_commments as $key => $value) {
            
			$recipes=Recipe::find($value->rc_rec_id)->rec_title;
            
             $all_commments[$key]->rec='';
             if(isset($recipes))
			     $all_commments[$key]->rec=$recipes;
			
		}
        
        
		return view('backend.list_recipe-comments')->with(['all_commments'=>$all_commments]);	
	}

	 function ajaxRecipeCommentData()
    {
        $recipecomments=RecipeComment::orderBy('rc_id','desc')->paginate(10);

        $str='<tbody class="table_data">';
        foreach ($recipecomments as $key => $value) {
        	$recipe_details=Recipe::find($value->rc_rec_id)->rec_title;
            $rec_title='';
             if(isset($recipe_details))
                $rec_title=$recipe_details;
            $url='/backend/user-recipe/'.$value->rc_rec_id;
           	$status=($value->rc_admin_status==1)?'Approved':'Disapproved';
            $valueforbutton=($value->rc_admin_status==1)?"Disapprove":"Approve";
            $classforbutton=($value->rc_admin_status==1)?"btn-danger":"btn-info";
            $textcolor=($value->rc_admin_status==0)?"color:red":"";
           $str.='<tr><td>'.$value->rc_content.'</td><td>'.$rec_title.'</td><td><center><a href='.$url.'><button class="btn btn-warning">View</button></a><input class="btn '.$classforbutton.'" type="button" name="" value='.$valueforbutton.' style="margin-left: 20px;" onclick="changeStatus('.$value->rc_id.')"></center></td></tr>';
        }
        $str.='</tbody>';
        echo $str;exit;
        return datatables()->of($str)->toJson();
    }

    function approveOrNot(Request $request)
    {
        if($request->id)
        {

            $recipeData=RecipeComment::find($request->id);
          
            $updateArr=array();
            if($recipeData->rc_admin_status==1)
            {
                $updateArr['rc_admin_status']=0;
            }
            else
            {
                $updateArr['rc_admin_status']=1;   
            }
            RecipeComment::find($request->id)->fill($updateArr)->save();
            echo "Status Chnaged successfully";exit;
        }
       
    }

    function postComment(Request $request)
    {
        if($request->all())
        {
            if($request['rc_cuser_id']=='')
            {
                echo '0';exit;
            }
            else
            {
                $updateArr=$request->all();
                $updateArr['rc_admin_status']=0;
            
                $id=RecipeComment::create($updateArr)->rc_id;

                if(!is_null($id))
                {
                    $recipeComments=RecipeComment::where('rc_rec_id',$request['rc_rec_id'])->where('rc_admin_status',1)->orderBy('rc_created_at','desc')->get();
                    
                    foreach ($recipeComments as $key => $value) {
                        $recipeComments[$key]->comment_user=User::find($value->rc_cuser_id)->name;
                        $comment_user_pic=UserDetail::where('ud_user',$value->rc_cuser_id)->first();
                        if(isset($comment_user_pic))
                        {
                            $recipeComments[$key]->cu_image=$this->getImagePath($value->rc_cuser_id,$comment_user_pic->ud_profile_img,'users');
                        }
                        else
                        {
                            $recipeComments[$key]->cu_image=$this->getImagePath($value->rc_cuser_id,'','users');
                        }
                    }
                    
                   return view('users.recipe_comments', ['recComments' => $recipeComments])->render();  
                }
            }
        }
    }
}