<?php 

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use View;
use App\Common;
use App\User;
use App\UserDetail;
use Redirect; 
use App\Http\Traits\GetData;
use App\Blog;
use App\Meal;
use App\Cuisine;
use App\Recipe;
use App\Business;
use App\BusinessType;
use App\RecipeStep;
use App\RecipeIngredient;
use App\BusinessTime;
use App\Days;
use App\Follower;

class UserController extends Controller
{
	use GetData;
	function index(Request $request)
    {
       $all_users=User::where('type',103)->orderBy('id','desc')->paginate(10); 
     
        foreach ($all_users as $key => $value) {
         	
            $userDetail=UserDetail::where('ud_user',$value->id)->first();
            if(isset($userDetail))
            {
            	$userDetail['ud_profile_img']=$this->getImagePath($value->id,$userDetail->ud_profile_img,'users');
                
            }
            else
            {
                $userDetail['ud_profile_img']=$this->getImagePath($value->id,'','users');
                $userDetail['ud_mobile']='';
            }
            $all_users[$key]['user_detail']=$userDetail;

        }

        $data['pagename']='users';
        $data['all_users']=$all_users;
        
        return view('backend.list_user')->with($data);
    }

    function userDetails($id)
    {

        $user_details=User::find($id);
        $users=UserDetail::where('ud_user',$id)->first();
        $blogs=Blog::where('blog_user_id',$id)->paginate(10);
        $recepieData=Recipe::where('rec_user',$id)->paginate(10);
        $businesses=Business::where('b_user_id',$id)->paginate(10);
        $users->profile_pic=$this->getImagePath($id,$users->ud_profile_img,'users');
        foreach ($blogs as $key => $value) {
            $blogs[$key]->meal=Meal::find($value->blog_meal_id)->meal_title;
            $blogs[$key]->cuisines=Cuisine::find($value->blog_cuisine_id)->cu_title;
        }
        foreach ($recepieData as $key1 => $value1) {
            $recepieData[$key1]->meal=Meal::find($value1->rec_meal_id)->meal_title;
            $recepieData[$key1]->cuisines=Cuisine::find($value1->rec_cuisine_id)->cu_title;
        }
         foreach ($businesses as $key2 => $value2) {
            $businesses[$key2]->business_type=BusinessType::find($value2->b_bt_id)->bt_name;
        }
        $data=array('userData'=>$user_details,'blogData'=>$blogs,'recepieData'=>$recepieData,'users'=>$users,'businesses'=>$businesses);
        
    	return view('backend.user_details')->with($data);
    }

    function userBlogDetails($id,Request $request)
    {
        $blog_details=Blog::find($id);
        if(isset($blog_details))
        {
            $user_details=User::find($blog_details->blog_user_id);
            $user=UserDetail::where('ud_user',$blog_details->blog_user_id)->first();
            $blog_details['meal']=Meal::find($blog_details->blog_meal_id)->meal_title;
            $blog_details['cuisines']=Cuisine::find($blog_details->blog_cuisine_id)->cu_title;
             $blog_details['created_at']=$this->dateTimeFormatWithYear($blog_details->blog_created_at);
              $blog_details['cover-pic']=$this->getImagePath($id,$blog_details->blog_cover_img,'blogs');
            $data=array('blog_details'=>$blog_details,'user'=>$user,'user_details'=>$user_details);
            return view('backend.user_blog-details')->with($data);
        }
        else
        {
            $request->session()->flash('message','Record doesnot exists');
            return Redirect::back();
        }
    }
    function userRecipeDetails($id,Request $request)
    {
        $rec_details=Recipe::find($id);
        if(isset($rec_details))
        {
            $user_details=User::find($rec_details->rec_user);
            $user=UserDetail::where('ud_user',$rec_details->rec_user)->first();
            $rec_details['meal']=Meal::find($rec_details->rec_meal_id)->meal_title;
            $rec_details['cuisines']=Cuisine::find($rec_details->rec_cuisine_id)->cu_title;
             $rec_details['created_at']=$this->dateTimeFormatWithYear($rec_details->rec_created_at);
              $rec_details['image']=$this->getImagePath($id,$rec_details->rec_img_path,'recipes');
              $recipe_steps=RecipeStep::where('rs_rec_id',$id)->get();
              $step_array=array();
              foreach ($recipe_steps as $key => $value) {
                  $recipe_steps[$key]->rs_rec_image=$this->getImagePath($value->rs_id,$value->rs_rec_image,'recipe_steps');
              }

            $data=array('rec_details'=>$rec_details,'user_details'=>$user_details,'user'=>$user,'recipe_steps'=>$recipe_steps);
            return view('backend.user_recipe-details')->with($data);
        }
        else
        {
            $request->session()->flash('message','Record doesnot exists');
            return Redirect::back();
        }
    }

    function userBusinessDetails($id,Request $request)
    {
        $bus_details=Business::find($id);
        if(isset($bus_details))
        {
            $user_details=User::find($bus_details->rec_user);
            $user=UserDetail::where('ud_user',$bus_details->rec_user)->first();
            $bus_details['business_type']=BusinessType::find($bus_details->b_bt_id)->bt_name;
            
             $bus_details['created_at']=$this->dateTimeFormatWithYear($bus_details->b_created_at);
              $bus_details['b_photo']=$this->getImagePath($id,$bus_details->b_photo,'businesses/photo');
            
              $bus_details['b_logo']=$this->getImagePath($id,$bus_details->b_logo,'businesses/logo');
              $days=explode("|", $bus_details->b_open_days);
              $timing=BusinessTime::where('but_b_id',$id)->get();
              foreach ($timing as $key => $value) {
                  $timing[$key]->day_title=Days::find($value->but_day_id)->day_title;
              }

            $data=array('bus_details'=>$bus_details,'user_details'=>$user_details,'user'=>$user,'days'=>$days,'timing'=>$timing);
            return view('backend.user_business-details')->with($data);
        }
        else
        {
            $request->session()->flash('message','Record doesnot exists');
            return Redirect::back();
        }
    }

    function userDisplayProfile(Request $request)
    {
        $userData=User::find($request->session()->get('sessionData.id'));
        $id=$request->session()->get('sessionData.id');
        $userDetails=array();
        $userDetails=UserDetail::where('ud_user',$request->session()->get('sessionData.id'))->first();
        if($userDetails==NULL)
        {
            $userDetails['ud_followers']=0;
            $userDetails['ud_followings']=0;
            $userDetails['ud_mobile']='';
            $userDetails['ud_gender']=0;
            $userDetails['ud_about']='';
            $userDetails['pic_status']=0;
            $userDetails['ud_id']=0;
            $userDetails['ud_profile_img']=$this->getImagePath($userData['id'],'','users');
        }
        else
        {
            $ud_profile_img=$this->getImagePath($userData['id'],$userDetails['ud_profile_img'],'users');
            $pic_arr=explode("/", $ud_profile_img);
            if(last($pic_arr)=='no_image.png')
            {
                $userDetails['pic_status']=0;
            }
            else
            {
                $userDetails['pic_status']=1;
            }
            $userDetails['ud_profile_img']=$ud_profile_img;
            
        }
        $blogDatapublish=Blog::where('blog_user_id',$id)->where('blog_admin_status',1)->paginate(4);
        $blogDataunpublish=Blog::where('blog_user_id',$id)->where('blog_admin_status',0)->paginate(4);

        $recepieDatapublish=Recipe::where('rec_user',$id)->where('rec_admin_status',1)->paginate(2);

        $recepieDataunpublish=Recipe::where('rec_user',$id)->where('rec_admin_status',0)->paginate(2);
        $businessesPublish=Business::where('b_user_id',$id)->where('b_payment_status',1)->paginate(1);

        $businessesunPublish=Business::where('b_user_id',$id)->where('b_payment_status',0)->paginate(2);
       
        foreach ($blogDatapublish as $key => $value) {
            $blogDatapublish[$key]->meal=Meal::find($value->blog_meal_id)->meal_title;
            $blogDatapublish[$key]->blog_cover_img=$this->getImagePath($value->blog_id,$value->blog_cover_img,'blogs');
            $blogDatapublish[$key]->cuisines=Cuisine::find($value->blog_cuisine_id)->cu_title;
        }
         foreach ($blogDataunpublish as $k => $v) {
            $blogDataunpublish[$k]->meal=Meal::find($v->blog_meal_id)->meal_title;
            $blogDataunpublish[$k]->blog_cover_img=$this->getImagePath($v->blog_id,$value->blog_cover_img,'blogs');
            $blogDataunpublish[$k]->cuisines=Cuisine::find($v->blog_cuisine_id)->cu_title;
        }
        foreach ($recepieDatapublish as $key1 => $value1) {
            $recepieDatapublish[$key1]->meal=Meal::find($value1->rec_meal_id)->meal_title;
            $rc_cover_img=$this->getImagePath($value1->rec_id,$value1->rec_img_path,'recipe');
          
            $imageArr=explode("/", $rc_cover_img);
            $lastElement=end($imageArr);
            
            if($lastElement=='no_image.png')
            {
                 $recepieDatapublish[$key1]->rec_img_path="No Image";
            }
            else
            {
                 $recepieDatapublish[$key1]->rec_img_path=$rc_cover_img;
            }
            $recepieDatapublish[$key1]->cuisines=Cuisine::find($value1->rec_cuisine_id)->cu_title;
            $recepieDatapublish[$key1]->ingrediants=RecipeIngredient::where('ri_rec_id',$value1->rec_id)->get();
        }
        foreach ($recepieDataunpublish as $k1 => $v1) {
            $recepieDataunpublish[$k1]->meal=Meal::find($v1->rec_meal_id)->meal_title;
            $recepieDataunpublish[$k1]->cuisines=Cuisine::find($v1->rec_cuisine_id)->cu_title;
            $rc_cover_img=$this->getImagePath($v1->rec_id,$v1->rec_img_path,'recipe');
          
            $imageArr=explode("/", $rc_cover_img);
            $lastElement=end($imageArr);
            
            if($lastElement=='no_image.png')
            {
                 $recepieDataunpublish[$k1]->rec_img_path="No Image";
            }
            else
            {
                 $recepieDataunpublish[$k1]->rec_img_path=$rc_cover_img;
            }
            $recepieDataunpublish[$k1]->ingrediants=RecipeIngredient::where('ri_rec_id',$v1->rec_id)->get();
        }
         foreach ($businessesPublish as $key2 => $value2) {
            $businessesPublish[$key2]->business_type=BusinessType::find($value2->b_bt_id)->bt_name;
        }
        foreach ($businessesunPublish as $k2 => $v2) {
            $businessesunPublish[$k2]->business_type=BusinessType::find($v2->b_bt_id)->bt_name;
        }
         if ($request->ajax()) {
            //print_r($blogs);exit;
            if($request->for=='publish')
            {
                return view('users.blogs-load', ['blogDatapublish' => $blogDatapublish])->render();  
            }
            if($request->for=='unpublish')
            {
                return view('users.blog-load-unpublish', ['blogDataunpublish' => $blogDataunpublish])->render();  
            }
            if($request->for=='recipe-publish')
            {
                return view('users.recipe-publish', ['recepieDatapublish' => $recepieDatapublish])->render();  
            }
            if($request->for=='recipe-unpublish')
            {
                return view('users.recipe-unpublish', ['recepieDataunpublish' => $recepieDataunpublish])->render();  
            }
            if($request->for=='bus-publish')
            {
                return view('users.business-publish', ['businessesPublish' => $businessesPublish])->render();  
            }
            if($request->for=='bus-unpublish')
            {
                return view('users.business-unpublish', ['businessesunPublish' => $businessesunPublish])->render();  
            }
            
        }
        $data=array('userData'=>$userData,'userDetails'=>$userDetails,'blogDatapublish'=>$blogDatapublish,'blogDataunpublish'=>$blogDataunpublish,'recepieDatapublish'=>$recepieDatapublish,'recepieDataunpublish'=>$recepieDataunpublish,'businessesPublish'=>$businessesPublish,'businessesunPublish'=>$businessesunPublish);
        $cookie['cookie_email'] = (isset($_COOKIE['cookie_email'])) ? $_COOKIE['cookie_email'] : '';
        $cookie['cookie_password'] = (isset($_COOKIE['cookie_password'])) ? $_COOKIE['cookie_password'] : '';
        $data['cookie']=$cookie;
        return view('users.user-profile')->with($data);
    }

    function userChangeProfile(Request $request)
    {
        if($request->all())
        {

            //return $request->all();
            $updateArr=$request->all();
            $updateArr['ud_gender']=(int)$updateArr['ud_gender'];
            $field_Arr=array('ud_mobile','ud_profile_img','ud_about','ud_gender');
            User::find($request->id)->fill($updateArr)->save();
            if($updateArr['ud_id']==0)
            {
                $updateArr['ud_user']=$updateArr['id'];
                $ud_id=UserDetail::create($updateArr)->ud_id;
            }
            else
            {
                UserDetail::find($updateArr['ud_id'])->fill($updateArr)->save();
                $ud_id=$updateArr['ud_id'];
            }
            if($request->ud_profile_img)
                {
                    $update_arr=array();
                    if ($request->hasFile('ud_profile_img')) {
                        
                        $path=$this->fileUpload("users",$request->id,$request->file('ud_profile_img')->getClientOriginalName());
                       
                        $split=explode("|", $path);
                        $img_path=chop($split[0],$split[1]);
                        $res =  $request->file('ud_profile_img')->move($img_path, $split[1]);
                        $request->ud_profile_img = $split[1];
                        $update_arr['ud_profile_img'] = $request->ud_profile_img;
                       

                    }
                     UserDetail::find($ud_id)->fill($update_arr)->save();
                }
            $request->session()->flash('message','Profile Updated Sucessfully');
        }

        return redirect('user-profile');
    }

    function follow(Request $request)
    {
        print_r($request->all());exit;
        if($sessionId=='')
        {
            echo "0";exit;
        }
        else
        {
            $followArr=array();
            $followArr['f_follow_status']=1;
            $followArr['f_uo_id']=$request['sessionId'];
            $followArr['f_us_id']=$request['bloguserid'];
            $f_id=Follower::create($followArr)->f_id;
            if(!is_null($f_id))
            {
                echo "1";exit;
            }
        }
    }
}
