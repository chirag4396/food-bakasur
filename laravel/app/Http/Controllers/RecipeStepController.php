<?php

namespace App\Http\Controllers;

use App\RecipeStep;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;

class RecipeStepController extends Controller
{
    use GetData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RecipeStep  $recipeStep
     * @return \Illuminate\Http\Response
     */
    public function show(RecipeStep $recipeStep)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RecipeStep  $recipeStep
     * @return \Illuminate\Http\Response
     */
    public function edit(RecipeStep $recipeStep)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RecipeStep  $recipeStep
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecipeStep $recipeStep)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RecipeStep  $recipeStep
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecipeStep $recipeStep)
    {
        //
    }

    function deleteRecipeStep(Request $request)
    {

        $rsData=RecipeStep::find($request->id);

        $filename=$rsData['rs_rec_image'];
        if($filename!='')
        {

            $this->removeImage($request->id,'recipe_steps',$filename);
        }
        RecipeStep::find($request->id)->delete();
        echo "1";exit;
    }
}
