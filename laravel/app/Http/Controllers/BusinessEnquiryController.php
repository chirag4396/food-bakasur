<?php 

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\DB;
use App\Events\SendMail;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use App\BusinessEnquiry;
use App\Business;
use App\User;
use Mail;

class BusinessEnquiryController extends Controller
{
	use GetData;
	function postEnquiry(Request $request)
	{
		if($request->all())
		{

			$be_id=BusinessEnquiry::create($request->all())->be_id;
			
			if(!is_null($be_id))
			{
				$business=Business::find($request['be_b_id']);
				if(isset($business))
				{
					$businessOwner=User::find($business->b_user_id);
					if(isset($businessOwner))
					{
						$toemail=$businessOwner->email;
						$toname=$businessOwner->name;
					}
					$emailDataArr=array('businessname'=>$business->b_name,'name'=>$request->e_name,'email'=>$request->e_email,'phone'=>$request->e_phone,'message'=>$request->e_msg,'owner_name'=>$businessOwner->name);
					$this->send_mail($emailDataArr,'users-businessenquirytemplate','Food-Bakasur::Enquiry');
					Mail::send('users.models.businessenquirytemplate', $data, function ($message) {
					  
					    $message->to("kinjal.sungare@gmail.com", $businessOwner->name);
					});
					echo "1";exit;
				}
			}
		}
	}
}