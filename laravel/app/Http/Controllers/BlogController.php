<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use App\Blog;
use App\BlogComment;
use Illuminate\Http\Request;
use App\Classes\UploadHandler;
use App\Http\Traits\GetData;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Meal;
use App\Cuisine;
use App\User;
use App\UserDetail;
use App\BlogVisitor;
use App\Follower;

class BlogController extends Controller
{
    use GetData;
    
    public function index()
    {
        $blogs = Blog::orderBy('blog_id','desc')->get();
        foreach ($blogs as $key => $value) {
            $blogs[$key]->meal=Meal::find($value->blog_meal_id)->meal_title;
            $blogs[$key]->cuisines=Cuisine::find($value->blog_cuisine_id)->cu_title;
        }
        $data=array('blogData'=>$blogs);
        return view('backend.list-blogs')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::guest()){
            return redirect('/');
        }else{
            if(isset($req['title'])){                
                $blog = $this->createBlog($req['title']);
            }else{
                if($req['requested'] != 'new'){
                    $blog = Blog::where('blog_user_id',Auth::user()->id)->where('blog_user_status',1)->first();                
                    if(!$blog){
                        $blog = $this->createBlog();               
                    }           
                }else{                
                    $blog = $this->createBlog();                    
                    // return redirect()->back()->with(['blog' => $blog]);                    
                }
            }
            return view('user.create_blog')->with(['blog' => $blog]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }

    public function createBlog(Request $request)
    {
        
        if($request->session()->get('sessionData.id')=='')
        {
            return redirect('/');
        }
        if($request->all())
        {
            $blog_data=$request->all();
          //  print_r($request->session()->get('sessionData'));exit;
            $blog_data['blog_user_id']=$request->session()->get('sessionData.id');
            $blog_data['blog_admin_status']=0;
            $blog_id=Blog::create($blog_data)->blog_id;
            if($blog_id>0)
            {
                $request->session()->flash('message','Blog created successfully, and wait for approval to publish.');
                if($request->blog_cover_img)
                {
                    $update_arr=array();
                    if ($request->hasFile('blog_cover_img')) {
                        
                        $path=$this->fileUpload("blogs",$blog_id,$request->file('blog_cover_img')->getClientOriginalName());
                       
                        $split=explode("|", $path);
                        $img_path=chop($split[0],$split[1]);
                        $res =  $request->file('blog_cover_img')->move($img_path, $split[1]);
                        $request->blog_cover_img = $split[1];
                        $update_arr['blog_cover_img'] = $request->blog_cover_img;
                       

                    }
                     Blog::find($blog_id)->fill($update_arr)->save();
                }
                return redirect('/foodbakasur-listing');
            }

        }
        $mealData=Meal::all();
        $cuisinesData=Cuisine::all();
        $data=array('meals'=>$mealData,'cuisines'=>$cuisinesData,'mode'=>'Create');
        return view('users.blog-create')->with($data);
    }

    function ajaxBlogData()
    {
        $blogs=Blog::orderBy('blog_id','desc')->get();
        $str='<tbody class="table_data">';

        foreach ($blogs as $key => $value) {
            $url='/backend/user-blog/'.$value->blog_id;
           $meal=Meal::find($value->blog_meal_id)->meal_title;
            $cuisines=Cuisine::find($value->blog_cuisine_id)->cu_title;
            $status=($value->blog_admin_status==1)?'Approved':'Disapproved';
            $vegnonveg=($value->blog_veg==1)?'Veg':'Non Veg';
            $valueforbutton=($value->blog_admin_status==1)?"Disapprove":"Approve";
            $classforbutton=($value->blog_admin_status==1)?"btn-danger":"btn-info";
            $textcolor=($value->blog_admin_status==0)?"color:red":"";
           $str.='<tr><td>'.$value->blog_title.'</td><td>'.$meal.'</td><td>'.$cuisines.'</td><td>'.$vegnonveg.'</td><td>'.$status.'</td><td><center><a href='.$url.'><button class="but btn-info">View</button></a><input class="btn '.$classforbutton.'" type="button" name="" value='.$valueforbutton.' style="margin-left: 20px;" onclick="changeStatus('.$value->blog_id.')"></center></td></tr>';
        }
        $str.='</tbody>';
        return datatables()->of($str)->toJson();
    }

    function approveOrNot(Request $request)
    {
        if($request->id)
        {

            $BlogData=Blog::find($request->id);
           
            $updateArr=array();
            if($BlogData->blog_admin_status==1)
            {
                $updateArr['blog_admin_status']=0;
            }
            else
            {
                $updateArr['blog_admin_status']=1;   
            }
            Blog::find($request->id)->fill($updateArr)->save();
            echo "Status Chnaged successfully";exit;
        }
       
    }

    function updateBlog($id,Request $request)
    {
        if($request->session()->get('sessionData.id')=='')
        {
            return redirect('/');
        }
        $blogData=Blog::find($id);

        if($blogData['blog_cover_img']=='')
        {
            $blogData['blog_cover_img']="No Image";
        }
        else
        {
            $blog_cover_img=$this->getImagePath($id,$blogData['blog_cover_img'],'blogs');
          
            $imageArr=explode("/", $blog_cover_img);
            $lastElement=end($imageArr);
            
            if($lastElement=='no_image.png')
            {
                $blogData['blog_cover_img']="No Image";
            }
            else
            {
                $blogData['blog_cover_img']=$blog_cover_img;
            }
        }
        
        $mealData=Meal::all();
        $cuisinesData=Cuisine::all();
        $data=array('meals'=>$mealData,'cuisines'=>$cuisinesData,'blogData'=>$blogData,'mode'=>'Update');
        return view('users.blog-create')->with($data);
    }

    public function editBlog(Request $request)
    {
        if($request->session()->get('sessionData.id')=='')
        {
            return redirect('/');
        }
        if($request->all())
        {
            $updateArr=$request->all();
            $updateArr['blog_admin_status']=0;
            $blog_id=$updateArr['blog_id'];
            unset($updateArr['blog_id']);
            $filename=Blog::find($blog_id)->blog_cover_img;
            Blog::find($blog_id)->fill($updateArr)->save();
             $request->session()->flash('message','Blog updated successfully,wait for admin approval.');
                if($request->blog_cover_img)
                {
                    $update_arr=array();
                    if ($request->hasFile('blog_cover_img')) {
                        if($filename!='')
                        {
                            $this->removeImage($blog_id,'blogs',$filename);
                        }
                        $path=$this->fileUpload("blogs",$blog_id,$request->file('blog_cover_img')->getClientOriginalName());
                       
                        $split=explode("|", $path);
                        $img_path=chop($split[0],$split[1]);
                        $res =  $request->file('blog_cover_img')->move($img_path, $split[1]);
                        $request->blog_cover_img = $split[1];
                        $update_arr['blog_cover_img'] = $request->blog_cover_img;
                       

                    }
                     Blog::find($blog_id)->fill($update_arr)->save();
                }
                return redirect('/foodbakasur-listing');
        }
    }

    function blogDetails($id,Request $request)
    {
       $topBlogger=$this->getTopBlogers();
        $userDetails=array();
        
        $blogData=Blog::find($id);
        $ip_address=$_SERVER['REMOTE_ADDR'];
        $visitors=BlogVisitor::where('bv_ip',$ip_address)->where('bv_blog_id',$id)->first();

            if(!isset($visitors))
            {
                
                    $visitors=$blogData['blog_visitor_count']+1;
                    $blogVisitor=array();
                    $blogVisitor['blog_visitor_count']=$visitors;
                    Blog::find($id)->fill($blogVisitor)->save();
                    $bvisitors=array();
                    $bvisitors['bv_ip']=$ip_address;
                    $bvisitors['bv_blog_id']=$id;
                    BlogVisitor::create($bvisitors)->bv_id;
            }
            $blogData=Blog::find($id);
            $blogData['blog_visitor_count']=$this->number_format_short($blogData['blog_visitor_count']);
            $userData=User::where('id',$blogData['blog_user_id'])->first();
            $countforuserblog=Blog::where('blog_user_id',$blogData['blog_user_id'])->where('blog_admin_status',1)->get();
            $blogCount=count($countforuserblog);
            $userDetails=UserDetail::where('ud_user',$blogData['blog_user_id'])->first();
            if($userDetails==NULL)
            {
                $userDetails['ud_followers']=0;
                $userDetails['ud_followings']=0;
                $userDetails['ud_mobile']='';
                $userDetails['ud_gender']=0;
                $userDetails['ud_about']='';
                $userDetails['pic_status']=0;
                $userDetails['ud_id']=0;
                $userDetails['ud_profile_img']=$this->getImagePath($userData['id'],'','users');
            }
            else
            {
                $ud_profile_img=$this->getImagePath($userData['id'],$userDetails['ud_profile_img'],'users');
                $pic_arr=explode("/", $ud_profile_img);
                if(last($pic_arr)=='no_image.png')
                {
                    $userDetails['pic_status']=0;
                }
                else
                {
                    $userDetails['pic_status']=1;
                }
                $userDetails['ud_profile_img']=$ud_profile_img;
                $userDetails['ud_followers']=0;
                $userDetails['ud_followings']=0;
                
            }
            $followStatus='No';
            $followingStatus='No';
            if($request->session()->get('sessionData.id')!='')
            {
                $sessionId=$request->session()->get('sessionData.id');
                $folloData=Follower::where('f_uo_id',$request->session()->get('sessionData.id'))->orWhere('f_us_id',$request->session()->get('sessionData.id'))->get();
                if(count($folloData)>0)
                {
                   foreach ($folloData as $key => $value) {
                       if($value->f_uo_id==$blogData->blog_user_id || $value->f_us_id==$blogData->blog_user_id)
                       {
                            if($value->f_followback_status==1)
                            {
                               $followingStatus='Yes'; 
                            }
                            if($value->f_follow_status==1)
                            {
                               $followStatus='Yes'; 
                            }
                       }
                   }
                }
            }
            
            $blogComments=BlogComment::where('c_blog_id',$id)->where('c_admin_status',1)->orderBy('c_created_at','desc')->get();
            foreach ($blogComments as $key => $value) {
                $blogComments[$key]->comment_user=User::find($value->c_user_id)->name;
                $comment_user_pic=UserDetail::where('ud_user',$value->c_user_id)->first();
                if(isset($comment_user_pic))
                {
                    $blogComments[$key]->cu_image=$this->getImagePath($value->c_user_id,$comment_user_pic->ud_profile_img,'users');
                }
                else
                {
                    $blogComments[$key]->cu_image=$this->getImagePath($value->c_user_id,'','users');
                }
            }
           
            $blogData['created_at']=date('F j, Y \a\t h:i A',strtotime($blogData['blog_created_at']));
            if($blogData['blog_cover_img']=='')
            {
                $blogData['blog_cover_img']="No Image";
            }
            else
            {
                $blog_cover_img=$this->getImagePath($id,$blogData['blog_cover_img'],'blogs');
              
                $imageArr=explode("/", $blog_cover_img);
                $lastElement=end($imageArr);
                
                if($lastElement=='no_image.png')
                {
                    $blogData['blog_cover_img']="No Image";
                }
                else
                {
                    $blogData['blog_cover_img']=$blog_cover_img;
                }
            }
             $data=array('blogData'=>$blogData,'userData'=>$userData,'userDetails'=>$userDetails,'blogComments'=>$blogComments,'blogCount'=>$blogCount,'topBloggers'=>$topBlogger,'cookie'=>array(),'followingStatus'=>$followingStatus,'followStatus'=>$followStatus);
            return view('users.user-blogdetail')->with($data);
    }

    function getTopBlogers()
    {
       $topBlogger= Blog::orderBy('blog_visitor_count','desc')->paginate(2);
       foreach ($topBlogger as $key => $value) {
           $userData=User::where('id',$value['blog_user_id'])->first();
            $countforuserblog=Blog::where('blog_user_id',$value['blog_user_id'])->where('blog_admin_status',1)->get();
            $blogCount=count($countforuserblog);
            $userDetails=UserDetail::where('ud_user',$value['blog_user_id'])->first();
            if($userDetails==NULL)
            {
                $userDetails['ud_followers']=0;
                $userDetails['ud_followings']=0;
                $userDetails['ud_mobile']='';
                $userDetails['ud_gender']=0;
                $userDetails['ud_about']='';
                $userDetails['pic_status']=0;
                $userDetails['ud_id']=0;
                $userDetails['ud_profile_img']=$this->getImagePath($userData['id'],'','users');
            }
            else
            {
                $ud_profile_img=$this->getImagePath($userData['id'],$userDetails['ud_profile_img'],'users');
                $pic_arr=explode("/", $ud_profile_img);
                if(last($pic_arr)=='no_image.png')
                {
                    $userDetails['pic_status']=0;
                }
                else
                {
                    $userDetails['pic_status']=1;
                }
                $userDetails['ud_profile_img']=$ud_profile_img;
                $userDetails['ud_followers']=0;
                $userDetails['ud_followings']=0;
                
            }       
            if($value['blog_cover_img']=='')
            {
                $value['blog_cover_img']="No Image";
            }
            else
            {
                $id=$value['blog_id'];
                $blog_cover_img=$this->getImagePath($id,$value['blog_cover_img'],'blogs');
              
                $imageArr=explode("/", $blog_cover_img);
                $lastElement=end($imageArr);
                
                if($lastElement=='no_image.png')
                {
                    $topBlogger[$key]->blog_cover_img="No Image";
                }
                else
                {
                    $topBlogger[$key]->blog_cover_img=$blog_cover_img;
                }
            }
            $topBlogger[$key]->userData=$userData;
            $topBlogger[$key]->userDetails=$userDetails;
       }
       return $topBlogger;
    }
}
