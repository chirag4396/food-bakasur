<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use App\User;
use Redirect;
use Symfony\Component\HttpFoundation\Cookie;
use Illuminate\Support\Facades\Auth;
use URL;
use Illuminate\Http\Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function userLogin(Request $request)
    {

        if($request->all())
        {
          // print_r($request->all());exit;
            $userData=User::where('email',$request->input('email'))->first();

            if(isset($userData)>0)
            {
                if($userData->visible_password==$request->input('password'))
                {
                    if($userData->status==1)
                    {
                        $cookie=array();
                        if(($request['remember_me']=='on')){
                          $cookie = $this->setCookie($request->all());
                        }
                        else
                        {
                            unset($_COOKIE['cookie_email']);
                            unset($_COOKIE['cookie_password']);

                        }
                       
                        $sessionData=array();
                        $request->session()->put('sessionData.name', $userData->name);
                        $request->session()->put('sessionData.id', $userData->id);
                        $request->session()->put('sessionData.email', $userData->email);
                        // echo "<pre>";
                        // print_r($request->session()->all());exit;
                        //$response['cookies'] = Cookie::get();
                        $sessionData=$request->session()->all();
                       // print_r($response);exit;
                        return response()->json(['sessionData'=>$sessionData,'cookie
                            .0'=>$cookie]);
                       // return redirect('blog-create');
                    }
                    else
                    {
                        $request->session()->flash('message','Your account is not verify.');
                        echo "Your account is not verify.";exit;
                        //return Redirect::back();
                    }
                    
                }
               
                else
                {
                    $request->session()->flash('message','Password does not match');
                    echo "Password does not match";exit;
                     //return Redirect::back();
                }
            }
            else
            {
                $request->session()->flash('message','Email and password does not match.');
                echo "Email and password does not match.";exit;
                return Redirect::back();
            }
        }
        
        $data=array();
        $cookie=array();
          $cookie['cookie_email'] = ($_COOKIE('cookie_email')!='') ? $_COOKIE('cookie_email') : '';
        $cookie['cookie_password'] = ($_COOKIE('cookie_password')!='') ? $_COOKIE('cookie_password') : '';
        $data['cookie']=$cookie;
        
        $data['remember_me'] = (get_cookie('login_credentials_email') && get_cookie('login_credentials_password')) ? 'on' : 'off';
        return view('users.userlogin')->with($data);
    }
    function setCookie($data)
    {
        $cookie_name = "cookie_email";
         $cookie_value = $data['email'];
         setcookie($cookie_name,$cookie_value, time() + (86400 * 30), "/");
          setcookie('cookie_password',$data['password'], time() + (86400 * 30), "/");
         $cookie_array=array();
         $cookie_array['cookie_email']=$_COOKIE['cookie_email'];
         $cookie_array['cookie_password']=$_COOKIE['cookie_password'];
         return $cookie_array;
    }
    function userLogout(Request $request)
    {
        $request->session()->flush();
        return redirect('/');
    }

    function loggedin()
    {
        $data=array();
        
        $cookie['cookie_email'] = (isset($_COOKIE['cookie_email'])) ? $_COOKIE['cookie_email'] : '';
        $cookie['cookie_password'] = (isset($_COOKIE['cookie_password'])) ? $_COOKIE['cookie_password'] : '';
        $data['cookie']=$cookie;
         
        $data['remember_me'] = (count($cookie)>0) ? 'on' : 'off';
        return view('users.index')->with($data);
    }

    
}
