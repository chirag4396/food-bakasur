<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Support\Facades\Hash;
use App\User;
use Redirect;
use App\UserDetail;

class ForgotPasswordController extends Controller
{
    use GetData;
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    //send email or send otp on entered email or mobile number
    public function passwordRecovery(Request $request)
    {
        if($request->all())
        {
            if($request->email)
            {
                $userData=User::where('email',$request->email)->where('type',103)->first();
                if(isset($userData))
                {
                    $otp=rand(000000,999999);
                    $updateArr['otp']=$otp;
                    User::find($userData->id)->fill($updateArr)->save();
                    $data=array('id'=>$userData->id,'email'=>$request->email,'name'=>$userData->name,'otp'=>$otp);
                    $this->send_mail($data,'users.resetPasswordLinkTemp','Reset Password - Food Bakasur');
                    $request->session()->flash('message','Reset Password link has been sended on your email address,Please check it.');
                    echo "1";exit;
                    return Redirect::back();

                }
                else
                {
                    $request->session()->flash('message','Email address is not registrated so, enter registrated email address');
                    echo "2";exit;
                    //return Redirect::back();
                }
            }
            if($request->ud_mobile)
            {
                $userData=UserDetail::where('ud_mobile',$request->ud_mobile)->first();
                if(isset($userData))
                {
                    $otp=rand(000000,999999);
                    $updateArr['otp']=$otp;
                    User::find($userData->ud_user)->fill($updateArr)->save();
                    $message=$otp." is your One Time Password for Reset Password of your account at Food Bakasur.";
                    $this->sendSMS($userData->ud_mobile,$message);

                    $request->session()->flash('message','OTP sended on your mobile number please check it.');
                    $request->session()->all();
                    echo "1";exit;
                    return json_encode($response);
                    return redirect('otp-verify');

                }
                else
                {
                    $request->session()->flash('message','Mobile No. is not registrated so, enter registrated mobile no.');
                   // echo "Mobile No. is not registrated so, enter registrated mobile no.";exit;
                    echo "2";exit;
                    return Redirect::back();
                }
            }
        }
        return view('users.user-send-resetpwd');
    }

    //view rset password page
    function resetPassword($id,$otp)
    {
        
        $userData=User::where('id',$id)->where('otp',$otp)->where('type',103)->first();
        if(isset($userData))
        {
            $data=array('otp'=>$otp,'id'=>$id);
            return view('users.user-resetpassword')->with($data);
        }
        else
        {
            $data=array('message'=>'Your Reset Link has been expired,Please try again');
            print_r($data);exit;
            return view('users.error-resetpassword')->with($data);
        }
        
    }

    //change password

    function passwordReset(Request $request)
    {
        if($request->all())
        {
            $userData=User::where('id',$request->id)->where('otp',$request->otp)->where('type',103)->first();
            if(isset($userData))
            {
                $updateArr=array();
                $updateArr['visible_password']=$request->password;
                $updateArr['password']=Hash::make($request->password);
                $updateArr['otp']='';
                User::find($request->id)->fill($updateArr)->save();
                $request->session()->flash('message','Password has been changed successfully.');
                return redirect('user-login');
            }
            else
            {
                $data=array('message'=>'Your Reset Password Link has been expired,Please try again.');
                print_r($data);exit;
                return view('users.error-resetpassword')->with($data);
            }
        }
    }

    //view send otp screen
    function sendOTP()
    {
        return view('users.user-sendotp');
    }

    //verify otp and view for otp enter
    function verifyOTP(Request $request)
    {
        if($request->otp)
        {
            $userData=User::where('otp',$request->otp)->first();
            if(isset($userData))
            {
                $request->session()->flash('message','OTP verified successfully.'); 
                return redirect('reset-password/'.$userData->id.'/'.$request->otp);
            }
            else
            {
                $request->session()->flash('message','OTP does not match. please try again.');
                return Redirect::back();
            }
        }
        return view('users.otp-verify');
    }

}
