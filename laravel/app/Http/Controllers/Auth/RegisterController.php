<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers,GetData;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function userRegister(Request $request)
    {
        $userData=$request->all();
        $userData['visible_password']=$userData['password'];
        $userData['password']=Hash::make($userData['password']);
        $userData['otp']=rand(00000,999999);
        $userData['type']=103;
        $userData['status']=0;
        $id=User::create($userData)->id;
        $user_detail['ud_mobile']=$request->ud_mobile;
        $user_detail['ud_user']=$id;
        $ud_id=UserDetail::create($user_detail)->ud_id;
        if(!is_null($id))
        {
            $data=array('id'=>$id,'name'=>$request->name,'email'=>$request->email,'otp'=>$userData['otp']);
            $this->send_mail($data,'users.emailVerifyTemp','Verify Account - Food - Bakasur');
            $request->session()->flash('message','Your are registrated successfully');
            echo "success";exit;
        }
    }

    public function emailAlreadyExists(Request $request)
    {
        $userData=array();
        $userData=User::where('email',$request->input('email'))->get();
        $result='not-exists';
        if(count($userData)>0)
        {
           $result='exists';
        }
        echo $result;exit;

    }

    public function verifyAccount($id,$otp,Request $request)
    {
        $userData=User::where('id',$id)->where('otp',$otp)->first();
        if(isset($userData))
        {
            $updateArr['otp']='';
            $userData['status']=1;
            User::find($id)->fill($updateArr)->save();
            $request->session()->flash('message',"Your account verified successfully");
            return redirect('/');
        }
        else
        {
            echo "sorry,this link has been expired";exit;
        }
    }
}
