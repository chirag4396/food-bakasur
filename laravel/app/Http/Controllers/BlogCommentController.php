<?php 

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\BlogComment;
use App\Blog;
use App\User;
use App\UserDetail;
use App\Http\Traits\GetData;

class BlogCommentController extends Controller
{
    use GetData;
	public function index(Request $request)
	{
		$all_commments=BlogComment::paginate(10);
		foreach ($all_commments as $key => $value) {
			$blog_details=Blog::find($value->c_blog_id)->blog_title;
			 $all_commments[$key]->blog_details=$blog_details;
			
		}
		return view('backend.list_blog-comments')->with(['all_commments'=>$all_commments]);	
	}

	 function ajaxBlogCommentData()
    {
        $blogcomments=BlogComment::orderBy('c_id','desc')->get();
        $str='<tbody class="table_data">';

        foreach ($blogcomments as $key => $value) {
        	$blog_details=Blog::find($value->c_blog_id)->blog_title;
            $url='/backend/user-blogcomments/'.$value->c_id;
           	$status=($value->c_admin_status==1)?'Approved':'Disapproved';
            $valueforbutton=($value->c_admin_status==1)?"Disapprove":"Approve";
            $classforbutton=($value->c_admin_status==1)?"btn-danger":"btn-info";
            $textcolor=($value->c_admin_status==0)?"color:red":"";
           $str.='<tr><td>'.$value->c_content.'</td><td>'.$blog_details.'</td><td><center><a href='.$url.'><button class="btn btn-warning">View</button></a><input class="btn '.$classforbutton.'" type="button" name="" value='.$valueforbutton.' style="margin-left: 20px;" onclick="changeStatus('.$value->c_id.')"></center></td></tr>';
        }
        $str.='</tbody>';
        echo $str;exit;
        return datatables()->of($str)->toJson();
    }

    function approveOrNot(Request $request)
    {
        if($request->id)
        {

            $BlogData=BlogComment::find($request->id);
          
            $updateArr=array();
            if($BlogData->c_admin_status==1)
            {
                $updateArr['c_admin_status']=0;
            }
            else
            {
                $updateArr['c_admin_status']=1;   
            }
            BlogComment::find($request->id)->fill($updateArr)->save();
            echo "Status Chnaged successfully";exit;
        }
       
    }

    function postComment(Request $request)
    {

        if($request->all())
        {
            if($request['c_user_id']=='')
            {
                echo '0';exit;
            }
            else
            {
                $updateArr=$request->all();
                $updateArr['c_admin_status']=0;
            
                $id=BlogComment::create($updateArr)->c_id;

                if(!is_null($id))
                {
                    $blogComments=BlogComment::where('c_blog_id',$request['c_blog_id'])->where('c_admin_status',1)->orderBy('c_created_at','desc')->get();
                  
                    foreach ($blogComments as $key => $value) {
                        $blogComments[$key]->comment_user=User::find($value->c_user_id)->name;
                        $comment_user_pic=UserDetail::where('ud_user',$value->c_user_id)->first();
                        if(isset($comment_user_pic))
                        {
                            $blogComments[$key]->cu_image=$this->getImagePath($value->c_user_id,$comment_user_pic->ud_profile_img,'users');
                        }
                        else
                        {
                            $blogComments[$key]->cu_image=$this->getImagePath($value->c_user_id,'','users');
                        }
                    }
                    //print_r($blogComments);exit;
                   return view('users.blog-comments', ['blogComments' => $blogComments])->render();  
                }
            }
        }
    }
}