<?php

namespace App\Http\Controllers;

use App\Meal;
use Illuminate\Http\Request;

class MealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_meals=Meal::orderBy('meal_id', 'desc')->get();
        $data['pagename']='meal';
        $data['all_portfoliotype']=$all_meals;
        return view('backend.meals')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['pagename']='meal';
        $data['mode']='Add';
        return view('backend.add_update_meal')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $meal_id=Meal::create($request->all())->meal_id;
        if($meal_id>0)
        {
            $request->session()->flash('message', 'Record added successfully');
        }
        return redirect('/backend/meals');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Meal  $meal
     * @return \Illuminate\Http\Response
     */
    public function show(Meal $meal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Meal  $meal
     * @return \Illuminate\Http\Response
     */
    public function edit(Meal $meal,$id)
    {
         $meal=$Meal->find($id);
        if(is_null($meal))
        {
            $request->session()->flash('message','Record does not exists.');
            return redirect('/backend/meal');
        }
        $data=array("meal"=>$meal,"mode"=>'Update',"pagename"=>'meal');
        return view('backend.add_update_meal')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meal  $meal
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
    {
        $result=Meal::find($id)->fill($request->all())->save();
        if($result)
        {
            $request->session()->flash('message','Record updated successfully.');  
        }
        else
        {
            $request->session()->flash('message',"Due to some technical error we can't update record.");  
        }
        return redirect('/backend/meals');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Meal  $meal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $result=Meal::whereIn('meal_id', $request->input('id'))->delete();
        if(!is_null($result))
        {
           $record=($result>1)?'Total ('.$result.') Records are':'Record';
           $request->session()->flash('message',$record.' deleted successfully.');  
        }
        return redirect('/backend/meals');
    }
}
