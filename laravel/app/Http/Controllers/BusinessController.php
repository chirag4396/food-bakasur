<?php

namespace App\Http\Controllers;

use App\Business;
use App\BusinessType;
use App\Http\Traits\GetData;
use Illuminate\Http\Request;
use App\Days;
use App\BusinessTime;
use App\Meal;
use App\Cuisine;
use App\User;
use App\UserDetail;
use App\BusinessComment;
use App\Country;
use App\State;
use App\City;

class BusinessController extends Controller
{
    use GetData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses=Business::orderBy('b_id','desc')->get();
        foreach ($businesses as $key => $value) {
            $bt=BusinessType::where('bt_id',$value->b_bt_id)->first();
            $businesses[$key]->business_type=$bt['bt_name'];
        }
         $data=array('businesses'=>$businesses);
         return view('backend.list_business')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Business $business)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Business  $business
     * @return \Illuminate\Http\Response
     */
    public function destroy(Business $business)
    {
        //
    }

    public function businessAdd(Request $request)
    {
        if($request->session()->get('sessionData.id')=='')
        {
            return redirect('/');
        }
        if($request->input())
        {

            $request['b_user_id']=$request->session()->get('sessionData.id');
            /*`*/
            $startDays=$request['start'];
            $closeDays=$request['close'];
            $request['b_address']=$request['b_addresses'];
             $request['b_meal_id']=implode("|", $request['b_meal_id']);
             $request['b_cuisine_id']=implode("|", $request['b_cuisine_id']);
             $request['b_food_type']=implode("|", $request['b_food_type']);
            $b_id=Business::create($request->all())->b_id;
             foreach ($startDays as $key => $value) {
               
               foreach ($closeDays as $k => $v) {
                        if($key==$k)
                        {
                           
                             if($value!='' && $v!='')
                             {
                                $days=array('but_open'=>$value,'but_close'=>$v,'but_day_id'=>$k,'but_b_id'=>$b_id);
                          
                                $but_id=BusinessTime::create($days)->but_id;
                             }
                        }
                      
                    }
                }
               
            if($request->b_logo || $request->b_photo)
            {
                $update_arr=array();
                if ($request->hasFile('b_logo')) {
                    
                    $path=$this->fileUpload("businesses/logo",$b_id,$request->file('b_logo')->getClientOriginalName());
                    
                    $split=explode("|", $path);
                    $img_path=chop($split[0],$split[1]);
                    $res =  $request->file('b_logo')->move($img_path, $split[1]);
                    $request->b_logo = $split[1];
                    $update_arr['b_logo'] = $request->b_logo;

                }
                if ($request->hasFile('b_photo')) {
                    
                    $path=$this->fileUpload("businesses/photo",$b_id,$request->file('b_photo')->getClientOriginalName());
                    $split=explode("|", $path);
                    $img_path=chop($split[0],$split[1]);
                    $res =  $request->file('b_photo')->move($img_path, $split[1]);
                    $request->b_photo = $split[1];
                    $update_arr['b_photo'] = $request->b_photo;

                }
                Business::find($b_id)->fill($update_arr)->save();
               
            }

            if(!is_null($b_id))
            {
                $data=array('b_name'=>$request->input('b_name'),'email'=>$request->input('b_owner_email'),'name'=>$request->input('b_name'),'username'=>$request->session()->get('sessionData.name'));
                $this->send_mail($data,'users.businessAddedEmailTemp','Thanks For Adding Your Business At Food-Bakasur');
                $request->session()->flash("message",'Thank you for adding your bussiness,Please Check your email and mobile for payment.');
            }
        }
        $mealData=Meal::all();
        $cuisinesData=Cuisine::all();
        $Days=Days::all();
        $businessTypes=BusinessType::all();
        $countries=Country::all();
        $states=State::all();
        $cities=City::all();
        $data=array('businesstypes'=>$businessTypes,'days'=>$Days,'mode'=>'Add','meals'=>$mealData,'cuisines'=>$cuisinesData,'countries'=>$countries,'states'=>$states,'cities'=>$cities);
        return view('users.business-add')->with($data);
    }

    function updateBusiness($id,Request $request)
    {
        if($request->session()->get('sessionData.id')=='')
        {
            return redirect('/');
        }
        $busData=Business::find($id);
        $busData['b_meal_id']=explode("|", $busData['b_meal_id']);
        $busData['b_cuisine_id']=explode("|", $busData['b_cuisine_id']);
        $busData['b_food_type']=explode("|", $busData['b_food_type']);
        $b_logo=$this->getImagePath($id,$busData['b_logo'],'businesses/logo');
        $b_photo=$this->getImagePath($id,$busData['b_photo'],'businesses/photo');
        $photo_arr=last(explode("/", $b_photo));

        if($photo_arr!='no_image.png')
        {
            $busData['b_photo']=$b_photo;
        }
        else
        {
            $busData['b_photo']='';
        }
        $logo_arr=last(explode("/", $b_logo));
        if($logo_arr!='no_image.png')
        {
            $busData['b_logo']=$b_logo;
        }
        else
        {
            $busData['b_logo']='';
        }
        $busTiming=BusinessTime::where('but_b_id',$id)->get();
        $timeArr=array();
        $timeArr['day']=array();
        $timeArr['open_time']=array();
        $timeArr['close_time']=array();
        $i=0;
        foreach ($busTiming as $key => $value) {
            $timeArr['day'][]=$value->but_day_id;
            $timeArr['open_time'][]=$value->but_open;
            $timeArr['close_time'][]=$value->but_close;
            $i++;
        }
        $busData['b_state_id']=City::find($busData['b_city_id'])->city_state_id;
        $busData['b_country_id']=State::find($busData['b_state_id'])->state_country_id;
        $mealData=Meal::all();
        $cuisinesData=Cuisine::all();
        $Days=Days::all();
        $businessTypes=BusinessType::all();
        $countries=Country::all();
        $states=State::all();
        $cities=City::all();
        $data=array('businesstypes'=>$businessTypes,'days'=>$Days,'mode'=>'Update','busData'=>$busData,'busTiming'=>$busTiming,'timeArr'=>$timeArr,'meals'=>$mealData,'cuisines'=>$cuisinesData,'countries'=>$countries,'states'=>$states,'cities'=>$cities);
        return view('users.business-add')->with($data);
    }

    function editBusiness(Request $request)
    {
        if($request->session()->get('sessionData.id')=='')
        {
            return redirect('/');
        }
        $updateData=$request->all();
        $updateData['b_meal_id']=implode("|", $updateData['b_meal_id']);
         $updateData['b_cuisine_id']=implode("|", $updateData['b_cuisine_id']);
         $updateData['b_food_type']=implode("|", $updateData['b_food_type']);
        $logoname=Business::find($updateData['b_id'])->b_logo;
        $b_id=$updateData['b_id'];
        $photoname=Business::find($updateData['b_id'])->b_photo;
        Business::find($updateData['b_id'])->fill($updateData)->save();
        if($request->b_logo || $request->b_photo)
            {
                $update_arr=array();
                if ($request->hasFile('b_logo')) {
                    if($logoname!='')
                    {
                        $this->removeImage($updateData['b_id'],'businesses/logo',$logoname);
                    }
                    $path=$this->fileUpload("businesses/logo",$b_id,$request->file('b_logo')->getClientOriginalName());
                    
                    $split=explode("|", $path);
                    $img_path=chop($split[0],$split[1]);
                    $res =  $request->file('b_logo')->move($img_path, $split[1]);
                    $request->b_logo = $split[1];
                    $update_arr['b_logo'] = $request->b_logo;

                }
                if ($request->hasFile('b_photo')) {
                    if($photoname!='')
                    {
                        $this->removeImage($updateData['b_id'],'businesses/photo',$photoname);
                    }
                    $path=$this->fileUpload("businesses/photo",$b_id,$request->file('b_photo')->getClientOriginalName());
                    $split=explode("|", $path);
                    $img_path=chop($split[0],$split[1]);
                    $res =  $request->file('b_photo')->move($img_path, $split[1]);
                    $request->b_photo = $split[1];
                    $update_arr['b_photo'] = $request->b_photo;

                }
                Business::find($b_id)->fill($update_arr)->save();
               
            }
        BusinessTime::where('but_b_id',$updateData['b_id'])->delete();
        $startDays=$request['start'];
        $closeDays=$request['close'];
        foreach ($startDays as $key => $value) {
           
           foreach ($closeDays as $k => $v) {
                    if($key==$k)
                    {
                       
                        if($value!='' && $v!='')
                        {
                            $days=array('but_open'=>$value,'but_close'=>$v,'but_day_id'=>$k,'but_b_id'=>$updateData['b_id']);
                            $but_id=BusinessTime::create($days)->but_id;
                        }
                    }
                  
                }
            }
       
        $request->session()->flash('message','Business Updated Successfully');
        return redirect('/');
     }

    function businessDetails($id,Request $request)
    {
        $userDetails=array();
        $businessData=Business::find($id);
        $userData=User::where('id',$businessData['b_user_id'])->first();
        $userDetails=UserDetail::where('ud_user',$businessData['b_user_id'])->first();
        $bt=BusinessType::where('bt_id',$id)->first();
        $businessData['business_type']=$bt['bt_name'];
        if($userDetails==NULL)
        {
            $userDetails['ud_followers']=0;
            $userDetails['ud_followings']=0;
            $userDetails['ud_mobile']='';
            $userDetails['ud_gender']=0;
            $userDetails['ud_about']='';
            $userDetails['pic_status']=0;
            $userDetails['ud_id']=0;
            $userDetails['ud_profile_img']=$this->getImagePath($userData['id'],'','users');
        }
        else
        {
            $ud_profile_img=$this->getImagePath($userData['id'],$userDetails['ud_profile_img'],'users');
            $pic_arr=explode("/", $ud_profile_img);
            if(last($pic_arr)=='no_image.png')
            {
                $userDetails['pic_status']=0;
            }
            else
            {
                $userDetails['pic_status']=1;
            }
            $userDetails['ud_profile_img']=$ud_profile_img;
            
        }
        $businessComment=BusinessComment::where('busc_b_id',$id)->where('busc_admin_status',1)->orderBy('busc_created_at','desc')->get();
        foreach ($businessComment as $key => $value) {
            $businessComment[$key]->comment_user=User::find($value->busc_cuser_id)->name;
            $comment_user_pic=UserDetail::where('ud_user',$value->busc_cuser_id)->first();
            if(isset($comment_user_pic))
            {
                $businessComment[$key]->cu_image=$this->getImagePath($value->c_user_id,$comment_user_pic->ud_profile_img,'users');
            }
            else
            {
                $businessComment[$key]->cu_image=$this->getImagePath($value->c_user_id,'','users');
            }
        }
       
        $businessData['created_at']=date('F j, Y \a\t h:i A',strtotime($businessData['b_created_at']));
        if($businessData['b_photo']=='')
        {
            $businessData['b_photo']="No Image";
        }
        else
        {
            $b_cover_img=$this->getImagePath($id,$businessData['b_photo'],'businesses/photo');
          
            $imageArr=explode("/", $b_cover_img);
            $lastElement=end($imageArr);
            
            if($lastElement=='no_image.png')
            {
                $businessData['b_photo']="No Image";
            }
            else
            {
                $businessData['b_photo']=$b_cover_img;
            }
        }
        $businessData['b_meal_id']=explode("|", $businessData['b_meal_id']);
        $businessData['b_cuisine_id']=explode("|", $businessData['b_cuisine_id']);
        $businessData['b_food_type']=explode("|", $businessData['b_food_type']);
        $businessTime=BusinessTime::where('but_b_id',$id)->get();
        foreach($businessTime as $bt=>$btv)
        {
            $businessTime[$bt]->daytitle=Days::find($btv->but_day_id)->day_title;
           $businessTime[$bt]->startTime= date("g:i a", strtotime($btv->but_open));
           $businessTime[$bt]->closeTime= date("g:i a", strtotime($btv->but_close));
        }  
        $business['food_type']=array();
        $business['meals']=array();
        $business['cuisines']=array();
        foreach ($businessData['b_meal_id'] as $key => $value) {
            if($value!='')
            {

                 $meal_title=Meal::find($value)->meal_title;
                array_push($business['meals'], $meal_title);
            }
           
        }
        foreach ($businessData['b_cuisine_id'] as $k1 => $v1) {
          if($v1!='')
          {
                

              $cu_title=Cuisine::find($v1)->cu_title;
            array_push($business['cuisines'], $cu_title);
          }
        }
        foreach ($businessData['b_food_type'] as $k2 => $v2) {
            $food_type='';
            
            if(isset($v2))
            {
                $food_type=(($v2=="0")?'Nonveg':'Veg');
                 
                $business['food_type'][$k2]= $food_type;
            }
        }
        $businessData['b_city']=City::find($businessData['b_city_id']);
        $businessData['b_state']=State::find($businessData['b_city']['city_state_id']);
        $businessData['b_country']=Country::find($businessData['b_state']['state_country_id']);
         $businessData['food_type']=implode(" , ", $business['food_type']);
         $data=array('businessData'=>$businessData,'userData'=>$userData,'userDetails'=>$userDetails,'businessComment'=>$businessComment,"business"=>$business,'businessTime'=>$businessTime,'cookie'=>array());
        return view('users.user-businessdeatil')->with($data);
    }

   function ajaxState($id)
    { 
       
        $stateData=State::where('state_country_id',$id)->get();
       $str='<option value="">--select state--</option>';
       foreach ($stateData as $key => $value) {
           $str.='<option value='.$value['state_id'].'>'.$value['state_name'].'</option>';
       }
       echo $str;exit;
    }
     function ajaxcity($id)
    { 
       
        $stateData=City::where('city_state_id',$id)->get();
       $str='<option value="">--select city--</option>';
       foreach ($stateData as $key => $value) {
           $str.='<option value='.$value['city_id'].'>'.$value['city_name'].'</option>';
       }
       echo $str;exit;
    }

}
