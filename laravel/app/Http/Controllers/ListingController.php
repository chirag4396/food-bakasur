<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use App\Listing;
use App\Recipe;
use App\RecipeStep;
use App\Blog;
use App\BusinessType;
use App\Business;
use App\Http\Traits\GetData;
use App\City;
use App\Cuisine;
use App\RecipeIngredient;
use App\Meal;
use StdClass;
class ListingController extends Controller
{
    use GetData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function list()
    {
        $business_listing=BusinessType::where('bt_status',1)->orderBy('bt_created_at','desc')->get();

        foreach ($business_listing as $key => $value) {
             $businessData=array();
            $business=Business::where('b_bt_id',$value->bt_id)->get();
            
            if(count($business)>0)
            {
                foreach ($business as $b => $v) {
                    $v->b_photo=$this->getImagePath($v->b_id,$v->b_photo,'businesses');
                    array_push($businessData, $v);
                }  
            }

           $business_listing[$key]->business=$businessData;
           
        }
        $blog_list=Blog::where('blog_admin_status',1)->orderBy('blog_created_at','desc')->paginate(1); 
        foreach ($blog_list as $key => $value) {
           $blog_Image=$this->getImagePath($value->blog_id,$value->blog_cover_img,'blogs');
            $blog_list[$key]->blog_Image=$blog_Image;
        }
        $reciepes_list=Recipe::where('rec_admin_status',1)->orderBy('rec_created_at','desc')->paginate(1);  
        
        foreach ($reciepes_list as $key => $value) {
            $reciepes_steps=array();
            $recipe_Image=$this->getImagePath($value->rec_id,$value->rec_img_path,'recipes');
            $reciepes_list[$key]->rec_img_path=$recipe_Image;
            $steps=RecipeStep::select('rs_title')->where('rs_rec_id',$value->rec_id)->get();
            foreach ($steps as $keystep => $step) {
               array_push($reciepes_steps, $step->rs_title);
            }
            $reciepes_list[$key]->reciepes_steps=$reciepes_steps;            

        }
        $data['business_listing']=$business_listing;  
        $data['reciepes_list']=$reciepes_list;   
        $data['blog_list']   =$blog_list;
        return view('users.foodbakasur-listing')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function show(Listing $listing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function edit(Listing $listing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Listing $listing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Listing $listing)
    {
        //
    }


    public function searchResult(Request $request)
    {
        // if($request->input('search_box'))
        // {
            $blogArr=array();
            $recipeArr=array();
            $businessArr=array();
            $cusiniesArr=array();
            $mealArr=array();
            $cityArr=array();
            $search_input=$request->input('search_box');
            $search_input_arr=explode(' ', $search_input);
            $keyworks_arr=array('in','at','near');
            foreach ($search_input_arr as $key => $value) {
                if(in_array($value, $keyworks_arr))
                {
                    if(isset($search_input_arr[$key+1]))
                    {

                        $city=City::where('city_name','like',$search_input_arr[$key+1].'%')->first();
                        if(isset($city))
                        {
                            array_push($cityArr,$city->city_id);
                           
                        }
                        
                    }
                    
                }
                $cusinies=Cuisine::where('cu_title','like',$value.'%')->first();
                if(isset($cusinies))
                {
                    $id=$cusinies->cu_id;
                    array_push($cusiniesArr,$id);
                    
                }
                $meals=Meal::where('meal_title','like',$value.'%')->first();
                if(isset($meals))
                {
                    array_push($mealArr,$meals->meal_id);
                }
                
            }
            $businessData=Business::where('b_payment_status',1)->orWhereIn('b_city_id',$cityArr)->paginate(2);
             foreach ($businessData as $buk => $buv) {
                  
                    if($buv->b_bt_id!='')
                    {
                        $businessData[$buk]->business_type=BusinessType::find($buv->b_bt_id)->bt_name;
                    }
                            
                    $businessData[$buk]->b_photo=$this->getImagePath($buv->b_id,$buv->b_photo,'businesses/photo');
                }
          
            
           $blogs= Blog::whereIn('blog_cuisine_id',$cusiniesArr)->orWhereIn('blog_meal_id',$mealArr)->where('blog_admin_status',1)->paginate(2);
           foreach ($blogs as $bk => $bv) {
                $blogs[$bk]->blog_cover_img =$this->getImagePath($bv->blog_id,$bv->blog_cover_img,'blogs');

            }
            $recipes=Recipe::whereIn('rec_cuisine_id',$cusiniesArr)->orWhereIn('rec_meal_id',$mealArr)->where('rec_admin_status',1)->paginate(2);
           foreach ($recipes as $rk => $rv) {
            $recipes[$rk]->rec_img_path =$this->getImagePath($rv->rec_id,$rv->rec_img_path,'recipe');
                $recipes[$rk]->ingrediants=RecipeIngredient::where('ri_rec_id',$rv->rec_id)->paginate(2);
            }
            $data=array('blog_list'=>$blogs,'reciepes_list'=>$recipes,'business_listing'=>$businessData,'cookie'=>array());
              
            return view('users.foodbakasur-listing')->with($data);
        // }
    }
}
