<?php

namespace App\Events;
use Illuminate\Queue\SerializesModels;

class SendEmail
{
    use SerializesModels;

    public $order;

    /**
     * Create a new event instance.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function __construct()
    {
    }
}