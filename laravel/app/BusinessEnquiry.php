<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessEnquiry extends Model
{
	protected $primaryKey = 'be_id';

	protected $fillable = ['be_name','be_email','be_phone','be_msg','be_b_id'];
}
