<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	protected $primaryKey = 'blog_id';

	protected $fillable = ['blog_title', 'blog_story', 'blog_city_id', 'blog_meal_id', 'blog_veg', 'blog_cuisine_id', 'blog_minutes', 'blog_views', 'blog_user_id', 'blog_cover_img', 'blog_user_status', 'blog_admin_status', 'blog_priority','blog_visitor_count'];
	
	CONST CREATED_AT = 'blog_created_at'; 

	CONST UPDATED_AT = 'blog_updated_at';
	
	public function user()
	{
		return $this->belongsTo(\App\User::class, 'blog_user_id');
	}

	public function city()
	{
		return $this->belongsTo(\App\City::class, 'blog_city_id');
	}

	public function meal()
	{
		return $this->belongsTo(\App\Meal::class, 'blog_meal_id');
	}

	public function cuisine()
	{
		return $this->belongsTo(\App\Cuisine::class, 'blog_cuisine_id');
	}

	public function blogcomments()
	{
		return $this->belongsTo(\App\BlogComment::class, 'blog_id');
	}
}
