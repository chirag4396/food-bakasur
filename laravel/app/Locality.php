<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
    protected $primaryKey = 'loc_id';

    protected $fillable = ['loc_title', 'loc_city_id'];

    public $timestamps = false;

    public function city()
    {
    	return $this->belongsTo(\App\City::class, 'loc_city_id');
    }    
}
