<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessType extends Model
{
    protected $primaryKey = 'bt_id';

	protected $fillable = ['bt_name', 'bt_status'];
	
	CONST CREATED_AT = 'bt_created_at'; 

	CONST UPDATED_AT = 'bt_updated_at';
}
