<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuisine extends Model
{
	protected $primaryKey = 'cu_id';

	protected $fillable = ['cu_title'];

	public $timestamps = false;

    public function recipes()
    {
    	return $this->hasMany(\App\Recipe::class, 'rec_cuisine_id');
    }

    public function blogs()
    {
    	return $this->hasMany(\App\Blog::class, 'blog_cuisine_id');
    }
}
