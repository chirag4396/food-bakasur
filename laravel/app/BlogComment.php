<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    protected $primaryKey = 'c_id';

    protected $fillable = ['c_user_id', 'c_blog_id','c_content','c_admin_status'];

    CONST CREATED_AT = 'c_created_at'; 

    CONST UPDATED_AT = 'c_updated_at';

    public function blog_details()
    {

    	return $this->hasOne(\App\Blog::class, 'blog_id');
    }

}
