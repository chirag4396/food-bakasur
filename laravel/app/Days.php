<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Days extends Model
{
	protected $primaryKey = 'day_id';

	protected $fillable = ['day_title'];
	
	// CONST CREATED_AT = false; 

	// CONST UPDATED_AT = false;
	
	/*public function business_days()
	{
		return $this->belongsTo(\App\User::class, 'but_day_id');
	}*/
}
