<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $primaryKey = 'rec_id';

    protected $fillable = ['rec_title', 'rec_img_path', 'rec_description', 'rec_meal_id', 'rec_cuisine_id', 'rec_veg', 'rec_duration', 'rec_status', 'rec_user', 'rec_user_status', 'rec_admin_status', 'rec_priority'];
    
    CONST CREATED_AT = 'rec_created_at'; 

    CONST UPDATED_AT = 'rec_updated_at';
    
    public function steps()
    {
    	return $this->hasMany(\App\RecipeStep::class, 'rs_rec_id');
    }

    public function user()
    {
    	return $this->belongsTo(\App\User::class, 'rec_user');
    }

    public function meal()
    {
    	return $this->belongsTo(\App\Meal::class, 'rec_meal_id');
    }

    public function cuisine()
    {
    	return $this->belongsTo(\App\Cuisine::class, 'rec_cuisine_id');
    }
     public function recipecomments()
    {
        return $this->hasMany(\App\RecipeComment::class, 'rc_rec_id');
    }

}
