<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $primaryKey = 'country_id';

    protected $fillable = ['country_sortname', 'country_name'];

    public $timestamps = false;

    public function state()
    {
    	return $this->hasMany(\App\State::class, 'state_country_id');
    }    
}
