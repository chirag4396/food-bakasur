<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeStep extends Model
{
	protected $primaryKey = 'rs_id';

	protected $fillable = ['rs_title', 'rs_rec_id','rs_rec_image'];

	public $timestamps = false;

	public function recipe()
	{
		return $this->belongsTo(\App\Recipe::class, 'rs_rec_id');
	}
}
