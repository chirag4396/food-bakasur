<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    protected $primaryKey = 'meal_id';

    protected $fillable = ['meal_title'];

    public $timestamps = false;

    public function recipes()
    {
    	return $this->hasMany(\App\Recipe::class, 'rec_meal_id');
    }

    public function recipe()
    {
    	return $this->hasMany(\App\Blog::class, 'blog_cuisine_id');
    }
}
