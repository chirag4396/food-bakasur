<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessTime extends Model
{
	protected $primaryKey = 'but_id';

	protected $fillable = ['but_day_id','but_open','but_close','but_b_id'];
	
	 public $timestamps = false;
	
	
}
