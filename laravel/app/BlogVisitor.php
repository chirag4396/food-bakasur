<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogVisitor extends Model
{    
    protected $primaryKey = 'bv_id';

    protected $fillable = ['bv_ip','bv_blog_id'];

}
