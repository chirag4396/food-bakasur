<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessComment extends Model
{
    protected $primaryKey = 'busc_id';

    protected $fillable = ['busc_cuser_id', 'busc_content','busc_b_id','busc_admin_status'];

    CONST CREATED_AT = 'busc_created_at'; 

    CONST UPDATED_AT = 'busc_updated_at';

    public function business_details()
    {
    	return $this->belongsTo('\App\Business');
    }

   
}
